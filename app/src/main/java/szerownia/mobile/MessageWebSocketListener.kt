package szerownia.mobile

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat.getSystemService
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import szerownia.mobile.Models.ThreadNotification

class MessageWebSocketListener(private val applicationContext: Context, private val activity: MainActivity) : WebSocketListener() {
    private val NORMAL_CLOSURE_STATUS = 1000
    private val messagesChannel = "messagesChannel"

    override fun onMessage(webSocket: WebSocket, text: String) {
        Log.println(Log.INFO, javaClass.toString(), "Received new message")

        val serializer: KSerializer<ThreadNotification> = ThreadNotification.serializer()
        val threadDTO = Json.decodeFromString(serializer, text)

        val sharedPreferences = activity.getSharedPreferences("logged", Context.MODE_PRIVATE)

        if (sharedPreferences.getString("USER_ID", threadDTO.messageSender)!!.toLong() != threadDTO.messageSender.toLong()) {
            val notificationChannel =
                NotificationChannel(messagesChannel, "Messages channel", NotificationManager.IMPORTANCE_DEFAULT)
            notificationChannel.description = "Messages channel for notifications"

            val notificationManager: NotificationManager =
                getSystemService(applicationContext, NotificationManager::class.java)!!
            notificationManager.createNotificationChannel(notificationChannel)

            val notificationManagerCompat = NotificationManagerCompat.from(applicationContext)

            val notificationBuilder = NotificationCompat.Builder(applicationContext, messagesChannel)
            notificationBuilder.setSmallIcon(R.drawable.szerownia_logo_512_transparent)
            notificationBuilder.setContentTitle("Nowa wiadomość")
            notificationBuilder.setContentText(threadDTO.lastMessage)
            notificationBuilder.priority = NotificationCompat.PRIORITY_DEFAULT
            notificationBuilder.setCategory(NotificationCompat.CATEGORY_MESSAGE)
            notificationBuilder.build()

            notificationManagerCompat.notify(1, notificationBuilder.build())
            /** Unique id used for multiple notifications eg. stacking notifications */
        }
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null)
        Log.println(Log.INFO, javaClass.toString(), "Closing socket with code: $code and reason: $reason")
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        Log.e(javaClass.toString(), "Socket failed with error: $t \nand response: $response")
    }
}
