package szerownia.mobile

/**
 * This class is used to validate input fields
 *
 * Use validationErrorMessages.xml for error messages
 * Use validationFields.xml for field names
 * Use validationRules.xml to determine the min/max length of the field etc.
 */

import android.content.res.Resources
import android.text.InputFilter
import android.util.Log
import android.widget.CheckBox
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import com.cottacush.android.currencyedittext.CurrencyEditText
import com.google.android.material.textfield.TextInputEditText
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.UsersAPI
import java.math.RoundingMode

class Validation {

    private var validationCounter = 0

    fun getValidationCounter(): Int {
        return validationCounter
    }

    fun addErrorToCounter() {
        validationCounter++
        Log.println(Log.WARN, javaClass.toString(), "Validation error in addErrorToCounter")

    }

    fun resetValidationCounter() {
        validationCounter = 0
    }

    /** Used for list validation and setting textView error status **/
    fun setTextViewErrorStatus(
        inputField: TextView, errorStringId: Int, listToValidate: MutableList<Any>, activity: FragmentActivity
    ) {
        if (listToValidate.isEmpty()) {
            validationCounter++
            Log.println(
                Log.WARN, javaClass.toString(), "Validation error in setTextViewErrorStatus with ${inputField.text}"
            )

            inputField.error = activity.getString(errorStringId)

            inputField.setOnClickListener {
                inputField.requestFocus()
            }

        } else {
            inputField.error = null
        }
    }

    fun setMaxLength(inputField: TextInputEditText, fieldMaxLength: Int, activity: FragmentActivity) {
        inputField.filters =
            arrayOf<InputFilter>(InputFilter.LengthFilter(activity.resources.getInteger(fieldMaxLength)))
    }

    fun validateOnlyLength(
        inputField: TextInputEditText, fieldNameId: Int, fieldMaxLength: Int, activity: FragmentActivity
    ) {
        if (inputField.text!!.length > activity.resources.getInteger(fieldMaxLength)) {
            validationCounter++
            Log.println(
                Log.WARN,
                javaClass.toString(),
                "Validation error in validateOnlyLength with ${activity.resources.getString(fieldNameId)}"
            )

            inputField.error = activity.resources.getQuantityString(
                R.plurals.all_toolong,
                activity.resources.getInteger(fieldMaxLength),
                activity.resources.getInteger(fieldMaxLength),
                activity.getString(fieldNameId)
            )
        }
    }

    fun validateRangedField(
        inputField: TextInputEditText,
        emptyId: Int,
        fieldNameId: Int,
        fieldMaxLength: Int,
        fieldMinLength: Int,
        activity: FragmentActivity
    ) {
        when {
            inputField.text.isNullOrEmpty() -> {
                validationCounter++
                Log.println(
                    Log.WARN,
                    javaClass.toString(),
                    "Validation error in validateRangedField:1 with ${activity.resources.getString(fieldNameId)}"
                )

                inputField.error = activity.resources.getString(emptyId, activity.resources.getString(fieldNameId))
            }
            inputField.text!!.length > activity.resources.getInteger(fieldMaxLength) -> {
                validationCounter++
                Log.println(
                    Log.WARN,
                    javaClass.toString(),
                    "Validation error in validateRangedField:2 with ${activity.resources.getString(fieldNameId)}"
                )

                inputField.error = activity.resources.getQuantityString(
                    R.plurals.all_toolong,
                    activity.resources.getInteger(fieldMaxLength),
                    activity.resources.getInteger(fieldMaxLength),
                    activity.resources.getString(fieldNameId)
                )
            }
            inputField.text!!.length < activity.resources.getInteger(fieldMinLength) -> {
                validationCounter++
                Log.println(
                    Log.WARN,
                    javaClass.toString(),
                    "Validation error in validateRangedField:3 with ${activity.resources.getString(fieldNameId)}"
                )

                inputField.error = activity.resources.getQuantityString(
                    R.plurals.all_tooshort,
                    activity.resources.getInteger(fieldMinLength),
                    activity.resources.getInteger(fieldMinLength),
                    activity.resources.getString(fieldNameId)
                )
            }
        }
    }

    fun validateIfEmpty(
        inputField: TextInputEditText, emptyId: Int, fieldNameId: Int, activity: FragmentActivity
    ) {
        if (inputField.text.isNullOrEmpty()) {
            validationCounter++
            Log.println(
                Log.WARN,
                javaClass.toString(),
                "Validation error in validateIfEmpty with ${activity.resources.getString(fieldNameId)}"
            )

            inputField.error = activity.resources.getString(emptyId, activity.resources.getString(fieldNameId))
        }
    }

    fun validateNumbers(
        inputField: TextInputEditText,
        emptyId: Int,
        fieldNameId: Int,
        fieldMaxLength: Int,
        fieldMinValue: Int,
        activity: FragmentActivity
    ) {
        when {
            inputField.text!!.isEmpty() -> {
                validationCounter++
                Log.println(
                    Log.WARN,
                    javaClass.toString(),
                    "Validation error in validateNumbers:1 with ${activity.resources.getString(fieldNameId)}"
                )

                inputField.error = activity.resources.getString(emptyId, activity.resources.getString(fieldNameId))
            }
            inputField.text!!.length > activity.resources.getInteger(fieldMaxLength) -> {
                validationCounter++
                Log.println(
                    Log.WARN,
                    javaClass.toString(),
                    "Validation error in validateNumbers:2 with ${activity.resources.getString(fieldNameId)}"
                )

                inputField.error = activity.resources.getQuantityString(
                    R.plurals.all_toolong,
                    activity.resources.getInteger(fieldMaxLength),
                    activity.resources.getInteger(fieldMaxLength),
                    activity.resources.getString(fieldNameId)
                )
            }
            inputField.text!!.toString().toDouble().toBigDecimal().setScale(2, RoundingMode.HALF_UP)
                .toDouble() < activity.resources.getInteger(fieldMinValue) -> {
                validationCounter++
                Log.println(
                    Log.WARN,
                    javaClass.toString(),
                    "Validation error in validateNumbers:3 with ${activity.resources.getString(fieldNameId)}"
                )

                inputField.error = activity.resources.getString(
                    R.string.add_pricetoolow,
                    activity.resources.getInteger(fieldMinValue),
                    activity.resources.getString(fieldNameId)
                )
            }
        }
    }
    fun validateNumbers(
        inputField: CurrencyEditText,
        emptyId: Int,
        fieldNameId: Int,
        fieldMaxLength: Int,
        fieldMinValue: Int,
        activity: FragmentActivity
    ) {
        when {
            inputField.text!!.isEmpty() -> {
                validationCounter++
                Log.println(
                    Log.WARN,
                    javaClass.toString(),
                    "Validation error in validateNumbers:1 with ${activity.resources.getString(fieldNameId)}"
                )

                inputField.error = activity.resources.getString(emptyId, activity.resources.getString(fieldNameId))
            }
            inputField.text!!.length > activity.resources.getInteger(fieldMaxLength) -> {
                validationCounter++
                Log.println(
                    Log.WARN,
                    javaClass.toString(),
                    "Validation error in validateNumbers:2 with ${activity.resources.getString(fieldNameId)}"
                )

                inputField.error = activity.resources.getQuantityString(
                    R.plurals.all_toolong,
                    activity.resources.getInteger(fieldMaxLength),
                    activity.resources.getInteger(fieldMaxLength),
                    activity.resources.getString(fieldNameId)
                )
            }
            inputField.getNumericValueBigDecimal().setScale(2, RoundingMode.HALF_UP)
                .toDouble() < activity.resources.getInteger(fieldMinValue) -> {
                validationCounter++
                Log.println(
                    Log.WARN,
                    javaClass.toString(),
                    "Validation error in validateNumbers:3 with ${activity.resources.getString(fieldNameId)}"
                )

                inputField.error = activity.resources.getString(
                    R.string.add_pricetoolow,
                    activity.resources.getInteger(fieldMinValue),
                    activity.resources.getString(fieldNameId)
                )
            }
        }
    }

    fun validateMatchingPasswords(
        passwordField1: TextInputEditText,
        passwordField2: TextInputEditText,
        wrongPasswordsId: Int,
        activity: FragmentActivity
    ) {
        when {
            passwordField1.text.toString() != passwordField2.text.toString() -> {
                validationCounter++
                passwordField2.error = activity.resources.getString(wrongPasswordsId)
            }
        }
    }

    fun validateChecked(checkBox: CheckBox, notCheckedId: Int, activity: FragmentActivity) {
        if (!checkBox.isChecked) {
            validationCounter++
            checkBox.error = activity.resources.getString(notCheckedId)
        } else {
            checkBox.error = null
        }
    }

    fun validateEmail(
        inputField: TextInputEditText,
        emptyId: Int,
        wrongFormatId: Int,
        emailTaken: Int,
        fieldNameId: Int,
        activity: FragmentActivity,
        serverUrl: String
    ) {
        val res: Resources = activity.resources
        when {
            inputField.text.isNullOrEmpty() -> {
                validationCounter++
                inputField.error = res.getString(emptyId, res.getString(fieldNameId))
            }
            !inputField.text!!.matches(Regex("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")) -> {
                validationCounter++
                inputField.error = res.getString(wrongFormatId, res.getString(fieldNameId))
            }
        }

        /*val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)*/
        val retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create()).build()

        val strRequestBody = inputField.text.toString()
        val requestBody = strRequestBody.toRequestBody("text/plain".toMediaTypeOrNull())

        var isPresent = true
        val api = retrofit.create(UsersAPI::class.java)
        api.checkIfExists(requestBody).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.isSuccessful) {
                    isPresent = response.body().toBoolean()
                }

                if (isPresent) {
                    validationCounter++
                    inputField.error = res.getString(emailTaken, res.getString(fieldNameId))
                }
                return
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.e("Register, unique email", t.toString())
                return
            }
        })
    }
}