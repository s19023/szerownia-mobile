package szerownia.mobile

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.auth0.android.jwt.JWT
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.TokenAPI
import szerownia.mobile.Fragments.*
import szerownia.mobile.Models.RefreshToken
import szerownia.mobile.Models.Token
import java.util.*

class MainActivity : AppCompatActivity() {
    var handler: Handler = Handler()
    var runnable: Runnable? = null
    var delay = 300000
    var position = 0;
    var previousPosition = 0
    private lateinit var bottomNavigationView: BottomNavigationView
    private var serverUrl = ""
    private var backgroundMessagesUrl = ""
    private var messagesWebSocketService = MessagesWebSocketService(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val globalVariable: GlobalVariable = applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(applicationContext, "localhost_url")!!
        }
        backgroundMessagesUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(applicationContext, "server_backgroundmessages_url")!!
        } else {
            Helper().getConfigValue(applicationContext, "localhost_backgroundmessages_url")!!
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)
        bottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationListener)
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, HomeFragment()).commit()
        val dm = resources.displayMetrics
        val conf = resources.configuration
        conf.setLocale(Locale("pl","PL"))
//        resources.updateConfiguration(conf,dm)

    }

    fun getMessagesWebSocketService(): MessagesWebSocketService {
        return messagesWebSocketService
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences = getSharedPreferences("logged", Context.MODE_PRIVATE)
        Log.i(javaClass.toString(), "Refreshing token onResume")
        refreshTokenAndRefreshSocket()

        if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
            try {
                val jwt = JWT(sharedPreferences.getString("TOKEN", null)!!)
                val role = jwt.getClaim("role").asString().toString()
                if (role in resources.getStringArray(R.array.Superusers)) {
                    val myIntent = Intent(this, AppraiserActivity::class.java)
                    startActivity(myIntent)
                }
            } catch (t: Exception) {
                Log.println(Log.ERROR, javaClass.toString(), "Error: $t")
            }
        }

        handler.postDelayed(Runnable {
            refreshTokenAndRefreshSocket()
            handler.postDelayed(runnable!!, delay.toLong())
        }.also { runnable = it }, delay.toLong())
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable!!)
    }

    private val navigationListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val sharedPreferences = getSharedPreferences("logged", Context.MODE_PRIVATE)
        var selectedFragment: Fragment? = null
        previousPosition = position
        when (item.itemId) {
            R.id.menu_home -> {
                position = 0
                selectedFragment = HomeFragment()
            }
            R.id.menu_messages -> {
                position = 3
                if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                    selectedFragment = ThreadsFragment()
                } else {
                    selectedFragment = UserNotLoggedInFragment()
                }
            }
            R.id.menu_browse -> {
                position = 1
                selectedFragment = BrowseFragment()
            }
            R.id.menu_add -> {
                position = 2
                if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                    val dialogBuilder = AlertDialog.Builder(this)
                    val view = LayoutInflater.from(this).inflate(R.layout.popup_add_new_rent, null)
                    val closeButton: ImageView = view.findViewById(R.id.imageview_popup_close)

                    val rentalAdIcon: ImageView = view.findViewById(R.id.imageview_popup_rentalad)
                    val searchAdIcon: ImageView = view.findViewById(R.id.imageview_popup_searchad)

                    dialogBuilder.setView(view)
                    val dialog = dialogBuilder.create()
                    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.show()

                    rentalAdIcon.setOnClickListener {
                        selectedFragment = AddFragment()
                        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment!!)
                            .commit()
                        dialog.dismiss()
                    }

                    searchAdIcon.setOnClickListener {
                        //przekierowanie do fragmemtu dodawania searchAd
                        selectedFragment = AddSearchFragment()
                        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment!!)
                            .commit()
                        dialog.dismiss()
                    }

                    closeButton.setOnClickListener {
                        dialog.dismiss()
                    }

                    return@OnNavigationItemSelectedListener true

                } else {
                    selectedFragment = UserNotLoggedInFragment()
                }
            }
            R.id.menu_account -> {
                position = 4
                selectedFragment = if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                    AccountFragment()
                } else {
                    LoginFragment()
                }
            }
        }

        if (selectedFragment != null) {
            showFragment(position, previousPosition, selectedFragment!!)
            return@OnNavigationItemSelectedListener true
        }

        return@OnNavigationItemSelectedListener false
    }

    private fun showFragment(position: Int, previousPosition: Int, selectedFragment: Fragment) {

        if (position > previousPosition) {
            supportFragmentManager.beginTransaction().setCustomAnimations(
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_left,
                R.anim.slide_in_from_right,
                R.anim.slide_out_to_right
            ).replace(R.id.fragment_container, selectedFragment).commit()
        } else if (position < previousPosition) {
            supportFragmentManager.beginTransaction().setCustomAnimations(
                R.anim.slide_in_from_right,
                R.anim.slide_out_to_right,
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_left
            ).replace(R.id.fragment_container, selectedFragment).commit()
        } else {
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit()
        }

    }

    private fun refreshTokenAndRefreshSocket() {
        val sharedPreferences = getSharedPreferences("logged", Context.MODE_PRIVATE)
        if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
            messagesWebSocketService.closeConnection()

            Log.i(javaClass.toString(), "Refreshing token")
            val gson = GsonBuilder().create()
            val retrofit =
                Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()

            val token = sharedPreferences.getString("TOKEN", null)
            val rfToken = sharedPreferences.getString("REFRESH_TOKEN", null)
            var refreshToken = RefreshToken(token!!, rfToken!!)

            val api = retrofit.create(TokenAPI::class.java)

            api.refreshToken(refreshToken).enqueue(object : Callback<Token> {
                override fun onResponse(call: Call<Token>, response: Response<Token>) {
                    println(response.code())

                    if (response.body() != null) {
                        val editor = sharedPreferences.edit()
                        editor.apply {
                            putString("TOKEN", response.body()!!.token)
                            putString("REFRESH_TOKEN", response.body()!!.refreshToken)
                        }.apply()

                        messagesWebSocketService.openConnection(applicationContext)

                    } else {
                        val editor = sharedPreferences.edit()
                        editor.apply {
                            putBoolean("LOGGED_KEY", false)
                        }.apply()
                    }

                }

                override fun onFailure(call: Call<Token>, t: Throwable) {
                    Log.e(javaClass.toString(), "Refreshing token failed", t)
                    Toast.makeText(applicationContext, "Błąd odświeżania tokenu", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }
}
