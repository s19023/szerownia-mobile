package szerownia.mobile

import android.content.Context
import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket

class MessagesWebSocketService(private val activity: MainActivity) {

    private lateinit var client: OkHttpClient
    private lateinit var request: Request
    private lateinit var listener: MessageWebSocketListener
    private lateinit var ws: WebSocket

    private var backgroundMessagesUrl = ""

    fun openConnection(applicationContext: Context) {
        val globalVariable: GlobalVariable = applicationContext as GlobalVariable

        backgroundMessagesUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(applicationContext, "server_backgroundmessages_url")!!
        } else {
            Helper().getConfigValue(applicationContext, "localhost_backgroundmessages_url")!!
        }

        val sharedPreferences = applicationContext.getSharedPreferences("logged", Context.MODE_PRIVATE)

        Log.println(Log.INFO, javaClass.toString(), "Opening messages socket")

        try {
            client = OkHttpClient()
            request = Request.Builder()
                .url(backgroundMessagesUrl + "msg/ws/messages?id=" + sharedPreferences.getString("TOKEN", null)).build()
            listener = MessageWebSocketListener(applicationContext, activity)

            ws = client.newWebSocket(request, listener)
            Log.println(Log.INFO, javaClass.toString(), "Messages socet created")
        } catch (e: Exception) {
            Log.e(javaClass.toString(),"Opening socket failed", e)
        }
    }

    fun closeConnection() {
        Log.println(Log.INFO, javaClass.toString(), "Closing messages socket")

        try {
            ws.close(1000, "closing socket, user logging out")
        } catch (e: Exception) {
            Log.e(javaClass.toString(),"Closing socket failed", e)
        }
    }

    fun updateConnection(applicationContext: Context) {
        Log.println(Log.INFO, javaClass.toString(), "Restarting messages socket")

        try {
            ws.close(1012, "service restarting, refreshing token")

            Thread.sleep(5000)
            openConnection(applicationContext)
        } catch (e: Exception) {
            Log.w(javaClass.toString(),"Restarting socket failed", e)
            openConnection(applicationContext)
        }
    }

}
