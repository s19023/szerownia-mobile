package szerownia.mobile

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView

class LoadingDialog(private val view: View, private val loadingMessage: String) {

    private lateinit var dialog: Dialog;

    fun showLoadingDialog() {
        val dialogBuilder = AlertDialog.Builder(view.context)
        val v = LayoutInflater.from(view.context).inflate(R.layout.popup_all_loading, null)

        val loadingMessage: TextView = v.findViewById(R.id.textview_loading_message)
        loadingMessage.text = this.loadingMessage;

        dialogBuilder.setView(v)
        dialogBuilder.setCancelable(false)
        dialog = dialogBuilder.create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    fun dismissLoadingDialog() {
        dialog.dismiss()
    }

}
