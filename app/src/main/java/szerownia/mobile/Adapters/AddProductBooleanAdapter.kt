package szerownia.mobile.Adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Models.Feature
import szerownia.mobile.Models.Filter
import szerownia.mobile.Models.PostFeatureValue
import szerownia.mobile.Models.ProductCreateRequestDTO
import szerownia.mobile.R

class AddProductBooleanAdapter() : RecyclerView.Adapter<AddProductBooleanAdapter.ViewHolder>() {

    var features: List<Feature> = listOf()
    val postFeatures: HashMap<Feature, PostFeatureValue> = HashMap()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val checkBox :CheckBox=  itemView.findViewById(R.id.checkBox_search)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_element_boolean, parent, false)


        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return features.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.checkBox.text = features[position].name
        Log.i("feature", features[position].toString())
        postFeatures[features[position]] = PostFeatureValue(holder.checkBox.isChecked,null,null,features[position].idFeature,null)


        holder.checkBox.setOnCheckedChangeListener { _, isChecked ->
            postFeatures[features[position]] = PostFeatureValue(isChecked,null,null, features[position].idFeature,null)
        }


    }

}