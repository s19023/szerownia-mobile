package szerownia.mobile.Adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.SearchAPI
import szerownia.mobile.Fragments.BrowseFragment
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.Models.CategoryReference
import szerownia.mobile.Models.SearchRequest
import szerownia.mobile.R

class SubcategoriesAdapter(private val subcategories: List<CategoryReference>,
                           val parentCategory: Long) : RecyclerView.Adapter<SubcategoriesAdapter.ViewHolder>() {

    private var serverUrl =""

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val subcategoryName: TextView = itemView.findViewById(R.id.subcategoryName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_subcategories_element, parent, false)
        val globalVariable:GlobalVariable = parent.context.applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(parent.context.applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(parent.context.applicationContext, "localhost_url")!!
        }

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.subcategoryName.text = subcategories[position].name

        holder.subcategoryName.setOnClickListener { v: View ->
            val gson = GsonBuilder().create()
            val retrofit = Retrofit.Builder().baseUrl(serverUrl)
                .addConverterFactory(GsonConverterFactory.create(gson)).build()

            val api = retrofit.create(SearchAPI::class.java)
//            subcategories[position].idCategory
            val searchRequest = SearchRequest(subcategories[position].idCategory, mutableListOf(), "")

//            api.findByParams(searchRequest).enqueue(object: Callback<List<RentalAd>> {
//                override fun onFailure(call: Call<List<RentalAd>>, t: Throwable) {
//
//                }
//
//                override fun onResponse(call: Call<List<RentalAd>>, response: Response<List<RentalAd>>) {
//
//                    val products = response.body()

            val browseFragment = BrowseFragment()
            val bundle = Bundle()

            bundle.putString("searchRequest", gson.toJson(searchRequest))
            bundle.putBoolean("filtered", true)
            browseFragment.arguments = bundle


            (v.context as FragmentActivity).supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, browseFragment).commit()
//
//                }
//            })
//            val categorySearchFragment = CategorySearchFragment()
//            val bundle = Bundle()
//            bundle.putLong("parentCategory",parentCategory)
//            bundle.putLong("idCategory", subcategories[position].idCategory)
//            categorySearchFragment.arguments = bundle
//
//            (v.context as FragmentActivity).supportFragmentManager.beginTransaction()
//                .replace(R.id.categoriesContainer, categorySearchFragment).commit()
        }
    }

    override fun getItemCount(): Int {
        return subcategories.size
    }
}