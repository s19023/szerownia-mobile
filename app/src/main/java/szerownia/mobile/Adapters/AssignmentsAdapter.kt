package szerownia.mobile.Adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text
import szerownia.mobile.Fragments.Appraiser.AssignedAppraisalFragment
import szerownia.mobile.Models.Appraisal
import szerownia.mobile.Models.Assignment
import szerownia.mobile.R
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class AssignmentsAdapter(private val assignments: MutableList<Appraisal>) : RecyclerView.Adapter<AssignmentsAdapter.ViewHolder>(){

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardView:CardView =itemView.findViewById(R.id.appraisalCardView)
        val title:TextView=itemView.findViewById(R.id.asignmentTitle)
        val assigner: TextView = itemView.findViewById(R.id.asigner)
        val date: TextView = itemView.findViewById(R.id.dateOfAssignment)
        val policy: TextView = itemView.findViewById(R.id.policyValue)
        val circumstances:TextView=itemView.findViewById(R.id.CircumstancesValue)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_to_appraise, parent, false)
        return AssignmentsAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.cardView.setOnClickListener {
                v: View ->
            /**sending data to rental ad details**/
            val nextFragment = AssignedAppraisalFragment()
            val bundle = Bundle()
            bundle.putLong("id", assignments[position].id)
            bundle.putLong("value", assignments[position].policy.insuranceTotal.toLong())
            nextFragment.arguments = bundle

            /**opening rental ad details fragment**/
            (v.context as FragmentActivity).supportFragmentManager.beginTransaction().setCustomAnimations(
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_left,
                R.anim.slide_in_from_right,
                R.anim.slide_out_to_right
            ).replace(R.id.appraiserFragmentContainer, nextFragment).commit()
        }
        val dateFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy")
        holder.title.text=assignments[position].title
        holder.assigner.text = assignments[position].user.firstName + " " + assignments[position].user.lastName
        holder.date.text = assignments[position].dateOfAssignToTheAppraiser.toString()
        holder.policy.text =String.format(Locale.getDefault(),assignments[position].policy.insuranceTotal.toString())
        holder.circumstances.text = assignments[position].circumstances


        }

    override fun getItemCount(): Int {
        return assignments.size
    }

}

