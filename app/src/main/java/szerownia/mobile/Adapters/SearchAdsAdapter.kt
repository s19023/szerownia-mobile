package szerownia.mobile.Adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Fragments.SearchAdDetailsFragment
import szerownia.mobile.Models.SearchAd
import szerownia.mobile.R

class SearchAdsAdapter(
    private var searchAds: ArrayList<SearchAd>
) : RecyclerView.Adapter<SearchAdsAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val adTitle: TextView = itemView.findViewById(R.id.name)
        val itemPicture: ImageView = itemView.findViewById(R.id.itemPicture)
        val locPicture: ImageView = itemView.findViewById(R.id.locationIcon2)
        val location: TextView = itemView.findViewById(R.id.loc)
        val cardView: CardView = itemView.findViewById(R.id.cardview_item_searchad)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_searchads, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.adTitle.text = searchAds[position].title
        holder.locPicture.setImageResource(R.drawable.all_location_icon)
        holder.location.text = searchAds[position].location
        holder.cardView.setOnClickListener { v: View ->
            val searchAdDetailsFragment = SearchAdDetailsFragment()
            val bundle = Bundle()
            println(searchAds[position].id)
            bundle.putLong("id", searchAds[position].id)
            searchAdDetailsFragment.arguments = bundle

            (v.context as FragmentActivity).supportFragmentManager.beginTransaction().setCustomAnimations(
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_left,
                R.anim.slide_in_from_right,
                R.anim.slide_out_to_right
            ).replace(R.id.fragment_container, searchAdDetailsFragment).commit()
        }
    }

    override fun getItemCount(): Int {
        return searchAds.size
    }
}
