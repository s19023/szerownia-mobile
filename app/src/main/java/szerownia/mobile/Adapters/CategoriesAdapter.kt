package szerownia.mobile.Adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Fragments.SubcategoriesFragment
import szerownia.mobile.Models.Category
import szerownia.mobile.R

class CategoriesAdapter(private val categories: List<Category>) : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val categoryName: TextView = itemView.findViewById(R.id.categoryName)
        val categoryIcon: ImageView = itemView.findViewById(R.id.categoryIcon)
        val item: CardView = itemView.findViewById(R.id.categoryCardView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_categories_main_categories_element, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val categoryName = categories[position].name
        holder.categoryName.text = categoryName

        when (categoryName) {
            "Motoryzacja"       -> holder.categoryIcon.setImageResource(R.drawable.categories_motoryzacja_icon)
            "Dom i Ogród"       -> holder.categoryIcon.setImageResource(R.drawable.categories_dom_i_ogrod_icon)
            "Elektronika"       -> holder.categoryIcon.setImageResource(R.drawable.categories_elektronika_icon)
            "Moda"              -> holder.categoryIcon.setImageResource(R.drawable.categories_moda_icon)
            "Rolnictwo"         -> holder.categoryIcon.setImageResource(R.drawable.categories_rolnictwo_icon)
            "Dla Dzieci"        -> holder.categoryIcon.setImageResource(R.drawable.categories_dla_dzieci_icon)
            "Sport i Hobby"     -> holder.categoryIcon.setImageResource(R.drawable.categories_sport_i_hobby_icon)
            "Muzyka i Edukacja" -> holder.categoryIcon.setImageResource(R.drawable.categories_muzyka_i_edukacja_icon)
            "Ślub i Wesele"     -> holder.categoryIcon.setImageResource(R.drawable.categories_slub_i_wesele_icon)

            else                -> holder.categoryIcon.setImageResource(R.drawable.categories_unknown_icon)
        }

        holder.item.setOnClickListener { v: View ->
            val subcategoriesFragment = SubcategoriesFragment()
            val bundle = Bundle()
            bundle.putLong("idCategory", categories[position].idCategory)
            subcategoriesFragment.arguments = bundle

            (v.context as FragmentActivity).supportFragmentManager.beginTransaction()
                .replace(R.id.categoriesContainer, subcategoriesFragment).commit()
        }
    }
}
