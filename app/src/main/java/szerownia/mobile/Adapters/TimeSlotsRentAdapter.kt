package szerownia.mobile.Adapters

import android.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.timessquare.CalendarPickerView
import szerownia.mobile.Fragments.RentFragment
import szerownia.mobile.Models.TimeSlot
import szerownia.mobile.R
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class TimeSlotsRentAdapter(private val timeSlots: MutableList<TimeSlot>,val rf:RentFragment) :
    RecyclerView.Adapter<TimeSlotsRentAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dateFrom: TextView = itemView.findViewById(R.id.dateFrom)
        val dateTo: TextView = itemView.findViewById(R.id.dateTo)
        val cardView:CardView= itemView.findViewById(R.id.rentDateRangeCardView)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_time_slot_rent, parent, false)
        return TimeSlotsRentAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dateFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy")

        holder.dateFrom.text = dateFormat.format(timeSlots[position].startDate)
        if (timeSlots[position].endDate != null) {
            holder.dateTo.text = dateFormat.format(timeSlots[position].endDate)
        } else {
            holder.dateTo.text = "----"
        }
        holder.cardView.setOnClickListener {
            val dateFrom= Calendar.getInstance()
            val dateTo = Calendar.getInstance()
            dateFrom.time=timeSlots[position].startDate
            if(timeSlots[position].endDate!=null){
                dateTo.time=timeSlots[position].endDate
            }else{
                dateTo.time=timeSlots[position].startDate
                dateTo.add(Calendar.WEEK_OF_YEAR,2)
            }

            val dialogBuilder = AlertDialog.Builder(holder.cardView.context)
            val addDateView = LayoutInflater.from(holder.cardView.context).inflate(R.layout.popup_date_range_picker, null)
            val closeButton: ImageView = addDateView.findViewById(R.id.dateRangeCloseButton)
            val calendarPicker: CalendarPickerView = addDateView.findViewById(R.id.addDateRangeCalendarPicker)
            val confirmButton: Button = addDateView.findViewById(R.id.confirmDateRange)
            calendarPicker.init(dateFrom.time, dateTo.time).inMode(CalendarPickerView.SelectionMode.RANGE)

            dialogBuilder.setView(addDateView)
            val dialog = dialogBuilder.create()
            dialog.show()

            closeButton.setOnClickListener {
                dialog.dismiss()
            }

            confirmButton.setOnClickListener {
                rf.pickedTimeSlot=
                    TimeSlot(
                        calendarPicker.selectedDates[calendarPicker.selectedDates.size - 1],
                        calendarPicker.selectedDates[0])
                rf.dateSelected=true
                rf.changeVisibility()
                rf.dateTo.time=calendarPicker.selectedDates[calendarPicker.selectedDates.size-1]
                rf.dateFrom.time=calendarPicker.selectedDates[0]


                dialog.dismiss()
            }

            calendarPicker.setOnDateSelectedListener(object : CalendarPickerView.OnDateSelectedListener {
                override fun onDateSelected(date: Date?) {
                    if (calendarPicker.selectedDates.size > 1) {
                        confirmButton.visibility = View.VISIBLE
                    }
                }

                override fun onDateUnselected(date: Date?) {
                    confirmButton.visibility = View.GONE
                }
            })

        }


    }

    override fun getItemCount(): Int {
        return timeSlots.size
    }

}