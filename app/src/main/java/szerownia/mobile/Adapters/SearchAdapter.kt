package szerownia.mobile.Adapters

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import szerownia.mobile.Models.Feature
import szerownia.mobile.Models.Filter
import szerownia.mobile.R

class SearchAdapter() : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    var features: List<Feature> = listOf()
    val filters: HashMap<Feature, Filter> = HashMap()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val field: TextInputLayout = itemView.findViewById(R.id.searchField)
        val feature: EditText = itemView.findViewById(R.id.feature)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_element, parent, false)


        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return features.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.field.hint = features[position].name
        Log.i("feature",features[position].toString())
        if(features[position].type=="DOUBLE"||features[position].type=="INTEGER"){
            holder.feature.inputType = InputType.TYPE_CLASS_NUMBER
        }

        holder.feature.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
                if (features[position].type == "TEXT") filters.put(features[position],
                                                                   Filter(null, holder.feature.text.toString(),null,
                                                                                     features[position].idFeature,null))

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }

}