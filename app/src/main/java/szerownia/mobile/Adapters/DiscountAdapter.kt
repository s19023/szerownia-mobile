package szerownia.mobile.Adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Models.Discount
import szerownia.mobile.Models.MyProduct
import szerownia.mobile.R

class DiscountAdapter(
    private val discounts: List<Discount>
) : RecyclerView.Adapter<DiscountAdapter.ViewHolder>() {
    val checkedProducts = ArrayList<Int>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val threshold: TextView = itemView.findViewById(R.id.discountThreshold)
        val value: TextView = itemView.findViewById(R.id.discountValue)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_discount, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.threshold.text="Minimalna Ilość Dni: "+discounts[position].minNumDays
        holder.value.text="Wartość Zniżki: "+discounts[position].value+"%"
    }

    override fun getItemCount(): Int {
        return discounts.size
    }
}
