package szerownia.mobile.Adapters

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.R

class AppraisalPhotosAdapter(private val photos:MutableList<Bitmap>): RecyclerView.Adapter<AppraisalPhotosAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val photo: ImageView = itemView.findViewById(R.id.appraisal_photo)
        val title: TextView = itemView.findViewById(R.id.appraisalPhotoInput)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppraisalPhotosAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_appraiser_photo, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val photo = photos[position]
        holder.photo.setImageBitmap(photo)
        holder.photo.setOnClickListener {
            if(photos.size>1) {
                photos.remove(photo)
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int {
        return  photos.size
    }


}