package szerownia.mobile.Adapters

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Models.FeatureOccurrence
import szerownia.mobile.R

class FeaturesAdapter(
    private val features: List<FeatureOccurrence>) : RecyclerView.Adapter<FeaturesAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val featureName: TextView = itemView.findViewById(R.id.featureName)
        val featureValue: TextView = itemView.findViewById(R.id.featureValue)
        //val item: CardView = itemView.findViewById(R.id.featureCardView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_popup_item_features_element, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        println(features[position].toString())
        holder.featureName.text = features[position].featureName
        Log.e("Features", features[position].toString())
        if (features[position].textValue != null) holder.featureValue.text = features[position].textValue
        else if (features[position].decimalNumberValue != 0) holder.featureValue.text = features[position].decimalNumberValue.toString()
        else if (features[position].floatingNumberValue != 0.0) holder.featureValue.text = features[position].floatingNumberValue.toString()
        else {
            if (features[position].booleanValue) holder.featureValue.text = "Tak"
            else holder.featureValue.text = "Nie"
        }
    }

    override fun getItemCount(): Int {
        return features.size
    }
}