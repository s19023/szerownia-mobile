package szerownia.mobile.Adapters

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import szerownia.mobile.Fragments.RentalAdDetailsFragment
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.Models.RentalAd
import szerownia.mobile.R
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId

class MyAdsAdapter(
    private var rentalAds: ArrayList<RentalAd>, private val images: List<Int>, private val locImages: List<Int>
) : RecyclerView.Adapter<MyAdsAdapter.ViewHolder>() {
    private var serverUrl = ""

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val adTitle: TextView = itemView.findViewById(R.id.textview_item_myads_title)
        val itemPicture: ImageView = itemView.findViewById(R.id.imageview_item_myads)
        val locPicture: ImageView = itemView.findViewById(R.id.imageview_item_myads_locicon)
        val pricePerDay: TextView = itemView.findViewById(R.id.textview_item_myads_price)
        val location: TextView = itemView.findViewById(R.id.textview_item_myads_location)
        val cardView: CardView = itemView.findViewById(R.id.cardview_item_myads)
        val date: TextView = itemView.findViewById(R.id.textview_item_myads_datefrom)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAdsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_myads, parent, false)
        val globalVariable: GlobalVariable = parent.context.applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(parent.context.applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(parent.context.applicationContext, "localhost_url")!!
        }
        return MyAdsAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return rentalAds.size
    }

    @SuppressLint("SetTextI18n") override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.adTitle.text = rentalAds[position].title
        holder.itemPicture.setImageResource(images[position])
        holder.locPicture.setImageResource(locImages[position])
        holder.location.text = rentalAds[position].location
        val localDate: LocalDate = Instant.ofEpochMilli(
            rentalAds[position].timeSlotList[0].startDate.time
        ).atZone(ZoneId.systemDefault()).toLocalDate()
        holder.date.text = "od: $localDate"

        if (rentalAds[position].imagesIdList != null) {
            if (rentalAds[position].imagesIdList.isNotEmpty()) {
                Glide.with(holder.itemView).load(serverUrl + "api/image/" + rentalAds[position].imagesIdList[0])
                    .diskCacheStrategy(
                        DiskCacheStrategy.RESOURCE
                    ).into(holder.itemPicture)
            }
        }

        val price: Double = rentalAds[position].pricePerDay
        if (price.rem(1).equals(0.0)) {
            holder.pricePerDay.text = price.toInt().toString() + " zł"
        } else {
            holder.pricePerDay.text = price.toString() + "0 zł"
        }

        holder.cardView.setOnClickListener { v: View ->
            /**sending data to rental ad details**/
            val rentalAdDetailsFragment = RentalAdDetailsFragment()
            val bundle = Bundle()
            bundle.putLong("id", rentalAds[position].id)
            rentalAdDetailsFragment.arguments = bundle

            /**opening rental ad details fragment**/
            (v.context as FragmentActivity).supportFragmentManager.beginTransaction().setCustomAnimations(
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_left,
                R.anim.slide_in_from_right,
                R.anim.slide_out_to_right
            ).replace(R.id.fragment_container, rentalAdDetailsFragment).commit()
        }

    }

}