package szerownia.mobile.Adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.annotation.LayoutRes
import szerownia.mobile.Models.Category

class CategoryChoiceAdapter(context: Context, @LayoutRes private val layoutResource: Int,
                            private val categories: List<Category>) : ArrayAdapter<Category>(context, layoutResource,
                                                                                             categories) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val currentCategory = categories[position]
        if (currentCategory.abstract) {

        }


        return super.getView(position, convertView, parent)

    }
}