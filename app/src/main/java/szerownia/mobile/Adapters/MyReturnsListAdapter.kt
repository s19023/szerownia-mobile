package szerownia.mobile.Adapters

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.textfield.TextInputEditText
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import szerownia.mobile.API.HireAPI
import szerownia.mobile.API.OpinionAPI
import szerownia.mobile.API.RentalAdAPI
import szerownia.mobile.Fragments.MyBorrowsFragment
import szerownia.mobile.Fragments.MySharesFragment
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.Borrow
import szerownia.mobile.Models.Opinion
import szerownia.mobile.Models.RentalAdById
import szerownia.mobile.R
import java.time.LocalDate

class MyReturnsListAdapter(
    private val myBorrows: List<Borrow>, private val retrofit: Retrofit
) : RecyclerView.Adapter<MyReturnsListAdapter.ViewHolder>() {

    private var serverUrl =""

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val moreButton: ImageButton = itemView.findViewById(R.id.moreButton)
        val ratingReceived: RatingBar = itemView.findViewById(R.id.ratingBarGivenRating)
        val ratingGiven: RatingBar = itemView.findViewById(R.id.ratingBarReceivedRating)
        val noRatingYet: TextView = itemView.findViewById(R.id.noRatingYet)
        val itemPicture: ImageView = itemView.findViewById(R.id.itemPicture)
        val date: TextView = itemView.findViewById(R.id.rentDateFrom)
        val cardView: CardView= itemView.findViewById(R.id.cardview_item_searchad)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_borrow_element, parent, false)
        val globalVariable: GlobalVariable = parent.context.applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(parent.context.applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(parent.context.applicationContext, "localhost_url")!!
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.moreButton.visibility = View.GONE
        holder.date.text = "wypożyczone: " + myBorrows[position].dateFrom
        val api0 = retrofit.create(RentalAdAPI::class.java)

        api0.getAd(myBorrows[position].ad.id).enqueue(object : Callback<RentalAdById> {
            override fun onResponse(call: Call<RentalAdById>, response: Response<RentalAdById>) {

                Log.i("Response", response.body().toString())
                val imageId = response.body()!!.imagesIdList[0]

                Glide.with(holder.itemView).load(serverUrl+"api/image/$imageId").diskCacheStrategy(
                    DiskCacheStrategy.RESOURCE).into(holder.itemPicture)
                Log.i("imageId", imageId.toString())
                return
            }

            override fun onFailure(call: Call<RentalAdById>, t: Throwable) {
                Log.e("Error:", t.message.toString());
                return
            }
        })

        holder.name.text = myBorrows[position].ad.title
        holder.moreButton.setOnClickListener {
            val popup: PopupMenu = PopupMenu(it.context, holder.moreButton)
            popup.setOnMenuItemClickListener {
                val date: LocalDate = LocalDate.now()
                val api = retrofit.create(HireAPI::class.java)
                api.returnBorrow(myBorrows[position].id, date.toString().split('T')[0])
                    .enqueue(object : Callback<Void> {
                        override fun onResponse(call: Call<Void>, response: Response<Void>) {
                            Log.i("response", response.toString())
                        return
                    }

                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        throw t
                    }
                })

                true
            }
            popup.inflate(R.menu.return_item)
            popup.show()
        }
        var idOpinionGot = -1
        for (opinion in myBorrows[position].opinions){
            if(opinion.aboutSharer){
                holder.ratingGiven.visibility= View.VISIBLE
                holder.noRatingYet.visibility = View.GONE
                Log.e("Opinion given"+opinion.id, opinion.rating.toFloat().toString())
                holder.ratingGiven.rating=opinion.rating.toFloat()
            }else{
                Log.e("Opinion recieved"+opinion.id, opinion.rating.toFloat().toString())
                holder.ratingReceived.rating=opinion.rating.toFloat()
                idOpinionGot=opinion.id.toInt()
            }
        }


        holder.cardView.setOnClickListener { view ->

            val popup: PopupMenu = PopupMenu(view.context, holder.moreButton)
            popup.setOnMenuItemClickListener {itemClicked->
                when(itemClicked.itemId){
//                    R.id.rentDetails ->{
//
//                    }
                    R.id.viewOpinion ->{
                        val dialogBuilder = AlertDialog.Builder(view.context)
                        val view = LayoutInflater.from(view.context).inflate(R.layout.popup_rate, null)
                        val closeButton: ImageButton = view.findViewById(R.id.rateCloseButton)
                        val input: TextInputEditText = view.findViewById(R.id.ratingInput)
                        val ratingBar: RatingBar = view.findViewById(R.id.ratingbar_opinion)
                        val submitButton: Button = view.findViewById(R.id.submitRating)
                        for(opinion in myBorrows[position].opinions){
                            if(opinion.aboutSharer){
                                input.setText(opinion.comment)
                                ratingBar.rating=opinion.rating.toFloat()
                            }
                        }
                        submitButton.visibility=View.GONE
                        input.isClickable=false
                        input.isCursorVisible=false
                        input.isFocusable=false
                        input.isFocusableInTouchMode=false




                        dialogBuilder.setView(view)
                        val dialog = dialogBuilder.create()
                        dialog.show()
                        closeButton.setOnClickListener {
                            dialog.dismiss() }
                    }
//                    R.id.returnItem ->{
//                        val date: LocalDate = LocalDate.now()
//                        val api = retrofit.create(HireAPI::class.java)
//                        api.returnBorrow(myBorrows[position].id, date.toString().split('T')[0])
//                            .enqueue(object : Callback<Void> {
//                                override fun onResponse(call: Call<Void>, response: Response<Void>) {
//                                    Log.i("response", response.toString())
//                                    return
//                                }
//
//                                override fun onFailure(call: Call<Void>, t: Throwable) {
//                                    throw t
//                                }
//                            })
//
//                    }
                    R.id.postOpinion ->{
                        val dialogBuilder = AlertDialog.Builder(view.context)
                        val view = LayoutInflater.from(view.context).inflate(R.layout.popup_rate, null)
                        val closeButton: ImageButton = view.findViewById(R.id.rateCloseButton)
                        val input:TextInputEditText = view.findViewById(R.id.ratingInput)

                        val ratingBar: RatingBar = view.findViewById(R.id.ratingbar_opinion)
                        val submitButton: Button = view.findViewById(R.id.submitRating)

                        for(opinion in myBorrows[position].opinions){
                            if(!opinion.aboutSharer){
                                input.setText(opinion.comment)
                                ratingBar.rating=opinion.rating.toFloat()
                            }
                        }

                        dialogBuilder.setView(view)
                        val dialog = dialogBuilder.create()
                        dialog.show()
                        closeButton.setOnClickListener {
                            dialog.dismiss() }

                        submitButton.setOnClickListener {
                            val loadingDialog = LoadingDialog(view, "Wysyłanie Opini...")
                            loadingDialog.showLoadingDialog()

                            val api1 = retrofit.create(OpinionAPI::class.java)
                            if(idOpinionGot<=0) {
                                var opinionId: Long = -1
                                api1.getAllOpinions().enqueue(object : Callback<List<Opinion>> {
                                    override fun onResponse(call: Call<List<Opinion>>, response: Response<List<Opinion>>) {
                                        if (response.isSuccessful) {
                                            opinionId = (response.body()!!.size + 1).toLong()
                                            return
                                        }
                                    }

                                    override fun onFailure(call: Call<List<Opinion>>, t: Throwable) {
                                        Log.e("Getting Opinions", t.toString())
                                        return
                                    }
                                })
                                val opinion = Opinion(
                                    false,
                                    myBorrows[position].sharer.id,
                                    input.text.toString(),
                                    myBorrows[position].id,
                                    opinionId,
                                    ratingBar.rating.toInt(),
                                    myBorrows[position].borrower.id

                                )
                                Log.e("Opinion", opinion.toString())
                                api1.createOpinion(opinion).enqueue(object : Callback<Void> {
                                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                        dialog.dismiss()
                                        Toast.makeText(it.context, "Opinia Wysłana", Toast.LENGTH_SHORT).show()
                                        Log.i("Sending Opinion", "Opinion Sent")
                                        loadingDialog.dismissLoadingDialog()
                                        val nextFragment = MySharesFragment()
                                        (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
                                            .replace(R.id.fragment_container, nextFragment).commit()
                                    }

                                    override fun onFailure(call: Call<Void>, t: Throwable) {
                                        Log.e("Sending Opinion", t.toString())
                                        dialog.dismiss()
                                        loadingDialog.dismissLoadingDialog()
                                    }
                                })
                            }else{

                                val opinion = Opinion(
                                    false,
                                    myBorrows[position].sharer.id,
                                    input.text.toString(),
                                    myBorrows[position].id,
                                    idOpinionGot.toLong(),
                                    ratingBar.rating.toInt(),
                                    myBorrows[position].borrower.id

                                )
                                api1.updateOpinion(opinion).enqueue(object : Callback<Void>{
                                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                        dialog.dismiss()
                                        Toast.makeText(it.context, "Opinia Uaktualniona", Toast.LENGTH_SHORT).show()
                                        Log.i("Sending Opinion", "Opinion Sent")
                                        loadingDialog.dismissLoadingDialog()
                                        val nextFragment = MyBorrowsFragment()
                                        (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
                                            .replace(R.id.fragment_container, nextFragment).commit()
                                    }

                                    override fun onFailure(call: Call<Void>, t: Throwable) {
                                        Log.e("Sending Opinion", t.toString())
                                        dialog.dismiss()
                                        loadingDialog.dismissLoadingDialog()
                                    }
                                })
                            }
                        }
                    }
                }



                true
            }
            var isAboutSharer = false
            for(opinion in myBorrows[position].opinions){
                if(opinion.aboutSharer){
                    isAboutSharer=true
                }
            }
            if(isAboutSharer){
                popup.inflate(R.menu.no_return_item_no_details)
            }else{
                popup.inflate(R.menu.no_return_item_without_opinion_no_details)
            }

            popup.show()
        }


//        holder.productNameCheckBox.text = myProducts[position].name
//        holder.model.text = myProducts[position].model
//        holder.make.text = myProducts[position].make
    }

    override fun getItemCount(): Int {
        return myBorrows.size
    }
}
