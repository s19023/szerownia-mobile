package szerownia.mobile.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.view.get
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.RentalAdAPI
import szerownia.mobile.API.UsersAPI
import szerownia.mobile.Fragments.ChatFragment
import szerownia.mobile.MainActivity
import szerownia.mobile.Models.RentalAdById
import szerownia.mobile.Models.Thread
import szerownia.mobile.Models.UserRespDto
import szerownia.mobile.R
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit

class ThreadsAdapter(private val threads: List<Thread>, private val serverUrl: String, private val token: String?, private val parentActivity: FragmentActivity) :
    RecyclerView.Adapter<ThreadsAdapter.ViewHolder>() {

    private var notificationCounter = 0

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val threadRentalAdTitle: TextView = itemView.findViewById(R.id.ThreadRentalAdTitle)
        val threadLastMessage: TextView = itemView.findViewById(R.id.ThreadLastMessage)
        val threadSendDate: TextView = itemView.findViewById(R.id.ThreadSendDate)
        val threadsImage: ImageView = itemView.findViewById(R.id.threads_image)
        val cardView: CardView = itemView.findViewById(R.id.threadCardView)
        val unreadCounter: TextView = itemView.findViewById(R.id.textview_threads_unreadcounter)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_threads, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n") override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var lastMessage = threads[position].lastMessage
        if (lastMessage.length > 40) lastMessage = lastMessage.substring(0, 40) + "..."
        holder.threadLastMessage.text = lastMessage

        val okHttpClient =
            OkHttpClient().newBuilder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()
        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
        val retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient).build()
        val api = retrofit.create(RentalAdAPI::class.java)

        api.getAd(threads[position].adId).enqueue(object : Callback<RentalAdById> {
            override fun onResponse(call: Call<RentalAdById>, response: Response<RentalAdById>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        if (response.body()!!.imagesIdList.isNotEmpty()) {
                            /** Replacing thread subject with ad title for clarity */
                            holder.threadRentalAdTitle.text = response.body()!!.title

                            Glide.with(holder.itemView)
                                .load("$serverUrl" + "api/image/" + response.body()!!.imagesIdList[0])
                                .into(holder.threadsImage)
                        }
                    }
                }
                return
            }

            override fun onFailure(call: Call<RentalAdById>, t: Throwable) {
                Log.e("Loading miniature", t.toString())
                return
            }
        })

        if (threads[position].lastMessageTime.isNotEmpty()) {
            val sentDate: LocalDateTime =
                LocalDateTime.parse(threads[position].lastMessageTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
            val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
            holder.threadSendDate.text = sentDate.format(formatter).toString()
        }

        if (threads[position].unreaded > 0) {
            notificationCounter++
            val bottomNavigationView = parentActivity.findViewById<BottomNavigationView>(R.id.bottom_navigation)
            bottomNavigationView.menu.getItem(3).setIcon(R.drawable.menu_messagenotification_24)

            holder.threadRentalAdTitle.setTypeface(null, Typeface.BOLD)
            holder.threadLastMessage.setTypeface(null, Typeface.BOLD)
            holder.unreadCounter.visibility = View.VISIBLE
            if (threads[position].unreaded > 99) holder.unreadCounter.text = "99+" else holder.unreadCounter.text =
                threads[position].unreaded.toString()
        } else {
            holder.unreadCounter.visibility = View.GONE
        }

        holder.cardView.setOnClickListener { v: View ->
            val chatFragment = ChatFragment()
            val bundle = Bundle()
            bundle.putLong("senderId", threads[position].authorId.toLong())
            bundle.putLong("receiverId", threads[position].receiverId.toLong())
            bundle.putLong("rentalAdId", threads[position].adId)
            bundle.putString("rentalAdTitle", holder.threadRentalAdTitle.text.toString())
            bundle.putString("threadId", threads[position].id)

            var responseCounter = 0

            retrofit.create(UsersAPI::class.java).getUser(threads[position].authorId.toLong())
                .enqueue(object : Callback<UserRespDto> {
                    override fun onResponse(call: Call<UserRespDto>, response: Response<UserRespDto>) {
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                bundle.putString(
                                    "senderFullName", response.body()!!.firstName + " " + response.body()!!.lastName
                                )
                                responseCounter++
                                checkIfReady(chatFragment, responseCounter, bundle, v)
                            } else {
                                bundle.putString("senderFullName", "")
                                responseCounter++
                                checkIfReady(chatFragment, responseCounter, bundle, v)
                            }
                        }
                    }

                    override fun onFailure(call: Call<UserRespDto>, t: Throwable) {
                        Log.e(javaClass.toString(), "Downloading sender details failed", t)
                        responseCounter++
                        checkIfReady(chatFragment, responseCounter, bundle, v)
                    }
                })

            retrofit.create(UsersAPI::class.java).getUser(threads[position].receiverId.toLong())
                .enqueue(object : Callback<UserRespDto> {
                    override fun onResponse(call: Call<UserRespDto>, response: Response<UserRespDto>) {
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                bundle.putString(
                                    "receiverFullName", response.body()!!.firstName + " " + response.body()!!.lastName
                                )
                                responseCounter++
                                checkIfReady(chatFragment, responseCounter, bundle, v)
                            } else {
                                bundle.putString("receiverFullName", "")
                                responseCounter++
                                checkIfReady(chatFragment, responseCounter, bundle, v)
                            }
                        }
                    }

                    override fun onFailure(call: Call<UserRespDto>, t: Throwable) {
                        Log.e(javaClass.toString(), "Downloading receiver details failed", t)
                        responseCounter++
                        checkIfReady(chatFragment, responseCounter, bundle, v)
                    }
                })

        }

        if (notificationCounter == 0){
            val bottomNavigationView = parentActivity.findViewById<BottomNavigationView>(R.id.bottom_navigation)
            bottomNavigationView.menu.getItem(3).setIcon(R.drawable.menu_messages_icon)
        }
    }

    fun checkIfReady(chatFragment: ChatFragment, responseCounter: Int, bundle: Bundle, v: View){
        if (responseCounter >= 2) {
            chatFragment.arguments = bundle
            (v.context as FragmentActivity).supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, chatFragment).commit()
        }
    }

    override fun getItemCount(): Int {
        return threads.size
    }
}
