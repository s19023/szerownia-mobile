package szerownia.mobile.Adapters

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.LayoutRes
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Models.Category
import szerownia.mobile.R

class CustomArrayAdapter(context: Context, @LayoutRes private val layoutResource:Int, private val categories:List<Category>, private val categoryNames:List<String>,
                         private val unicodeMap:HashMap<String,String> = hashMapOf()) : ArrayAdapter<String>(context, layoutResource, categoryNames) {






    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: TextView = super.getView(position, convertView, parent) as TextView

//        if(getItemId(position)==0L){
//            view.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
//        }



        for(category in categories){
            if(category.abstract){
                if(category.name==getItem(position)){
                    view.text=unicodeMap[category.name]+" "+category.name
                }
            }
        }
//            val img: Drawable = context.resources.getDrawable(R.drawable.categories_elektronika_icon, context.theme)
//            view.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
//            view.visibility=View.INVISIBLE


////            view.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null)
////            view.setBackgroundColor(Color.LTGRAY)
//            view.textSize=20f
//            return view
//        }
//        view.text=getItem(position)!!.name

        return view
    }


}