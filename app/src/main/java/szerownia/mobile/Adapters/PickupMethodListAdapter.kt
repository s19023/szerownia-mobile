package szerownia.mobile.Adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Models.PickupMethod
import szerownia.mobile.R

class PickupMethodListAdapter(private val methods: List<PickupMethod>,
                              private val pickupImages: List<Int>) : RecyclerView.Adapter<PickupMethodListAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val pickupMethod: TextView = itemView.findViewById(R.id.pickupMethodName)
        val pickupIcon: ImageView = itemView.findViewById(R.id.pickupIcon)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_rentaladdetails_pickup_element, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return methods.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val methodName = methods[position]
        if (methodName == PickupMethod.PERSONAL_PICKUP) {
            holder.pickupMethod.text = "Odbiór osobisty"
            holder.pickupIcon.setImageResource(R.drawable.all_pickup_personal_icon)
        } else if (methodName == PickupMethod.PARCEL_LOCKER) {
            holder.pickupMethod.text = "Paczkomat"
            holder.pickupIcon.setImageResource(R.drawable.parcel_locker)
        } else if (methodName == PickupMethod.COURIER) {
            holder.pickupMethod.text = "Kurier"
            holder.pickupIcon.setImageResource(R.drawable.all_pickup_shipping_icon)
        }
    }
}
