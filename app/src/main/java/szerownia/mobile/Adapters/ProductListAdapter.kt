package szerownia.mobile.Adapters

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Models.Product
import szerownia.mobile.R

class ProductListAdapter(private val products: List<Product>) : RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {
    private var dialogBuilder: AlertDialog.Builder? = null

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productName: TextView = itemView.findViewById(R.id.productName)
        val productModel: TextView = itemView.findViewById(R.id.productModel)
        val item: CardView = itemView.findViewById(R.id.productCardView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_addetails_item_element, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.productName.text = products[position].name
        holder.productModel.text = products[position].model

        holder.item.setOnClickListener { v: View ->
            dialogBuilder = AlertDialog.Builder(v.context)
            val view = LayoutInflater.from(v.context).inflate(R.layout.popup_rentalad_item_details, null)

            val popupProductName: TextView = view.findViewById(R.id.textview_popup_title)
            val popupProductMake: TextView = view.findViewById(R.id.popupProductMake)
            val popupProductModel: TextView = view.findViewById(R.id.popupProductModel)
            val popupProductEstimatedValue: TextView = view.findViewById(R.id.popupProductEstimatedValue)
            val popupProductCategory: TextView = view.findViewById(R.id.popupProductCategory)
            val ratingBar: RatingBar = view.findViewById(R.id.ratingBar)
            val featureRecyclerView: RecyclerView = view.findViewById(R.id.featureRecyclerView)
            val closeButton: ImageView = view.findViewById(R.id.imageview_popup_close)

            popupProductName.text = products[position].name
            popupProductMake.text = products[position].make
            popupProductModel.text = products[position].model
            popupProductEstimatedValue.text = products[position].estimatedValue.toString() + "zł"
            popupProductCategory.text = products[position].categoryReferenceDTO.name
            ratingBar.rating = products[position].condition.toFloat()

            val adapter = FeaturesAdapter(products[position].featureOccurrences)

            val gridLayout = GridLayoutManager(view.context, 1)
            featureRecyclerView.layoutManager = gridLayout
            featureRecyclerView.adapter = adapter

            dialogBuilder!!.setView(view)
            val dialog = dialogBuilder!!.create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

            closeButton.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

}
