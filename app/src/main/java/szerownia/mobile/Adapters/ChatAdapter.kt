package szerownia.mobile.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Models.Message
import szerownia.mobile.R
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class ChatAdapter(
    private val messages: List<Message>, private val userId: String
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val VIEW_TYPE_MESSAGE_SENT = 1
    private val VIEW_TYPE_MESSAGE_RECEIVED = 2

    private class SentMessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView = itemView.findViewById(R.id.text_gchat_message_me)
        var messageDate: TextView = itemView.findViewById(R.id.textview_sendermessage_date)
        var messageTime: TextView = itemView.findViewById(R.id.textview_sendermessage_time)

        fun bind(message: String, date: String, time: String) {
            messageText.text = message
            messageDate.text = date
            messageTime.text = time
        }

        fun bindSameDay(message: String, time: String) {
            messageText.text = message
            messageTime.text = time
            messageDate.visibility = View.GONE
        }
    }

    private class ReceivedMessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView = itemView.findViewById<View>(R.id.text_gchat_message_other) as TextView
        var messageDate: TextView = itemView.findViewById(R.id.textview_receivermessage_date)
        var messageTime: TextView = itemView.findViewById(R.id.textview_receivermessage_time)

        fun bind(message: String, date: String, time: String) {
            messageText.text = message
            messageDate.text = date
            messageTime.text = time
        }

        fun bindSameDay(message: String, time: String) {
            messageText.text = message
            messageTime.text = time
            messageDate.visibility = View.GONE
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].authorId.equals(userId)) {
            VIEW_TYPE_MESSAGE_SENT
        } else {
            VIEW_TYPE_MESSAGE_RECEIVED
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view: View

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_sender_message, parent, false)
            return SentMessageHolder(view)
        } else {
            view = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_receiver_message, parent, false)
            return ReceivedMessageHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val message: String = messages[position].message
        var date = ""
        var time = ""

        val sentLocalDateTime: LocalDateTime =
            LocalDateTime.parse(messages[position].createdDate, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        date = sentLocalDateTime.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")).toString()
        time = sentLocalDateTime.format(DateTimeFormatter.ofPattern("HH:mm")).toString()

        var previousMessageDate = ""

        if (position > 0) {
            previousMessageDate =
                LocalDateTime.parse(messages[position - 1].createdDate, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                    .format(DateTimeFormatter.ofPattern("dd.MM.yyyy")).toString()
        }

        if (date == previousMessageDate) {
            when (holder.itemViewType) {
                VIEW_TYPE_MESSAGE_SENT -> (holder as SentMessageHolder).bindSameDay(message, time)
                VIEW_TYPE_MESSAGE_RECEIVED -> (holder as ReceivedMessageHolder).bindSameDay(message, time)
            }
        } else {
            when (holder.itemViewType) {
                VIEW_TYPE_MESSAGE_SENT -> (holder as SentMessageHolder).bind(message, date, time)
                VIEW_TYPE_MESSAGE_RECEIVED -> (holder as ReceivedMessageHolder).bind(message, date, time)
            }
        }
    }

    override fun getItemCount(): Int {
        return messages.size
    }
}
