package szerownia.mobile.Adapters

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import szerownia.mobile.Models.Feature
import szerownia.mobile.Models.Filter
import szerownia.mobile.R

class SearchNumericAdapter() : RecyclerView.Adapter<SearchNumericAdapter.ViewHolder>() {

    var features: List<Feature> = listOf()
    val filters: HashMap<Feature, Filter> = HashMap()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val label :TextView = itemView.findViewById(R.id.searchFeatureNumericLabel)
        val featureFrom: EditText = itemView.findViewById(R.id.feature_numeric_From)
        val featureTo: EditText = itemView.findViewById(R.id.feature_numeric_To)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_numeric_element, parent, false)


        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return features.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.label.text = features[position].name

        holder.featureFrom.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {

                if(!filters.containsKey(features[position])){
                   filters.put(features[position], Filter(null, null, null, features[position].idFeature,null))
                }
                if(holder.featureFrom.text.toString().isNullOrEmpty()){
                    filters[features[position]]!!.gt=null
                }else{
                filters[features[position]]!!.gt=holder.featureFrom.text.toString().toInt()}
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
        holder.featureTo.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {

                if(!filters.containsKey(features[position])){
                    filters.put(features[position], Filter(null, null, null, features[position].idFeature,null))
                }
                if(holder.featureTo.text.toString().isNullOrEmpty()){
                    filters[features[position]]!!.lt=null
                }else{
                    filters[features[position]]!!.lt=holder.featureTo.text.toString().toInt()}
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }

}