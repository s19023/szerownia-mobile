package szerownia.mobile.Adapters

import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.R

class PhotosAdapter(private val photos:MutableList<Bitmap>): RecyclerView.Adapter<PhotosAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val photo: ImageView = itemView.findViewById(R.id.photoImageView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_photo, parent, false)
        return PhotosAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: PhotosAdapter.ViewHolder, position: Int) {
       val photo = photos[position]
        holder.photo.setImageBitmap(photo)
        holder.photo.setOnClickListener {
            if(photos.size>1) {
                photos.remove(photo)
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int {
        return  photos.size
    }

    public fun getPhotosList(): MutableList<Bitmap> {
        return photos
    }
}