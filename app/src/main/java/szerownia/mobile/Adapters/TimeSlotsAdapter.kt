package szerownia.mobile.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Models.TimeSlot
import szerownia.mobile.R
import java.text.DateFormat
import java.text.SimpleDateFormat

class TimeSlotsAdapter(private val timeSlots: MutableList<TimeSlot>) :
    RecyclerView.Adapter<TimeSlotsAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dateFrom: TextView = itemView.findViewById(R.id.dateFrom)
        val dateTo: TextView = itemView.findViewById(R.id.dateTo)
        val delete: ImageButton = itemView.findViewById(R.id.deleteTimeSlotButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_time_slot, parent, false)
        return TimeSlotsAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dateFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy")

        holder.dateFrom.text = dateFormat.format(timeSlots[position].startDate)
        if (timeSlots[position].endDate != null) {
            holder.dateTo.text = dateFormat.format(timeSlots[position].endDate)
        } else {
            holder.dateTo.text = "----"
        }
        holder.delete.setOnClickListener {
            timeSlots.removeAt(position)
            notifyDataSetChanged()
        }

    }

    override fun getItemCount(): Int {
        return timeSlots.size
    }

}