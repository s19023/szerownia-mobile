package szerownia.mobile.Adapters

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatImageButton
import androidx.cardview.widget.CardView
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.getstream.sdk.chat.utils.extensions.activity
import com.google.android.material.textfield.TextInputEditText
import io.getstream.chat.android.client.parser.StreamGson.gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.HireAPI
import szerownia.mobile.API.MessagesAPI
import szerownia.mobile.API.OpinionAPI
import szerownia.mobile.API.RentalAdAPI
import szerownia.mobile.Fragments.MyBorrowsFragment
import szerownia.mobile.Fragments.MySharesFragment
import szerownia.mobile.Fragments.UserNotLoggedInFragment
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.Borrow
import szerownia.mobile.Models.Opinion
import szerownia.mobile.Models.PickupMethod
import szerownia.mobile.Models.RentalAdById
import szerownia.mobile.R
import java.text.NumberFormat
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList

class MyBorrowsListAdapter(
    private val myBorrows: ArrayList<Borrow>,
    private val retrofit: Retrofit,
    private val myReturns: ArrayList<Borrow>,
    private val noActiveBorrows: TextView,
    private val noPastBorrows :TextView,
    private val  rc:RecyclerView,
    private val adapterReturns:MyReturnsListAdapter,
    private val isMyBorrows:Boolean

) : RecyclerView.Adapter<MyBorrowsListAdapter.ViewHolder>() {

    private var serverUrl =""
    
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)

        val moreButton: ImageButton = itemView.findViewById(R.id.moreButton)
        val ratingReceived: RatingBar = itemView.findViewById(R.id.ratingBarGivenRating)
        val ratingGiven: RatingBar = itemView.findViewById(R.id.ratingBarReceivedRating)
        val noRatingYet: TextView = itemView.findViewById(R.id.noRatingYet)
        val itemPicture: ImageView = itemView.findViewById(R.id.itemPicture)
        val date: TextView = itemView.findViewById(R.id.rentDateFrom)
        val cardView:CardView=itemView.findViewById(R.id.cardview_item_searchad)



    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_borrow_element, parent, false)
        val globalVariable: GlobalVariable = parent.context.applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(parent.context.applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(parent.context.applicationContext, "localhost_url")!!
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = myBorrows[position].ad.title
        holder.moreButton.visibility=View.GONE



        holder.date.text = "wypożyczane od: "+ myBorrows[position].dateFrom
        val api0 = retrofit.create(RentalAdAPI::class.java)


        api0.getAd(myBorrows[position].ad.id).enqueue(object : Callback<RentalAdById> {
            override fun onResponse(call: Call<RentalAdById>, response: Response<RentalAdById>) {

                Log.i("Response", response.body().toString())
                val imageId = response.body()!!.imagesIdList[0]


                Glide.with(holder.itemView).load(serverUrl+"api/image/$imageId").diskCacheStrategy(
                    DiskCacheStrategy.RESOURCE).into(holder.itemPicture)
                Log.i("imageId", imageId.toString())
                return
            }

            override fun onFailure(call: Call<RentalAdById>, t: Throwable) {
                Log.e("Error:", t.message.toString());
                return
            }
        })

        val api = retrofit.create(HireAPI::class.java)

        var idOpinionGot=-1
        for (opinion in myBorrows[position].opinions){
            if(opinion.aboutSharer){
                holder.ratingGiven.visibility= View.VISIBLE
                holder.noRatingYet.visibility = View.GONE
                Log.e("Opinion given"+opinion.id, opinion.rating.toFloat().toString())
                holder.ratingGiven.rating=opinion.rating.toFloat()
            }else{
                holder.ratingReceived.rating=opinion.rating.toFloat()
                idOpinionGot = opinion.id.toInt()
                Log.e("opinion Got",idOpinionGot.toString())
                Log.e("Opinion recieved"+opinion.id, opinion.rating.toFloat().toString())
            }
        }
        holder.cardView.setOnClickListener { view ->
            val popup: PopupMenu = PopupMenu(view.context, holder.moreButton)
            popup.setOnMenuItemClickListener {itemClicked->
                when(itemClicked.itemId){
                    R.id.rentDetails ->{
                        val dialogBuilder = AlertDialog.Builder(view.context)
                        val view = LayoutInflater.from(view.context).inflate(R.layout.popup_hire_details, null)

                        val title: TextView = view.findViewById(R.id.rentalAdTitle)
                        val publicationDate: TextView = view.findViewById(R.id.adStartDate)
                        val endDate: TextView = view.findViewById(R.id.adEndDate)
                        val adImage: ImageView = view.findViewById(R.id.adImage)
                        val userName: TextView = view.findViewById(R.id.userName)
                        val sendButton: Button = view.findViewById(R.id.sendMessageButton)
                        val callButton: Button = view.findViewById(R.id.callButton)
                        val backButton: ImageView = view.findViewById(R.id.imageview_addetails_backbutton)
                        val pickupRecyclerView = view.findViewById<RecyclerView>(R.id.pickupMethodList)
                        val pricePerDayDetails: TextView = view.findViewById(R.id.pricePerDayDetails)
                        val depositAmount: TextView = view.findViewById(R.id.depositAmount)
                        val penaltyForEachDayOfDelayInReturns: TextView = view.findViewById(R.id.penaltyForEachDayOfDelayInReturns)

                        sendButton.visibility= GONE

//                        sendButton.setOnClickListener {
//                            val fr = activity!!.supportFragmentManager
//
//
//                                val dialogBuilder = AlertDialog.Builder(context)
//                                val popupView = LayoutInflater.from(context).inflate(R.layout.popup_addetails_newmessage, null)
//                                dialogBuilder.setView(popupView)
//                                val dialog = dialogBuilder.create()
//                                dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//                                dialog.show()
//
//                                val message: TextInputEditText = popupView.findViewById(R.id.chatTextInputEditText)
//                                val sendButton: ImageView = popupView.findViewById(R.id.chatSendButton)
//
//                                sendButton.setOnClickListener {
//                                    focus.clearFocus(requireActivity(), requireContext())
//
//                                    if (message.text.toString().isEmpty()) {
//                                        Toast.makeText(context, "Wiadomość jest pusta", Toast.LENGTH_SHORT).show()
//                                    } else {
//                                        val strRequestBody = message.text.toString()
//                                        val requestBody = strRequestBody.toRequestBody("text/plain".toMediaTypeOrNull())
//
//                                        val retrofitMessages = Retrofit.Builder().baseUrl(messagesUrl)
//                                            .addConverterFactory(GsonConverterFactory.create(gson)).client(okHttpClient).build()
//
//                                        val apiMessages = retrofitMessages.create(MessagesAPI::class.java)
//                                        apiMessages.createMessageForNewThread(
//                                            "Bearer " + sharedPreferences.getString(
//                                                "TOKEN", null
//                                            ).toString(),
//                                            rentalAdById.user.id.toString() + "A" + rentalAdById.id.toString(),
//                                            requestBody
//                                        ).enqueue(object : Callback<Void> {
//                                            override fun onResponse(call: Call<Void>, response: Response<Void>) {
//                                                if (response.code() == 200) {
//                                                    dialog.dismiss()
//                                                    Toast.makeText(view.context, "Wiadomość wysłana", Toast.LENGTH_SHORT).show()
//                                                    Log.println(
//                                                        Log.INFO, javaClass.toString(), "New message sent"
//                                                    )
//                                                } else {
//                                                    Toast.makeText(view.context, "Nie można wysłać wiadomości", Toast.LENGTH_SHORT)
//                                                        .show()
//                                                    Log.println(
//                                                        Log.ERROR,
//                                                        javaClass.toString(),
//                                                        "Sending message failed with error code ${response.code()}"
//                                                    )
//                                                }
//                                            }
//
//                                            override fun onFailure(call: Call<Void>, t: Throwable) {
//                                                Toast.makeText(view.context, "Nie można wysłać wiadomości", Toast.LENGTH_SHORT)
//                                                    .show()
//                                                Log.println(
//                                                    Log.ERROR,
//                                                    javaClass.toString(),
//                                                    "Sending message failed: $t"
//                                                )
//                                            }
//                                        })
//                                    }
//                                }
//
//                        }




                        callButton.setOnClickListener {

                                val telephoneNumber = myBorrows[position].sharer.telephoneNumber

                                val intent = Intent(Intent.ACTION_DIAL)
                                intent.data = Uri.parse("tel:$telephoneNumber")
                                startActivity(view.context,intent,null)


                        }

                        title.text=myBorrows[position].ad.title
                        publicationDate.text=myBorrows[position].dateFrom
                        endDate.text=myBorrows[position].dateToPlanned
                        if (myBorrows[position].ad.adThumbnailId!=null) {

                            Glide.with(holder.itemView)
                                .load(serverUrl+"api/image/" + myBorrows[position].ad.adThumbnailId)
                                .diskCacheStrategy(
                                    DiskCacheStrategy.RESOURCE)
                                .into(adImage)
                        }
                        userName.text = myBorrows[position].sharer.firstName +" " + myBorrows[position].sharer.lastName
                        val pickupImages = ArrayList<Int>()
                        pickupImages.add(R.drawable.all_pickup_personal_icon)
                        pickupImages.add(R.drawable.all_pickup_shipping_icon)
                        pickupImages.add(R.drawable.parcel_locker)
                        var pickupMethodList = arrayListOf<PickupMethod>()
                        when(myBorrows[position].selectedPickupMethod){
                            "PERSONAL_PICKUP" ->{pickupMethodList.add(PickupMethod.PERSONAL_PICKUP)}
                            "PARCEL_LOCKER" ->{pickupMethodList.add(PickupMethod.PARCEL_LOCKER)}
                            "COURIER" ->{pickupMethodList.add(PickupMethod.COURIER)}
                        }

                        var pickupMethodListString = ArrayList<String>()

                        for (i in pickupMethodList) {
                            pickupMethodListString.add(i.name)
                        }

                        val pickupAdapter = PickupMethodListAdapter(pickupMethodList, pickupImages)
                        val pickupGridLayoutManager = GridLayoutManager(view.context, 1)
                        pickupRecyclerView.layoutManager = pickupGridLayoutManager
                        pickupRecyclerView.adapter = pickupAdapter

                        when(myBorrows[position].hireStatus){
                            "WAITING_FOR_SHIPMENT" ->{pricePerDayDetails.text = "Oczekuje na wysłanie"}
                            "SHIPPED" ->{pricePerDayDetails.text = "Wysłane do wypożyczającego"}
                            "PENDING_HIRE" ->{pricePerDayDetails.text = "W trakcie wypożyczenia"}
                            "SHIPPED_BACK" ->{pricePerDayDetails.text = "Wysłane do użyczającego"}
                            "RETURNED" ->{pricePerDayDetails.text = "Zakończone"}
                        }

                        depositAmount.text=myBorrows[position].outgoingShipmentNumber
                        penaltyForEachDayOfDelayInReturns.text=
                            NumberFormat.getCurrencyInstance(Locale("pl", "PL")).format(myBorrows[position].cost)

                        dialogBuilder.setView(view)
                        val dialog = dialogBuilder.create()
                        dialog.show()

                        backButton.setOnClickListener { dialog.dismiss() }
                    }
                    R.id.viewOpinion ->{
                        val dialogBuilder = AlertDialog.Builder(view.context)
                        val view = LayoutInflater.from(view.context).inflate(R.layout.popup_rate, null)
                        val closeButton: ImageButton = view.findViewById(R.id.rateCloseButton)
                        val input: TextInputEditText = view.findViewById(R.id.ratingInput)
                        val ratingBar: RatingBar = view.findViewById(R.id.ratingbar_opinion)
                        val submitButton: Button = view.findViewById(R.id.submitRating)
                        for(opinion in myBorrows[position].opinions){
                            if(opinion.aboutSharer){
                                input.setText(opinion.comment)
                                ratingBar.rating=opinion.rating.toFloat()
                            }
                        }
                        submitButton.visibility=View.GONE
                        input.isClickable=false
                        input.isCursorVisible=false
                        input.isFocusable=false
                        input.isFocusableInTouchMode=false




                        dialogBuilder.setView(view)
                        val dialog = dialogBuilder.create()
                        dialog.show()
                        closeButton.setOnClickListener {
                            dialog.dismiss() }
                    }
                    R.id.returnItem ->{
                        val date: LocalDate = LocalDate.now()
                        api.returnBorrow(myBorrows[position].id, date.toString().split('T')[0])
                            .enqueue(object : Callback<Void> {
                                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                    Log.i("response", response.toString())
                                    myReturns.add(myBorrows.get(position))
                                    myBorrows.remove(myBorrows.get(position))
                                    if (myBorrows.isEmpty()) {
                                        noActiveBorrows.visibility = View.VISIBLE
                                        rc.visibility = View.GONE

                                    } else {
                                        noActiveBorrows.visibility = View.GONE
                                    }
                                    if (myReturns.isEmpty()) {
                                        noPastBorrows.visibility = View.VISIBLE
                                    } else {
                                        noPastBorrows.visibility = View.GONE
                                    }

                                    val dialogBuilder = AlertDialog.Builder(view.context)
                                    val view =
                                        LayoutInflater.from(view.context).inflate(R.layout.popup_return_confirmation, null)
                                    val closeButton: AppCompatImageButton = view.findViewById(R.id.rentCloseButton)

                                    dialogBuilder.setView(view)
                                    val dialog = dialogBuilder.create()
                                    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.LTGRAY))
                                    dialog.show()
                                    notifyDataSetChanged()

                                    closeButton.setOnClickListener {
                                        dialog.dismiss()
                                    }
                                    adapterReturns.notifyDataSetChanged()
                                    notifyDataSetChanged()

                                    return
                                }

                                override fun onFailure(call: Call<Void>, t: Throwable) {
                                    throw t
                                }
                            })
                    }
                    R.id.postOpinion ->{
                        val dialogBuilder = AlertDialog.Builder(view.context)
                        val view = LayoutInflater.from(view.context).inflate(R.layout.popup_rate, null)
                        val closeButton: ImageButton = view.findViewById(R.id.rateCloseButton)
                        val input: TextInputEditText = view.findViewById(R.id.ratingInput)

                        val ratingBar: RatingBar = view.findViewById(R.id.ratingbar_opinion)
                        val submitButton: Button = view.findViewById(R.id.submitRating)

                        for(opinion in myBorrows[position].opinions){
                            if(!opinion.aboutSharer){
                                input.setText(opinion.comment)
                                ratingBar.rating=opinion.rating.toFloat()
                            }
                        }

                        dialogBuilder.setView(view)
                        val dialog = dialogBuilder.create()
                        dialog.show()
                        closeButton.setOnClickListener {
                            dialog.dismiss() }

                        submitButton.setOnClickListener {
                            val loadingDialog = LoadingDialog(view, "Wysyłanie Opini...")
                            loadingDialog.showLoadingDialog()
                            val api1 = retrofit.create(OpinionAPI::class.java)
                            Log.e("IdOpinionSent",idOpinionGot.toString())
                            if(idOpinionGot <= 0) {
                                var opinionId: Long = -1

                                api1.getAllOpinions().enqueue(object : Callback<List<Opinion>> {
                                    override fun onResponse(call: Call<List<Opinion>>, response: Response<List<Opinion>>) {
                                        if (response.isSuccessful) {
                                            opinionId = (response.body()!!.size + 1).toLong()
                                            return
                                        }
                                    }

                                    override fun onFailure(call: Call<List<Opinion>>, t: Throwable) {
                                        Log.e("Getting Opinions", t.toString())
                                        return
                                    }
                                })
                                val opinion = Opinion(
                                    false,
                                    myBorrows[position].sharer.id,
                                    input.text.toString(),
                                    myBorrows[position].id,
                                    opinionId,
                                    ratingBar.rating.toInt(),
                                    myBorrows[position].borrower.id

                                )
                                Log.e("Opinion", opinion.toString())
                                api1.createOpinion(opinion).enqueue(object : Callback<Void> {
                                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                        dialog.dismiss()
                                        Toast.makeText(it.context, "Opinia Wysłana", Toast.LENGTH_SHORT).show()
                                        Log.i("Sending Opinion", "Opinion Sent")
                                        loadingDialog.dismissLoadingDialog()

                                        val nextFragment = if(isMyBorrows){
                                            MyBorrowsFragment()
                                        }else{
                                            MySharesFragment()
                                        }
                                        (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
                                            .replace(R.id.fragment_container, nextFragment).commit()
                                    }

                                    override fun onFailure(call: Call<Void>, t: Throwable) {
                                        Log.e("Sending Opinion", t.toString())
                                        dialog.dismiss()
                                        loadingDialog.dismissLoadingDialog()
                                    }
                                })
                            }else{

                                val opinion = Opinion(
                                    false,
                                    myBorrows[position].sharer.id,
                                    input.text.toString(),
                                    myBorrows[position].id,
                                    idOpinionGot.toLong(),
                                    ratingBar.rating.toInt(),
                                    myBorrows[position].borrower.id

                                )
                                api1.updateOpinion(opinion).enqueue(object : Callback<Void>{
                                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                        dialog.dismiss()
                                        Toast.makeText(it.context, "Opinia Uaktualniona", Toast.LENGTH_SHORT).show()
                                        Log.i("Sending Opinion", "Opinion Sent")
                                        loadingDialog.dismissLoadingDialog()
                                        val nextFragment = if(isMyBorrows){
                                            MyBorrowsFragment()
                                        }else{
                                            MySharesFragment()
                                        }
                                        (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
                                            .replace(R.id.fragment_container, nextFragment).commit()
                                    }

                                    override fun onFailure(call: Call<Void>, t: Throwable) {
                                        Log.e("Sending Opinion", t.toString())
                                        dialog.dismiss()
                                        loadingDialog.dismissLoadingDialog()
                                    }
                                })
                            }
                        }
                    }
                }

                    true

                }



            var isAboutSharer = false
            for(opinion in myBorrows[position].opinions){
                if(opinion.aboutSharer){
                    isAboutSharer=true
                }
            }
            when(isMyBorrows){
                true ->{
                    if(isAboutSharer){
                        popup.inflate(R.menu.return_item)
                    }else{
                        popup.inflate(R.menu.return_item_without_opinion)
                    }
                }
                false ->{
                    if(isAboutSharer){
                        popup.inflate(R.menu.no_return_item)
                    }else{
                        popup.inflate(R.menu.no_return_item_without_opinion)
                    }
                }
            }


                popup.show()

        }

    }

    override fun getItemCount(): Int {
        return myBorrows.size
    }
}
