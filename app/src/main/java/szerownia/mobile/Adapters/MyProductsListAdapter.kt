package szerownia.mobile.Adapters

import android.util.Log
import android.util.Log.INFO
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Models.MyProduct
import szerownia.mobile.R

class MyProductsListAdapter(
    private val myProducts: List<MyProduct>
) : RecyclerView.Adapter<MyProductsListAdapter.ViewHolder>() {
    val checkedProducts = ArrayList<Int>()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productNameCheckBox: CheckBox = itemView.findViewById(R.id.myProductName)
        val model: TextView = itemView.findViewById(R.id.myProductModel)
        val make: TextView = itemView.findViewById(R.id.myProductMake)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_add_product_element, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.productNameCheckBox.text = myProducts[position].name
        holder.model.text = myProducts[position].model
        holder.make.text = myProducts[position].make

        holder.productNameCheckBox.setOnClickListener {
            Log.println(INFO, javaClass.toString(), "Click listener used on " + myProducts[position].idProduct + " " + myProducts[position].name)
            if (holder.productNameCheckBox.isChecked){
                if (!checkedProducts.contains(myProducts[position].idProduct))
                checkedProducts.add(myProducts[position].idProduct)
            }else if (!holder.productNameCheckBox.isChecked){
                if (checkedProducts.contains(myProducts[position].idProduct))
                    checkedProducts.remove(myProducts[position].idProduct)
            }
        }
    }

    override fun getItemCount(): Int {
        return myProducts.size
    }
}
