package szerownia.mobile.Adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import szerownia.mobile.Models.MyShare
import szerownia.mobile.R

class MySearchAdsAdapter(private val myShares: List<MyShare>) : RecyclerView.Adapter<MySearchAdsAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.textview_item_searchad_history_title)
        val dateFrom: TextView = itemView.findViewById(R.id.textview_item_searchad_history_datefrom)
        val dateTo: TextView = itemView.findViewById(R.id.textview_item_searchad_history_dateto)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_popup_searchads_history, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n") override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = myShares[position].ad.title
        val dateFrom = myShares[position].dateFrom
        val dateTo = myShares[position].dateToPlanned

        holder.dateFrom.text = "od: $dateFrom"
        holder.dateTo.text = "do: $dateTo"
    }

    override fun getItemCount(): Int {
        return myShares.size
    }

}