package szerownia.mobile.Models

data class UserX(
    val borrowerRate: Double,
    val email: String,
    val firstName: String,
    val id: Long,
    val lastName: String,
    val sharerRate: Double,
    val telephoneNumber: String,
    val userImageId: Long
)