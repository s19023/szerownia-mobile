package szerownia.mobile.Models

import java.util.*

data class Assignment(
    val assignedDate: Date,
    val assigner: String,
    val id: Long
)