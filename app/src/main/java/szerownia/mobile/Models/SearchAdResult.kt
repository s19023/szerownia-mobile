package szerownia.mobile.Models

data class SearchAdResult(
    val count: Long,
    val results: List<SearchAd>,
    val totalPages : Long
)