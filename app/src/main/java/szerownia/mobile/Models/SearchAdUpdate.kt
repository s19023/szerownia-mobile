package szerownia.mobile.Models

data class SearchAdUpdate(
    val description: String,
    val title: String
)