package szerownia.mobile.Models

data class Borrow(
    val ad: Ad,
    val borrower: UserX,
    val cost: Double,
    val dateFrom: String,
    val dateToActual: String,
    val dateToPlanned: String,
    val outgoingShipmentNumber:String,
    val hireStatus :String,
    val id: Long,
    val opinions: List<Opinion>,
    val selectedPickupMethod: String,
    val sharer: UserX
)