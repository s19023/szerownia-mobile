package szerownia.mobile.Models

data class RentalAdResult(
    val count: Long,
    val results: List<RentalAd>,
    val totalPages: Long
)