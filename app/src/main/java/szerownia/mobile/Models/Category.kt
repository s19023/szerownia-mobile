package szerownia.mobile.Models

data class Category(
    val idCategory: Long,
    val name: String,
    val parentCategory: CategoryReference?,
    val subCategories: List<CategoryReference>,
    val abstract: Boolean
)