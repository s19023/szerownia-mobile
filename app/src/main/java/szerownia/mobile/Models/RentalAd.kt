package szerownia.mobile.Models

data class RentalAd(

    val id: Long,
    val title: String,
    val location: String,
    val pricePerDay: Double,
    val timeSlotList: List<TimeSlot>,
    val imagesIdList: List<Int>,
    val averageOpinion: Double,
    val averageRentalPrice: Int,
    val customRequirement: String,
    val minRentalHistory: Int,
    val promoted:Boolean

)