package szerownia.mobile.Models

data class CategoryReference(
    val idCategory: Long,
    val name: String
)
