package szerownia.mobile.Models

data class User(
    val email: String,
    val firstName: String,
    val lastName: String,
    val password: String,
    val telephoneNumber: String
)