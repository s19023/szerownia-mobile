package szerownia.mobile.Models

data class NewMessage(val content: String, val thread: Long)
