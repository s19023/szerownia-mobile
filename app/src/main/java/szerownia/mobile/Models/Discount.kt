package szerownia.mobile.Models

import java.math.BigDecimal
import java.text.DecimalFormat

data class Discount(
    val id: Int,
    val minNumDays: Int,
    val value: BigDecimal
)