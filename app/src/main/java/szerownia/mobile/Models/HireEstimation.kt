package szerownia.mobile.Models

import android.icu.util.CurrencyAmount
import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.*

data class HireEstimation(
    val commission: BigDecimal,
    val cost: BigDecimal,
    val depositAmount: BigDecimal,
    val period: Int,
    val pricePerDay: BigDecimal,
    val usedDiscount: Discount
)