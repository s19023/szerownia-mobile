package szerownia.mobile.Models

import java.lang.StringBuilder

data class PostFeatureValue(
    val booleanValue: Boolean?,
    val decimalNumberValue: Int?,
    val floatingNumberValue: Double?,
    val idFeature: Int,
    val textValue: String?
) {
    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("{")
        if(booleanValue!=null){
            sb.append("\"booleanValue\":")
            sb.append(booleanValue)
            sb.append(",\n")
        }
        if(decimalNumberValue!=null){
            sb.append("\"decimalNumberValue\":\"")
            sb.append(decimalNumberValue)
            sb.append("\"")
            sb.append(",\n")
        }
        if(floatingNumberValue!=null){
            sb.append("\"floatingNumberValue\":")
            sb.append(floatingNumberValue)
            sb.append(",\n")
        }
        if(textValue!=null){
            sb.append("\"textValue\":")
            sb.append(textValue)
            sb.append(",\n")
        }
        sb.append("\"idFeature\":")
        sb.append(idFeature)
        sb.append("}")
        return sb.toString()
    }
}