package szerownia.mobile.Models

data class Ad(
    val id: Long, val title: String, val adThumbnailId :Long
)
