package szerownia.mobile.Models

import java.util.*

data class SearchAdById(
    val id: Long,
    val title: String,
    val creationDate: Date,
    val timeSlotList:List<TimeSlot>,
    val endDate: Date,
    val location: Location,
    val pickupMethod: List<PickupMethod>,
    val description: String,
    val user: Author,
    val products: List<Product>,
    val visible: Boolean,
    val averageOpinion: Double,
    val averageRentalPrice: Double,
    val minRentalHistory: Int,
    val customerRequirement: String
)