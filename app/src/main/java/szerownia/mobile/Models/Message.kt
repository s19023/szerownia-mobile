package szerownia.mobile.Models

data class Message(
    val id: String,
    val authorId: String,
    val message: String,
    val createdDate: String,
    val seenDate: String,
    val threadId: String
)
