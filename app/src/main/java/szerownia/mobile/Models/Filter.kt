package szerownia.mobile.Models

import java.lang.StringBuilder

data class Filter(
    val bool: Boolean?,
    val eq: String?,
    var gt: Int?,
    val id: Int,
    var lt: Int?
){
    override fun toString(): String {
        val sb =StringBuilder()
        sb.append("{")
        if(bool!=null){
            sb.append("\"bool\":")
            sb.append(bool)
            sb.append(",\n")
        }
        if(eq!=null){
            sb.append("\"eq\":\"")
            sb.append(eq)
            sb.append("\"")
            sb.append(",\n")
        }
        if(lt!=null){
            sb.append("\"lt\":")
            sb.append(lt)
            sb.append(",\n")
        }
        if(gt!=null){
            sb.append("\"gt\":")
            sb.append(gt)
            sb.append(",\n")
        }
        sb.append("\"id\":")
        sb.append(id)
        sb.append("}")
        return sb.toString()
    }
}