package szerownia.mobile.Models

data class SearchAd(val endDate: String, val id: Long, val location: String, val pickupMethod: List<String>,
                    val timeSlotList: List<TimeSlot>, val title: String, val userId: Long)
