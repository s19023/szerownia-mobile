package szerownia.mobile.Models

import java.util.*

data class CreateClaimDTO(
    val claimId: Long,
    val damageAssessment: Int,
    val description: String,
    val imagesId: List<Long>,
    val imagesNames: List<String>,
    val inspectionDate: Date
)