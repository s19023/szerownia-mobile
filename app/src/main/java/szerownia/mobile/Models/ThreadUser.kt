package szerownia.mobile.Models

data class ThreadUser(val id: Long, val firstName: String, val lastName: String)
