package szerownia.mobile.Models

/**
 * Shell class for pagination and storing List of Rental Ads
 * **/

data class RentalAdShell(
    val count: Long,
    val totalPages: Long,
    val results: List<RentalAd>
)
