package szerownia.mobile.Models

data class ProductFeature(
    val booleanValue: Boolean,
    val decimalNumberValue: Int,
    val floatingNumberValue: Int,
    val idFeature: Int,
    val textValue: String
)