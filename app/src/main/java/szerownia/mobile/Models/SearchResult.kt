package szerownia.mobile.Models

data class SearchResult(
    val count: Long,
    val results: List<RentalAd>,
    val totalPages: Long
)