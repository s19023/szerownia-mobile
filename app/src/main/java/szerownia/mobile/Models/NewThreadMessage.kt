package szerownia.mobile.Models

data class NewThreadMessage(
    val threadDetails: String
)
