package szerownia.mobile.Models

data class ThreadMessage(
    val content: String,
    val id: Long,
    val receiver: ThreadReceiver,
    val receiverId: Long,
    val senderId: Long,
    val sentDate: String,
    val readDate: String
)
