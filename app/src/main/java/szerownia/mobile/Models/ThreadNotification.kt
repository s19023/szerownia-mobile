package szerownia.mobile.Models

import kotlinx.serialization.Serializable

@Serializable data class ThreadNotification(
    val threadId: String,
    val authorId: String,
    val receiverId: String,
    val messageSender: String,
    val lastMessage: String,
    val lastMessageTime: String
)