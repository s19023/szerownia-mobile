package szerownia.mobile.Models

data class FeatureOccurrence(
    val idFeatureOccurrence: Long,
    val featureName: String,
    val textValue: String,
    val decimalNumberValue: Int,
    val floatingNumberValue: Double,
    val booleanValue: Boolean
    )
