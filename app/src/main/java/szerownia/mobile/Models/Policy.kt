package szerownia.mobile.Models

import java.math.BigDecimal

data class Policy(
    val id: Int,
    val insuranceTotal: BigDecimal
)