package szerownia.mobile.Models

data class RefreshToken(val token: String, var refreshToken: String)