package szerownia.mobile.Models

import java.util.*

data class Appraisal(
    val id: Long,
    val claimDate: String,
    val reportDate: String,
    val claimAdjusterSendDate: String,
    val dateOfAssignToTheAppraiser: String,
    val damageDescription: String,
    val circumstances: String,
    val justification: String,
    val user: UserX,
    val adjuster :Adjuuster,
    val policy: Policy,
    val location: LocationForRentalAdRequest,
    val status: String,
    val title: String,
    val borrower: UserX,
)