package szerownia.mobile.Models

data class UserRespDto(
    val id: Long,
    val email: String,
    val firstName: String,
    val lastName: String,
    val borrowerRate: Double,
    val sharerRate: Double,
    val telephoneNumber: String,
    val userImageId: Long
)
