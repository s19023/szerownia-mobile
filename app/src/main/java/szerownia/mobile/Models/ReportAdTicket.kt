package szerownia.mobile.Models

data class ReportAdTicket(val adId: Long, val desc: String, val notifierId: Long, val subject: String, val isSearchAd: Boolean)
