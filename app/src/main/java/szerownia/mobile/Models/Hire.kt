package szerownia.mobile.Models

import java.time.LocalDate
import java.util.*

data class Hire(
    val adId: Long,
    val dateFrom: String,
    val dateToPlanned: String,
    val selectedPickupMethod: String
)