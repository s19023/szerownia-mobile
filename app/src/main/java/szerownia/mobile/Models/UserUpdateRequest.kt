package szerownia.mobile.Models

data class UserUpdateRequest(
    val firstName: String, val lastName: String, val email: String, val telephoneNumber: String
)
