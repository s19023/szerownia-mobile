package szerownia.mobile.Models

data class Opinion(
    val aboutSharer: Boolean,
    val authorId: Long,
    val comment: String,
    val hireId: Long,
    val id: Long,
    val rating: Int,
    val takerId: Long
)
