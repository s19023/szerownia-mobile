package szerownia.mobile.Models

data class Product(
    val id: Long,
    val name: String,
    val make: String,
    val model: String,
    val condition: Int,
    val estimatedValue: Int,
    val categoryReferenceDTO: CategoryReference,
    val featureOccurrences: List<FeatureOccurrence>
)
