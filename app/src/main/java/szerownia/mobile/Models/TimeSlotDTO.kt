package szerownia.mobile.Models

import java.util.*

data class TimeSlotDTO(
    val to: Date, val from: Date
)