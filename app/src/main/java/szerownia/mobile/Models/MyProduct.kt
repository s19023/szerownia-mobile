package szerownia.mobile.Models

data class MyProduct(val condition: Int, val estimatedValue: Int, val idProduct: Int, val make: String,
                     val model: String, val name: String)
