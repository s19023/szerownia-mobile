package szerownia.mobile.Models

import java.math.BigDecimal

data class ProductCreateRequestDTO(
    val condition: Double,
    val estimatedValue: BigDecimal,
    val idCategory: Long,
    val make: String,
    val model: String,
    val name: String,
    val productFeatures: List<PostFeatureValue>,
    val searchProduct :Boolean
)