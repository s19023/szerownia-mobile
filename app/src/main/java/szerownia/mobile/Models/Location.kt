package szerownia.mobile.Models

data class Location(
    var id: Long,
    var latitude: Double,
    var longitude: Double,
    var name: String
)
{
    override fun toString(): String {
        return "{latitude=$latitude, longitude=$longitude, name='$name'}"
    }
}
