package szerownia.mobile.Models

import java.util.*

data class RentalAdById(

    val averageOpinion: Double,
    val creationDate: Date,
    val averageRentalPrice: Double,
    val customRequirement: String,
    val description: String,
    val depositAmount: Double,
    val discounts: List<Discount>,
    val id: Long,
    val imageUserId: Long,
    val imagesIdList: List<Int>,
    val location: Location,
    val minRentalHistory: Int,
    val penaltyForEachDayOfDelayInReturns: Double,
    val pickupMethodList: ArrayList<PickupMethod>,
    val pricePerDay: Double,
    val timeSlotList: List<TimeSlot>,
    val title: String,
    val user: Author,
    var promoted:Boolean,
    val products: List<Product>,
    val visible: Boolean
)
