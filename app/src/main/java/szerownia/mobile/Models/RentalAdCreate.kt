package szerownia.mobile.Models

import java.lang.StringBuilder
import java.math.BigDecimal

data class RentalAdCreate(
    val title: String,
    val timeSlotList: List<TimeSlot>,
    val location: LocationForRentalAdRequest,
    val pickupMethod: List<PickupMethod>,
    val description: String,
    val pricePerDay: BigDecimal,
    val depositAmount: BigDecimal,
    val penaltyForEachDayOfDelayInReturns: BigDecimal,
    val averageOpinion: Double,
    val averageRentalPrice: Double,
    val minRentalHistory: Int,
    val customRequirement: String,
    val imageNames: List<Long>,
    var promoted: Boolean
)
