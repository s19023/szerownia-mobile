package szerownia.mobile.Models

data class Thread(
    val id: String,
    val authorId: String,
    val receiverId: String,
    val subject: String,
    val lastMessage: String,
    val lastMessageTime: String,
    val unreaded: Long,
    val ticketId: Long,
    val adId: Long
)
