package szerownia.mobile.Models

data class ThreadRentalAd(val id: Long, val title: String, val user: ThreadUser)
