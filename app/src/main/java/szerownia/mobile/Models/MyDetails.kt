package szerownia.mobile.Models

data class MyDetails(val averageRate2Sharer: Double,
                     val averageRateBorrower: Double,
                     val blocked: Boolean,
                     val email: String,
                     val firstName: String,
                     val lastName: String,
                     val premium: Boolean,
                     val premiumFrom: String,
                     val premiumTo: String,
                     val registrationDate: String,
                     val telephoneNumber: String)
