package szerownia.mobile.Models

data class Author(
    val id: Long,
    val email: String,
    val firstName: String,
    val lastName: String,
    val telephoneNumber: String,
    val borrowerRate: Double,
    val sharerRate: Double
)
