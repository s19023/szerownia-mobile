package szerownia.mobile.Models

import java.util.*

data class TimeSlot(
    val endDate: Date?, val startDate: Date
)