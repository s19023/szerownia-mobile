package szerownia.mobile.Models

data class Token(val token: String, val refreshToken: String)