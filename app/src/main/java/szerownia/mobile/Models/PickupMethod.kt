package szerownia.mobile.Models

enum class PickupMethod {
    PERSONAL_PICKUP, PARCEL_LOCKER, COURIER
}