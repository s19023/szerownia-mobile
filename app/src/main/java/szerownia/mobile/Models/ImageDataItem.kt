package szerownia.mobile.Models

data class ImageDataItem(
    val imageId: Long,
    val thumbnailId: Long
)