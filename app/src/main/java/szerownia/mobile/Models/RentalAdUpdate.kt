package szerownia.mobile.Models

import java.math.BigDecimal

data class RentalAdUpdate(val title: String,
                          val timeSlotsList: List<TimeSlot>,
                          val location: LocationForRentalAdRequest,
                          val pickupMethod: List<PickupMethod>,
                          val description: String,
                          val pricePerDay: BigDecimal,
                          val depositAmount: BigDecimal,
                          val penaltyForEachDayOfDelayInReturns: BigDecimal,
                          val averageOpinion: Double,
                          val averageRentalPrice: Double,
                          val minRentalHistory: Int,
                          val customRequirement: String
)
