package szerownia.mobile.Models

data class LocationForRentalAdRequest(
    val latitude: Double,
    val longitude: Double,
    val name: String
)
