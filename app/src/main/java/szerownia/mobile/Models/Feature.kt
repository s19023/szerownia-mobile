package szerownia.mobile.Models

data class Feature(
    val idFeature:Int,
    val name: String,
    val required: Boolean,
    val type: String
)
