package szerownia.mobile.Models

data class SearchAdCreate(
    val description: String,
    val location: LocationForRentalAdRequest,
    val pickupMethod: List<PickupMethod>,
    val timeSlotList: List<TimeSlot>,
    val title: String
)