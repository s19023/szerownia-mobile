package szerownia.mobile.Models

import java.time.LocalDate

data class HireRequest(
    val endDate: String,
    val rentalAdId: Long,
    val startDate: String
)