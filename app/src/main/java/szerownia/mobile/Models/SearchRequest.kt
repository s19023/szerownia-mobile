package szerownia.mobile.Models

data class SearchRequest(
    var category: Long?,
    var filters: MutableList<Filter>,
    var search: String?
)