package szerownia.mobile.API

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import szerownia.mobile.Models.*

interface UsersAPI {
    @POST("api/users") fun addUser(@Body newUser: User): Call<Void>

    @POST("login") fun login(@Body login: Login): Call<Token>

    @GET("api/users/details") fun getMyDetails(@Header("Authorization") authorization: String): Call<MyDetails>

    @POST("api/users/premium") fun changePremium(@Header("Authorization") authorization: String): Call<Void>

    @POST("api/users/exists") fun checkIfExists(@Body email: RequestBody): Call<String>

    @GET("api/users/{id}") fun getUser(@Path("id") id: Long): Call<UserRespDto>

    @POST("api/users/update") fun updateUser(
        @Header("Authorization") authorization: String, @Body userUpdate: UserUpdateRequest
    ): Call<Void>
    @POST("api/users/rtoken") fun getResetToken(@Body email:RequestBody):Call<Void>

}