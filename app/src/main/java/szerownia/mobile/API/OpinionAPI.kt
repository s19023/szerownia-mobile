package szerownia.mobile.API

import retrofit2.Call
import retrofit2.http.*
import szerownia.mobile.Models.Opinion
import szerownia.mobile.Models.RentalAdResult

interface OpinionAPI {
    @GET("api/opinions")
    fun getAllOpinions(): Call<List<Opinion>>
    @POST("api/opinions")
    fun createOpinion(@Body opinion:Opinion): Call<Void>
    @PUT("api/opinions/")
    fun updateOpinion(@Body opinion: Opinion):Call<Void>
}