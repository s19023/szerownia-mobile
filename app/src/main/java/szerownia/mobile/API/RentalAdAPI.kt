package szerownia.mobile.API

import retrofit2.Call
import retrofit2.http.*
import szerownia.mobile.Models.*

interface RentalAdAPI {

    @GET("api/rentalAd") fun getAds(
        @Query("howManyRecord") howManyRecord: Long, @Query("page") page: Long
    ): Call<RentalAdResult>

    @GET("api/rentalAd/{id}") fun getAd(@Path("id") id: Long?): Call<RentalAdById>

    @DELETE("api/rentalAd/{id}") fun deleteAd(@Path("id") id: Long): Call<Void>

    @POST("api/rentalAd/{arrayProductId}") fun createNewRentalAd(
        @Path("arrayProductId") arrayProductId: String,
        @Header("Authorization") authorization: String,
        @Body rentalAdCreate: RentalAdCreate
    ): Call<Long>

    @PUT("api/rentalAd/{rentalAdId}") fun updateRentalAd(
        @Path("rentalAdId") rentalAdId: Long,
        @Header("Authorization") authorization: String,
        @Body updateRentalAd: RentalAdUpdate
    ): Call<Void>

    @GET("api/rentalAd/myRentalAds") fun getMyAds(
        @Header("Authorization") authorization: String,
        @Query("howManyRecord") howManyRecord: Long,
        @Query("page") page: Long
    ): Call<RentalAdResult>

    @GET("api/rentalAd/pickups/{id}") fun getPickupMethods(@Path("id") id: Long): Call<List<String>>
    @GET("api/rentalAd/availableDates/{idRentalAd}")
    fun getAllAvailableDatesOfHire(@Path("idRentalAd") id:Long):Call<List<TimeSlotDTO>>

}
