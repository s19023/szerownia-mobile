package szerownia.mobile.API

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*
import szerownia.mobile.Models.ImageData
import java.io.File

interface ImageAPI {

    @Multipart
    @POST("/api/image")
    fun uploadImage(@Header("Authorization") authorization: String, @Part imageList: MultipartBody.Part): Call<ImageData>

}