package szerownia.mobile.API

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import szerownia.mobile.Models.ReportAdTicket

interface ReportAPI {
    @POST("api/tickets")
    fun createModTicket(@Body ticket: ReportAdTicket): Call<Void>
}
