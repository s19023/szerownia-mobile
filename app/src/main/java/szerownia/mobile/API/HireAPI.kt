package szerownia.mobile.API


import retrofit2.Call
import retrofit2.http.*
import szerownia.mobile.Models.*

interface HireAPI {

    @POST("/api/hires")
    fun createHire(@Header("Authorization") authorization: String, @Body hire: Hire): Call<Void>

    @POST("api/hires/estimate")
    fun calculateHireCost(@Body hireRequest: HireRequest):Call<HireEstimation>

    @GET("api/hires/myBorrows")
    fun getMyBorrows(@Header("Authorization") authorization: String) :Call<List<Borrow>>

    @PUT("api/hires/{id}")
    fun returnBorrow(@Path("id") id: Long,@Body dateToActual: String):Call<Void>

    @GET("api/hires/myShares")
    fun getMyShares(@Header("Authorization") authorization: String): Call<List<Borrow>>

}
