package szerownia.mobile.API

import retrofit2.Call
import retrofit2.http.*
import szerownia.mobile.Models.*

interface SearchAdAPI {
    @GET("api/searchingAd") fun getAllSearchAds(
        @Query("howManyRecord") howManyRecord: Long,
        @Query("page") page: Long
    ): Call<SearchAdResult>

    @GET("api/searchingAd/{id}") fun getSearchAd(@Path("id") id: Long): Call<SearchAdById>

    @POST("api/searchingAd/{arrayProductId}") fun createNewSearchAd(
        @Path("arrayProductId") arrayProductId: String,
        @Header("Authorization") authorization: String,
        @Body rentalAdCreate: SearchAdCreate
    ): Call<Void>

    @DELETE("api/searchingAd/{id}") fun deleteAd(@Path("id") id: Long, @Header("authorization") authorization:String): Call<Void>


    @PUT("api/searchingAd/{searchAdId}") fun updateSearchAd(
        @Path("searchAdId") searchAdId: Long,
        @Header("authorization") authorization: String,
        @Body updateSearchAd: SearchAdUpdate
    ): Call<Void>
}
