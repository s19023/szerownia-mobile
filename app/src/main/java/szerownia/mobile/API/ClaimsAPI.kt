package szerownia.mobile.API

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import szerownia.mobile.Models.Appraisal
import szerownia.mobile.Models.CreateClaimDTO

interface ClaimsAPI {

    @POST("/api/inspection")
    fun createInspection(@Body dto:CreateClaimDTO,@Header("authorization") token: String): Call<Void>

    @GET("/api/inspection/claimsForInspection")
    fun getClaimsForInspection(@Header("authorization") token:String) : Call<List<Appraisal>>

}