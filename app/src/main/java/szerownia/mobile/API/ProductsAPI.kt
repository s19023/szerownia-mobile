package szerownia.mobile.API

import retrofit2.Call
import retrofit2.http.*
import szerownia.mobile.Models.MyProduct
import szerownia.mobile.Models.ProductCreateRequestDTO

interface ProductsAPI {
    @GET("api/products/myProducts")
    fun getMyProducts(@Header("Authorization") authorization: String,@Query("showSearch") showSearch: Boolean): Call<List<MyProduct>>
    @POST("api/products")
    fun createProduct(@Header("Authorization") authorization: String, @Body productCreateRequestDTO: ProductCreateRequestDTO):Call<Void>

}