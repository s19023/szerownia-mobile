package szerownia.mobile.API

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import szerownia.mobile.Models.RefreshToken
import szerownia.mobile.Models.Token

interface TokenAPI {
    @POST("refreshToken")
    fun refreshToken(@Body token: RefreshToken): Call<Token>
}