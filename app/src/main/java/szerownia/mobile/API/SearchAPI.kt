package szerownia.mobile.API

import retrofit2.Call
import retrofit2.http.*
import szerownia.mobile.Models.Feature
import szerownia.mobile.Models.RentalAd
import szerownia.mobile.Models.SearchRequest
import szerownia.mobile.Models.SearchResult

interface SearchAPI {
    @GET("api/features/list/{idCategory}")
    fun getAllFeatures(@Path("idCategory") id: Long): Call<MutableList<Feature>>

    @POST("api/rentalAd/findByParams")
    fun findByParams(@Query("howManyRecord")howManyRecord: Long, @Query("page") page:Long,@Body body: SearchRequest): Call<SearchResult>
}