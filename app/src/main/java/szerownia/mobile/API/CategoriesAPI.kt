package szerownia.mobile.API

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import szerownia.mobile.Models.Category

interface CategoriesAPI {
    @GET("api/categories/")
    fun getAllCategories(): Call<List<Category>>

    @GET("api/categories/{id}")
    fun getCategory(@Path("id") id: Long?): Call<Category>
}