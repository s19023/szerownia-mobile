package szerownia.mobile.API

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import szerownia.mobile.Models.Message
import szerownia.mobile.Models.Thread

interface MessagesAPI {
    @GET("msg/threads") fun getAllThreads(@Header("Authorization") authorization: String): Call<List<Thread>>

    @GET("msg/thread/{id}") fun getAllMessagesFromThread(
        @Header("Authorization") authorization: String, @Path("id") id: String
    ): Call<List<Message>>

    @POST("msg/message/{id}") fun createMessageForExistingThread(
        @Header("Authorization") authorization: String, @Body newMessage: RequestBody, @Path("id") id: String
    ): Call<Void>

    @POST("msg/newMessage/{threadDetails}") fun createMessageForNewThread(
        @Header("Authorization") authorization: String,
        @Path("threadDetails") threadDetails: String,
        @Body message: RequestBody
    ): Call<Void>
}
