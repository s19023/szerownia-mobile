package szerownia.mobile.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.RentalAdAPI
import szerownia.mobile.API.SearchAPI
import szerownia.mobile.Adapters.RentalAdsAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.RentalAd
import szerownia.mobile.Models.RentalAdResult
import szerownia.mobile.Models.SearchRequest
import szerownia.mobile.Models.SearchResult
import szerownia.mobile.R
import java.util.concurrent.TimeUnit

class RentalAdsFragment : Fragment() {

    private var serverUrl = ""

    var searchRequest = SearchRequest(0, mutableListOf(), "")

    var filtered: Boolean = false
//    var categoryId: Long = 0L

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }

        val bundle = this.arguments



        if (bundle != null) {

            val gson = GsonBuilder().create()
            searchRequest =
                gson.fromJson(bundle.get("searchRequest").toString(), object : TypeToken<SearchRequest>() {}.type)
            filtered = bundle.getBoolean("filtered")
//            categoryId = bundle.getLong("idCategory")
        }

        return inflater.inflate(R.layout.fragment_rentalads, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bottomNavigationView = requireActivity().findViewById<BottomNavigationView>(R.id.bottom_navigation)
        val rentalAdsRecyclerView = view.findViewById<RecyclerView>(R.id.recyclerview_rentalads)
        val nothingHere:TextView = view.findViewById(R.id.noRentalAds)
        val rentalAds = mutableListOf<RentalAd>()

        val loadingDialog = LoadingDialog(view, "Odświeżanie ogłoszeń...")
        loadingDialog.showLoadingDialog()

        val okHttpClient =
            OkHttpClient().newBuilder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()

        val gson = GsonBuilder().create()
        val retrofit =
            Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).client(okHttpClient).build()
        if (!filtered) {
            val api = retrofit.create(RentalAdAPI::class.java)
            api.getAds(25, 0).enqueue(object : Callback<RentalAdResult> {
                override fun onResponse(call: Call<RentalAdResult>, response: Response<RentalAdResult>) {
                    for (element in response.body()!!.results) {
                        rentalAds.add(element)
                    }
                    if(rentalAds.size==0){
                        nothingHere.visibility=View.VISIBLE
                    }else{
                        nothingHere.visibility=View.GONE
                    }

                    val productImages = mutableListOf<Int>()
                    val locImages = mutableListOf<Int>()
                    for (item in rentalAds) {
                        productImages.add(R.drawable.szerownia_logo_512)
                        locImages.add(R.drawable.all_location_icon)
                    }

                    val adapter = RentalAdsAdapter(rentalAds, productImages, locImages)

                    val gridLayout = GridLayoutManager(activity, 2)
                    rentalAdsRecyclerView.layoutManager = gridLayout
                    rentalAdsRecyclerView.adapter = adapter

                    loadingDialog.dismissLoadingDialog()
                    return
                }

                override fun onFailure(call: Call<RentalAdResult>, t: Throwable) {
                    Log.println(Log.ERROR, javaClass.toString(), "Downloading rental ads failed with error:/n$t")
                    loadingDialog.dismissLoadingDialog()
                    Toast.makeText(context, getString(R.string.rentalads_getallfailed), Toast.LENGTH_SHORT).show()

                    val fragment: Fragment = HomeFragment()
                    val fr = activity!!.supportFragmentManager
                    bottomNavigationView.selectedItemId = R.id.menu_home
                    fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                }
            })
        } else {

            val api = retrofit.create(SearchAPI::class.java)
            api.findByParams(25, 0, searchRequest).enqueue(object : Callback<SearchResult> {
                override fun onResponse(call: Call<SearchResult>, response: Response<SearchResult>) {
                    if (response.isSuccessful) {
                        rentalAds.clear()
                        Log.e("Request", searchRequest.toString())
                        Log.e("Response", response.body().toString())


                        if (response.body() != null) {
                            for (element in response.body()!!.results) {
                                rentalAds.add(element)
                            }

                        }
                        if(rentalAds.size==0){
                            nothingHere.visibility=View.VISIBLE
                        }else{
                            nothingHere.visibility=View.GONE
                        }
                        val productImages = mutableListOf<Int>()
                        val locImages = mutableListOf<Int>()
                        for (item in rentalAds) {
                            productImages.add(R.drawable.szerownia_logo_512)
                            locImages.add(R.drawable.all_location_icon)
                        }

                        val adapter = RentalAdsAdapter(rentalAds, productImages, locImages)

                        val gridLayout = GridLayoutManager(activity, 2)
                        rentalAdsRecyclerView.layoutManager = gridLayout
                        rentalAdsRecyclerView.adapter = adapter
                        rentalAdsRecyclerView.adapter!!.notifyDataSetChanged()

                    }
                    loadingDialog.dismissLoadingDialog()
                    return
                }

                override fun onFailure(call: Call<SearchResult>, t: Throwable) {
                    Log.e("Filtered failure",t.toString())
                    loadingDialog.dismissLoadingDialog()
                    return
                }
            })
        }


    }
}
