package szerownia.mobile.Fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.CategoriesAPI
import szerownia.mobile.Adapters.SubcategoriesAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.Models.Category
import szerownia.mobile.R

class SubcategoriesFragment : Fragment() {
    private var serverUrl =""
    private var idCategory: Long = 0
    private lateinit var category: Category
    private lateinit var parentCategoryIcon: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        val bundle = this.arguments
        if (bundle != null) {
            idCategory = bundle.getLong("idCategory")
        }
        return inflater.inflate(R.layout.fragment_subcategories, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.e("Sub", "categories")
        var categoriesRecyclerView = view.findViewById<RecyclerView>(R.id.subcategoriesRecyclerView)
        val categoriesTextView: TextView = view.findViewById(R.id.subcategoriesTextView)
        val parentCategoryName: TextView = view.findViewById(R.id.parentCategoryName)
        val backIcon: ImageView = view.findViewById(R.id.backIcon)
        val parentCategory: ConstraintLayout = view.findViewById(R.id.parentCategory)
        parentCategoryIcon = view.findViewById(R.id.parentCategoryIcon)
        categoriesTextView.text = "Wszystkie kategorie"

        val gson = GsonBuilder().create()
        val retrofit = Retrofit.Builder().baseUrl(serverUrl)
            .addConverterFactory(GsonConverterFactory.create(gson)).build()

        val api = retrofit.create(CategoriesAPI::class.java)

        api.getCategory(idCategory).enqueue(object : Callback<Category> {
            override fun onResponse(call: Call<Category>, response: Response<Category>) {
                category = response.body()!!

                parentCategoryName.text = category.name
                setParentCategoryIcon(category.name)

                val adapter = SubcategoriesAdapter(category.subCategories, idCategory)

                val gridLayout = GridLayoutManager(activity, 1)
                categoriesRecyclerView.layoutManager = gridLayout
                categoriesRecyclerView.adapter = adapter
            }

            override fun onFailure(call: Call<Category>, t: Throwable) {
                Log.e(javaClass.toString(), "Downloading category failed", t)
                Toast.makeText(context, "Błąd pobierania szczegółów kategorii", Toast.LENGTH_SHORT).show()
            }
        })

        backIcon.setOnClickListener {
            (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, CategoriesFragment()).commit()
        }
    }

    private fun setParentCategoryIcon(categoryName: String) {
        when (categoryName) {
            "Motoryzacja"       -> parentCategoryIcon.setImageResource(R.drawable.categories_motoryzacja_icon)
            "Dom i Ogród"       -> parentCategoryIcon.setImageResource(R.drawable.categories_dom_i_ogrod_icon)
            "Elektronika"       -> parentCategoryIcon.setImageResource(R.drawable.categories_elektronika_icon)
            "Moda"              -> parentCategoryIcon.setImageResource(R.drawable.categories_moda_icon)
            "Rolnictwo"         -> parentCategoryIcon.setImageResource(R.drawable.categories_rolnictwo_icon)
            "Dla Dzieci"        -> parentCategoryIcon.setImageResource(R.drawable.categories_dla_dzieci_icon)
            "Sport i Hobby"     -> parentCategoryIcon.setImageResource(R.drawable.categories_sport_i_hobby_icon)
            "Muzyka i Edukacja" -> parentCategoryIcon.setImageResource(R.drawable.categories_muzyka_i_edukacja_icon)
            "Ślub i Wesele"     -> parentCategoryIcon.setImageResource(R.drawable.categories_slub_i_wesele_icon)
            else                -> parentCategoryIcon.setImageResource(R.drawable.categories_unknown_icon)
        }
    }
}