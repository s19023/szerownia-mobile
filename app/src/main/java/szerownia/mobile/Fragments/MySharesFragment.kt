package szerownia.mobile.Fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.HireAPI
import szerownia.mobile.Adapters.MyBorrowsListAdapter
import szerownia.mobile.Adapters.MyReturnsListAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.Borrow
import szerownia.mobile.R

class MySharesFragment : Fragment() {

    private var serverUrl =""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        return inflater.inflate(R.layout.fragment_my_borrows, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val title: TextView = view.findViewById(R.id.myBorrows)
        title.text = getString(R.string.myShares)
        val titleMyShares: TextView = view.findViewById(R.id.textViewMyBorrows)
        titleMyShares.text = getString(R.string.mySharesActive)

        val titleMySharesPast: TextView = view.findViewById(R.id.textViewMyReturns)
        titleMySharesPast.text = getString(R.string.mySharesPast)
        val backIcon: ImageView = view.findViewById(R.id.myBorrowsBackIcon)
        val borrows = arrayListOf<Borrow>()
        val returns = arrayListOf<Borrow>()
        val noActiveBorrows: TextView = view.findViewById(R.id.noActiveBorrows)
        val noPastBorrows: TextView = view.findViewById(R.id.noPastBorrows)
        val rc = view.findViewById<RecyclerView>(R.id.myBorrowsRecyclerView)
        val rcReturns = view.findViewById<RecyclerView>(R.id.myReturnsRecyclerView)
        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        val token = "Bearer " + sharedPreferences.getString("TOKEN", null)
        val loadingDialog = LoadingDialog(view, "Wczytywanie ogłoszeń...")
        loadingDialog.showLoadingDialog()

        val gson = GsonBuilder().create()
        val retrofit =
            Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        val api = retrofit.create(HireAPI::class.java)
        Log.e("Token", token)


        backIcon.setOnClickListener {
            val nextFragment = AccountFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, nextFragment).commit()
        }




        api.getMyShares(token).enqueue(object : Callback<List<Borrow>> {
            override fun onResponse(call: Call<List<Borrow>>, response: Response<List<Borrow>>) {
                for (element in response.body()!!) {
                    if (element.dateToActual != null) {
                        returns.add(element)
                    } else {
                        borrows.add(element)
                    }

                    if (borrows.isEmpty()) {
                        noActiveBorrows.visibility = View.VISIBLE

                    } else {
                        noActiveBorrows.visibility = View.GONE
                        rc.visibility=View.VISIBLE
                    }
                    if(returns.isEmpty()){
                        noPastBorrows.visibility = View.VISIBLE
                    }else{
                        noPastBorrows.visibility = View.GONE
                    }
                }


                val adapterReturns = MyReturnsListAdapter(returns, retrofit)
                val adapter = MyBorrowsListAdapter(borrows, retrofit,returns,noActiveBorrows,noPastBorrows,rc,adapterReturns,false)

//
                val gridLayout = GridLayoutManager(activity, 2)
                val gridLayout2 = GridLayoutManager(activity, 2)
                rc.layoutManager = gridLayout
                rc.adapter = adapter
                rcReturns.layoutManager = gridLayout2
                rcReturns.adapter = adapterReturns
                loadingDialog.dismissLoadingDialog()
                return
            }

            override fun onFailure(call: Call<List<Borrow>>, t: Throwable) {
                Log.e("Loading Borrows Error",t.toString())
                loadingDialog.dismissLoadingDialog()
                val dialogBuilder = AlertDialog.Builder(context)
                val view2 = LayoutInflater.from(context).inflate(R.layout.popup_use_web, null)
                val closeButton: ImageView = view2.findViewById(R.id.useWebButton)
                val textView:TextView = view2.findViewById(R.id.useWebMessage)
                textView.text= getString(R.string.ErrorWhileLoading)
                dialogBuilder.setView(view2)
                val dialog = dialogBuilder.create()
                dialog.show()
                closeButton.setOnClickListener { dialog.dismiss()
                    val nextFragment = AccountFragment()
                    val fr = requireActivity().supportFragmentManager
                    fr.beginTransaction().replace(R.id.fragment_container, nextFragment).commit()}

            }

        })

    }
}