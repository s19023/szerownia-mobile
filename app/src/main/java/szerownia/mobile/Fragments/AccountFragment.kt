package szerownia.mobile.Fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.*
import szerownia.mobile.API.UsersAPI
import szerownia.mobile.Models.MyDetails
import java.lang.Exception

class AccountFragment : Fragment() {

    private var isPremium = false;
    private lateinit var userDetails: MyDetails
    private var serverUrl = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        val logoutButton: TextView = view.findViewById(R.id.logoutButton)
        val userName: TextView = view.findViewById(R.id.userFirstNameAndLastName)
        val email: TextView = view.findViewById(R.id.userEmail)
        val phone: TextView = view.findViewById(R.id.userTelephoneNumber)
        val ratingBorrowerBar: RatingBar = view.findViewById(R.id.ratingBarBorrower)
        val ratingSharerBar: RatingBar = view.findViewById(R.id.ratingBarSharer)
        val buttonPremium: Button = view.findViewById(R.id.button_account_buy_premium)
        val editUserDetailsButton: Button = view.findViewById(R.id.button_account_edituserdetails)
        val searchAdsHistory: ConstraintLayout = view.findViewById(R.id.constraintlayout_account_searchads)
        val rentalAdsHistory: ConstraintLayout = view.findViewById(R.id.constraintlayout_account_rentalads)
        val mySearchAds:ConstraintLayout = view.findViewById(R.id.constraintlayout_account_mySearchads)
        val myRentalAds: ConstraintLayout = view.findViewById(R.id.constraintlayout_account_myads)

        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }

        val token = "Bearer " + sharedPreferences.getString("TOKEN", null)

        val gson = GsonBuilder().create()
        val retrofit =
            Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()

        val api = retrofit.create(UsersAPI::class.java)
        api.getMyDetails(token).enqueue(object : Callback<MyDetails> {
            @SuppressLint("SetTextI18n") override fun onResponse(call: Call<MyDetails>, response: Response<MyDetails>) {
                val myDetails = response.body()
                if (myDetails != null) {
                    userDetails = myDetails
                    isPremium = myDetails.premium
                    if (isPremium) buttonPremium.text = "Szczegóły konta premium"
                    else buttonPremium.text = "Przejdź na wersję premium"

                    userName.text = myDetails.firstName + " " + myDetails.lastName
                    email.text = myDetails.email
                    phone.text = myDetails.telephoneNumber
                    ratingBorrowerBar.rating = myDetails.averageRateBorrower.toFloat()
                    ratingSharerBar.rating = myDetails.averageRate2Sharer.toFloat()

                }
            }

            override fun onFailure(call: Call<MyDetails>, t: Throwable) {
                Log.println(Log.ERROR, javaClass.toString(), "Downloading user details failed with error:/n$t")
                Toast.makeText(requireContext(), getString(R.string.account_details_error), Toast.LENGTH_SHORT).show()
            }
        })

        searchAdsHistory.setOnClickListener {
            val nextFragment = MySharesFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, nextFragment).commit()
//            val dialogBuilder = AlertDialog.Builder(view.context)
//            val v = LayoutInflater.from(view.context).inflate(R.layout.popup_account_searchads_history, null)
//
//            val closeButton: ImageView = v.findViewById(R.id.imageview_popup_searchads_history_close)
//            val searchAdsRecyclerView: RecyclerView = v.findViewById(R.id.recyclerview_popup_searchads_history)
//
//            val apiHire = retrofit.create(HireAPI::class.java)

//            apiHire.getMyShares("Bearer " + sharedPreferences.getString("TOKEN", null))
//                .enqueue(object : Callback<List<Borrow>> {
//
//                    override fun onResponse(call: Call<List<MyShare>>, response: Response<List<MyShare>>) {
//                        println(response.body())
//                        val myShares = response.body()
//
//                        if (myShares != null) {
//                            val adapter = MySearchAdsAdapter(myShares)
//                            val gridLayoutManager = GridLayoutManager(activity, 1)
//                            searchAdsRecyclerView.layoutManager = gridLayoutManager
//                            searchAdsRecyclerView.adapter = adapter
//                        }
//
//                    }
//
//                    override fun onFailure(call: Call<List<MyShare>>, t: Throwable) {
//                        println("here\n\n\n\n")
//                        throw t
//                    }
//
//                })

//            dialogBuilder.setView(v)
//            val dialog = dialogBuilder.create()
//            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            dialog.show()
//
//            closeButton.setOnClickListener {
//                dialog.dismiss()
//            }
        }

        editUserDetailsButton.setOnClickListener {
            val fragment: Fragment = EditUserDetailsFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
        }

        buttonPremium.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(view.context)
            val v1 = LayoutInflater.from(view.context).inflate(R.layout.popup_account_premium_details, null)
            val premiumFrom: TextView = v1.findViewById(R.id.textview_popup_premium_details_premiumfrom)
            val premiumTo: TextView = v1.findViewById(R.id.textview_popup_premium_details_premiumto)
            val closePopupDetails: ImageView = v1.findViewById(R.id.imageview_popup_premium_details_close)
            val cancelPremium: Button = v1.findViewById(R.id.button_popup_account_details_endsubscription)

            val v2 = LayoutInflater.from(view.context).inflate(R.layout.popup_account_premium_buy, null)
            val buyPremiumButton: Button = v2.findViewById(R.id.button_popup_premium_buy)
            val closeBuyPremiumPopupIcon: ImageView = v2.findViewById(R.id.imageview_popup_premium_buy_close)

            if (isPremium) {
                premiumFrom.text = userDetails.premiumFrom
                premiumTo.text = userDetails.premiumTo

                dialogBuilder.setView(v1)
                val dialog = dialogBuilder.create()
                dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.show()

                cancelPremium.setOnClickListener {
                    api.changePremium(token).enqueue(object : Callback<Void> {
                        override fun onResponse(call: Call<Void>, response: Response<Void>) {
                            Toast.makeText(context, "Anulowano subskrypcję premium", Toast.LENGTH_SHORT).show()
                            dialog.dismiss()
                            val fragment: Fragment = AccountFragment()
                            val fr = requireActivity().supportFragmentManager
                            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                        }

                        override fun onFailure(call: Call<Void>, t: Throwable) {
                            Toast.makeText(context, "Anulowanie subskrypcji nie powiodło się", Toast.LENGTH_SHORT)
                                .show()
                            Log.e("Premium cancellation", t.toString())
                        }
                    })
                }

                closePopupDetails.setOnClickListener {
                    dialog.dismiss()
                }
            } else {
                dialogBuilder.setView(v2)
                val dialog = dialogBuilder.create()
                dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.show()

                buyPremiumButton.setOnClickListener {
                    api.changePremium(token).enqueue(object : Callback<Void> {
                        override fun onResponse(call: Call<Void>, response: Response<Void>) {
                            Toast.makeText(context, "Zakup premium udany", Toast.LENGTH_SHORT).show()
                            dialog.dismiss()
                            val fragment: Fragment = AccountFragment()
                            val fr = requireActivity().supportFragmentManager
                            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                        }

                        override fun onFailure(call: Call<Void>, t: Throwable) {
                            Toast.makeText(context, "Zakup konta premium nie powiódł się", Toast.LENGTH_SHORT).show()
                            Log.e("Premium purchase", t.toString())
                        }
                    })
                }

                closeBuyPremiumPopupIcon.setOnClickListener {
                    dialog.dismiss()
                }
            }
        }

        logoutButton.setOnClickListener {
            val editor = sharedPreferences.edit()
            editor.clear()  //temporary solution? not to safe
            editor.apply {
                putBoolean("LOGGED_KEY", false)
            }.apply()

            editor.apply() {
                putString("USER_NAME", null)
            }.apply()

            val fragment: Fragment = BrowseFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()

            try {
                val activity: MainActivity = requireActivity() as MainActivity
                activity.getMessagesWebSocketService().closeConnection()
            } catch (e: Exception){
                Log.println(Log.ERROR, javaClass.toString(), "Closing socket failed with error: $e")
            }
        }
        rentalAdsHistory.setOnClickListener {

            val nextFragment = MyBorrowsFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, nextFragment).commit()
        }

        myRentalAds.setOnClickListener {

            val nextFragment = MyAdsFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, nextFragment).commit()
        }
        mySearchAds.setOnClickListener {

            val nextFragment = MySearchAdsFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, nextFragment).commit()
        }
    }

}
