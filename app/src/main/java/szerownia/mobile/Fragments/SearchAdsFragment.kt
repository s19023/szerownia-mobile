package szerownia.mobile.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.SearchAdAPI
import szerownia.mobile.Adapters.SearchAdsAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.SearchAd
import szerownia.mobile.Models.SearchAdResult
import szerownia.mobile.R

class SearchAdsFragment : Fragment() {

    private var serverUrl =""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        return inflater.inflate(R.layout.fragment_searchads, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val searchAdsRecyclerView: RecyclerView = view.findViewById(R.id.recyclerview_searchads)
        val searchAds = arrayListOf<SearchAd>()

        val loadingDialog = LoadingDialog(view, "Odświeżanie ogłoszeń...")
        loadingDialog.showLoadingDialog()

        val gson = GsonBuilder().create()
        val retrofit = Retrofit.Builder().baseUrl(serverUrl)
            .addConverterFactory(GsonConverterFactory.create(gson)).build()

        val api = retrofit.create(SearchAdAPI::class.java)
        api.getAllSearchAds(25,0).enqueue(object : Callback<SearchAdResult> {
            override fun onResponse(call: Call<SearchAdResult>, response: Response<SearchAdResult>) {
                for (element in response.body()!!.results) searchAds.add(element)

                val adapter = SearchAdsAdapter(searchAds)
                val gridLayout = GridLayoutManager(activity, 2)
                searchAdsRecyclerView.layoutManager = gridLayout
                searchAdsRecyclerView.adapter = adapter

                loadingDialog.dismissLoadingDialog()
            }

            override fun onFailure(call: Call<SearchAdResult>, t: Throwable) {
                loadingDialog.dismissLoadingDialog()

                Log.e(javaClass.toString(), "Downloading search ads failed", t)
                Toast.makeText(context, "Błąd pobierania ogłoszeń", Toast.LENGTH_SHORT).show()
            }
        })
    }
}
