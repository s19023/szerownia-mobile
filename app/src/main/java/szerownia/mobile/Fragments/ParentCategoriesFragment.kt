package szerownia.mobile.Fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.CategoriesAPI
import szerownia.mobile.Adapters.CategoriesAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.Models.Category
import szerownia.mobile.R

class ParentCategoriesFragment : Fragment() {

    private var serverUrl =""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        return inflater.inflate(R.layout.fragment_parent_categories, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var categoriesRecyclerView = view.findViewById<RecyclerView>(R.id.categoriesRecyclerView)
        val categoriesTextView: TextView = view.findViewById(R.id.categoriesTextView)
        categoriesTextView.text = "Kategorie główne"

        val categories = arrayListOf<Category>()

        val gson = GsonBuilder().create()
        val retrofit = Retrofit.Builder().baseUrl(serverUrl)
            .addConverterFactory(GsonConverterFactory.create(gson)).build()

        val api = retrofit.create(CategoriesAPI::class.java)

        api.getAllCategories().enqueue(object : Callback<List<Category>> {
            override fun onResponse(call: Call<List<Category>>, response: Response<List<Category>>) {

                for (element in response.body()!!) {
                    /**dodajemy same kategorie główne póki co**/
                    if (element.parentCategory == null) categories.add(element)
                }

                val adapter = CategoriesAdapter(categories)

                val gridLayout = GridLayoutManager(activity, 1)
                categoriesRecyclerView.layoutManager = gridLayout
                categoriesRecyclerView.adapter = adapter
            }

            override fun onFailure(call: Call<List<Category>>, t: Throwable) {
                Log.e(javaClass.toString(), "Downloading categories failed failed", t)
                Toast.makeText(context, "Błąd pobierania kategorii", Toast.LENGTH_SHORT).show()
            }
        })
    }
}
