package szerownia.mobile.Fragments

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.format.Time
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatImageButton
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.squareup.timessquare.CalendarPickerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.HireAPI
import szerownia.mobile.Adapters.TimeSlotsRentAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.*
import szerownia.mobile.R
import java.math.BigDecimal
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.collections.ArrayList

class RentFragment : Fragment(){

    private var serverUrl =""
    private var rentalAdId:Long = 0L
    var dateFrom:Calendar = Calendar.getInstance()
    var dateTo: Calendar = Calendar.getInstance()
    private var pickupMethods:java.util.ArrayList<String> = ArrayList()
    private var timeSlots: MutableList<TimeSlot> = mutableListOf()
    private var newTimeSlots:MutableList<TimeSlot> = mutableListOf()
    var dateSelected = false
    lateinit var dateRanges: RecyclerView
    lateinit var dateRangesTextView:TextView
    lateinit var dateChosen:TextView
    lateinit var  pickedTimeSlot: TimeSlot
    lateinit var dateChosenCardView:CardView
    lateinit var changeDatesButton: ImageButton
    lateinit var dateFromPicked:TextView
    lateinit var dateToPicked:TextView
    lateinit var rentButton: Button
    var buttonChecked:Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        val bundle = this.arguments

        if (bundle != null) {
            rentalAdId = bundle.getLong("rentalAdId")
            val gson = GsonBuilder().create()
            timeSlots =
                gson.fromJson(bundle.get("timeSlots").toString(), object : TypeToken<List<TimeSlot>>() {}.type)
//            dateFrom.time = Date(bundle.getLong("DateFrom"))

//            dateTo.time = Date(bundle.getLong("DateTo",dateFrom.time.time))
//            pickedTimeSlot= TimeSlot(timeSlots[0].startDate,timeSlots[0].startDate)
            pickupMethods = bundle.getStringArrayList("pickupMethods")!!
        }
        if(dateFrom.time==dateTo.time){
            dateTo.add(Calendar.MONTH,2)
        }
        return inflater.inflate(R.layout.fragment_rent, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
        val retrofit = Retrofit.Builder().baseUrl(serverUrl)
            .addConverterFactory(GsonConverterFactory.create(gson)).build()
        val api = retrofit.create(HireAPI::class.java)
        val adapter = TimeSlotsRentAdapter(timeSlots,this)
        val backButton = view.findViewById<ImageView>(R.id.searchBackIcon)


        backButton.setOnClickListener {

            val rentalAdDetailsFragment = RentalAdDetailsFragment()
            val bundle = Bundle()
            bundle.putLong("id", rentalAdId)
            rentalAdDetailsFragment.arguments = bundle

            /**opening rental ad details fragment**/
            (context as FragmentActivity).supportFragmentManager.beginTransaction().setCustomAnimations(
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_left,
                R.anim.slide_in_from_right,
                R.anim.slide_out_to_right
            ).replace(R.id.fragment_container, rentalAdDetailsFragment).commit()


        }


        rentButton= view.findViewById(R.id.checkCost)

        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        val token = "Bearer " + sharedPreferences.getString("TOKEN", null).toString()
        dateRanges = view.findViewById(R.id.calendarScroll)
        dateRangesTextView = view.findViewById(R.id.choseDatesText)
        dateChosen = view.findViewById(R.id.dateChosenText)
        dateChosenCardView = view.findViewById(R.id.rentDateChosenCardView)
        changeDatesButton = view.findViewById(R.id.change_dates_button)
        dateFromPicked = view.findViewById(R.id.dateFromPicked)
        dateToPicked = view.findViewById(R.id.dateToPicked)
//        calendarPicker.init(dateFrom.time, dateTo.time).inMode(CalendarPickerView.SelectionMode.RANGE)

//        if((timeSlots[0].endDate)!=null) {
//            pickedTimeSlot = TimeSlot(timeSlots[0].endDate, timeSlots[0].startDate)
//        }else{
//
//        }

//        val numOfMonths = ChronoUnit.MONTHS.between(timeSlots[0].startDate.toInstant()
//                                      .atZone(ZoneId.systemDefault())
//                                      .toLocalDate(),timeSlots[timeSlots.size-1].endDate!!.toInstant()
//            .atZone(ZoneId.systemDefault())
//            .toLocalDate())+1


//        Log.e("NumberOfMonths",numOfMonths.toString())
        for(timeSlot in timeSlots) {
            var workedDate: Date = timeSlot.startDate
            while (workedDate.before(timeSlot.endDate)) {
                val calendar = Calendar.getInstance()
                calendar.time = workedDate
                calendar.add(Calendar.DATE, 1)
                workedDate = calendar.time
            }
        }




        val gridLayout = GridLayoutManager(context,1)
        dateRanges.adapter=adapter
        dateRanges.layoutManager=gridLayout


        changeDatesButton.setOnClickListener {
            dateSelected=false

            changeVisibility()

        }



        val radioGroup:RadioGroup = view.findViewById(R.id.radioGroup)



//        calendarPicker.setOnDateSelectedListener(object:CalendarPickerView.OnDateSelectedListener{
//            override fun onDateSelected(date: Date?) {
//                if(buttonChecked){
//                    rentButton.visibility = View.VISIBLE
//                }
//            }
//
//            override fun onDateUnselected(date: Date?) {
//                rentButton.visibility = View.GONE
//            }
//        })

        var pickupText: String =""
        for (position in pickupMethods.indices){

            val radioButton = RadioButton(context)
            radioGroup.addView(radioButton)
            if(pickupMethods[position]=="PERSONAL_PICKUP")
                radioButton.setText( R.string.textView_add_shipingOption1)
            if(pickupMethods[position]=="PARCEL_LOCKER")
                radioButton.setText( R.string.textView_add_shipingOption2)
            if(pickupMethods[position]=="COURIER")
                radioButton.setText( R.string.textView_add_shipingOption3)

            radioButton.setOnCheckedChangeListener { _, isChecked ->

                buttonChecked=true
                if(isChecked){
                    pickupText = pickupMethods[position]
                    if(dateSelected){
                        rentButton.visibility = View.VISIBLE
                    }
                }
            }
        }



        rentButton.setOnClickListener {

            val dialogBuilder = AlertDialog.Builder(context)
            val view = LayoutInflater.from(context).inflate(R.layout.popup_rental_cost, null)
            val closeButton: AppCompatImageButton = view.findViewById(R.id.rentCloseButton)
            val pricePerDay:TextView = view.findViewById(R.id.pricePerDayresult)
            val numberOfDays:TextView = view.findViewById(R.id.numberOfDaysResult)
            val discount:TextView = view.findViewById(R.id.discountResult)
            val deposit:TextView = view.findViewById(R.id.depositResult)
            val fee:TextView = view.findViewById(R.id.returnConfirmMessage)
            val sum:TextView = view.findViewById(R.id.sumResult)

            val rentButton: Button = view.findViewById(R.id.rentButton2)


            api.calculateHireCost(HireRequest(
                                                dateTo.time.toInstant()
                                                    .atZone(ZoneId.systemDefault())
                                                    .toLocalDate().toString().split('T')[0],
                                                rentalAdId,
                                                dateFrom.time.toInstant()
                                                    .atZone(ZoneId.systemDefault())
                                                    .toLocalDate().toString().split('T')[0])).enqueue(object: Callback<HireEstimation>{
                override fun onResponse(call: Call<HireEstimation>, response: Response<HireEstimation>) {
                    if(response.isSuccessful) {

                        val hireEstimation = response.body()!!

                        pricePerDay.text = NumberFormat.getCurrencyInstance(Locale("pl", "PL")).format(hireEstimation.pricePerDay)
                        numberOfDays.text = "x" + hireEstimation.period.toString()
                        val discountD:Discount = hireEstimation.usedDiscount
                        var discounted: BigDecimal = BigDecimal.ZERO
                        if(discountD!=null){
                            discounted=discountD.value
                        }

                        discount.text = NumberFormat.getCurrencyInstance(Locale("pl","PL")).format(discounted)
                        deposit.text = NumberFormat.getCurrencyInstance(Locale("pl","PL")).format(hireEstimation.depositAmount)
                        fee.text = NumberFormat.getCurrencyInstance(Locale("pl","PL")).format(hireEstimation.commission)
                        sum.text = NumberFormat.getCurrencyInstance(Locale("pl","PL")).format(hireEstimation.cost+hireEstimation.commission+hireEstimation.depositAmount-discounted)

                    }else{
//                        Log.e("Date",calendarPicker.selectedDates[calendarPicker.selectedDates.size-1].toInstant()
//                            .atZone(ZoneId.systemDefault()).toString())
                        Log.e("response",response.toString())
                    }
                    return
                }

                override fun onFailure(call: Call<HireEstimation>, t: Throwable) {
                    throw t
                }
            })

            dialogBuilder.setView(view)
            val dialog = dialogBuilder.create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.LTGRAY))
            dialog.show()

            closeButton.setOnClickListener {
                dialog.dismiss()
            }


            rentButton.setOnClickListener {
                val loadingDialog = LoadingDialog(view, "Wypożyczanie przedmiotu...")
                loadingDialog.showLoadingDialog()

                val newHire = Hire(rentalAdId,dateFrom.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate().toString().split('T')[0],dateTo.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate().toString().split('T')[0],pickupText)

                Log.e("Token",token)
                api.createHire(token,newHire).enqueue(object : Callback<Void>{
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        loadingDialog.dismissLoadingDialog()
                        dialog.dismiss()
                        val fragment: Fragment = HomeFragment()
                        val fr = requireActivity().supportFragmentManager
                        fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                        Toast.makeText(context, "Wypożyczenie udane", Toast.LENGTH_SHORT).show()
                        return
                    }

                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        loadingDialog.dismissLoadingDialog()

                        Log.e("Z wypożyczania",t.toString())
                    }
                })
            }
        }






    }
    fun changeVisibility(){
        if(dateSelected){
            val dateFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy")
            dateRanges.visibility=View.GONE
            dateRangesTextView.visibility=View.GONE
            dateChosen.visibility=View.VISIBLE
            dateChosenCardView.visibility=View.VISIBLE
            changeDatesButton.visibility=View.VISIBLE
            dateToPicked.text= dateFormat.format(pickedTimeSlot.endDate)
            dateFromPicked.text=dateFormat.format(pickedTimeSlot.startDate)
            if(buttonChecked){
                rentButton.visibility=View.VISIBLE
            }

        } else{
            dateRanges.visibility=View.VISIBLE
            dateRangesTextView.visibility=View.VISIBLE
            dateChosen.visibility=View.GONE
            dateChosenCardView.visibility=View.GONE
            changeDatesButton.visibility=View.GONE
            rentButton.visibility=View.GONE
        }
    }
}
