package szerownia.mobile.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.R

class TermsFragment : Fragment() {
    private var serverUrl =""
    private val saved : HashMap<String,String> = hashMapOf()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        val bundle = this.arguments
        if (bundle != null) {
            if(bundle.containsKey("savedValues")) {
                saved.putAll(bundle.getSerializable("savedValues") as HashMap<String, String>)
            }
        }
        return inflater.inflate(R.layout.fragment_terms, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val closeTermsFragment: ImageView = view.findViewById(R.id.imageview_terms_close)

        closeTermsFragment.setOnClickListener {
            val fragment: Fragment = RegisterFragment()
            val bundle = Bundle()
            bundle.putSerializable("savedValues",saved)
            fragment.arguments = bundle
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
        }
    }
}
