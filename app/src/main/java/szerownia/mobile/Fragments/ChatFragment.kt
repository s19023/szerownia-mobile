package szerownia.mobile.Fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.GsonBuilder
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.WebSocket
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.*
import szerownia.mobile.API.MessagesAPI
import szerownia.mobile.Adapters.ChatAdapter
import szerownia.mobile.Models.Message
import java.util.concurrent.TimeUnit

class ChatFragment : Fragment() {
    private var rentalAdId: Long = 0
    private var threadId: String = ""
    private var rentalAdTitle: String = ""
    private var serverUrl = ""
    private var messagesUrl = ""
    private var senderId: Long = 0
    private var receiverId: Long = 0
    private var senderFullName: String = ""
    private var receiverFullName: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        messagesUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_messages_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_messages_url")!!
        }

        val bundle = this.arguments

        if (bundle != null) {
            rentalAdId = bundle.getLong("rentalAdId")
            threadId = bundle.getString("threadId").toString()
            rentalAdTitle = bundle.getString("rentalAdTitle").toString()

            senderId = bundle.getLong("senderId")
            receiverId = bundle.getLong("receiverId")
            senderFullName = bundle.getString("senderFullName").toString()
            receiverFullName = bundle.getString("receiverFullName").toString()
        }

        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bottomNavigationView = requireActivity().findViewById<BottomNavigationView>(R.id.bottom_navigation)
        val messagesRecyclerView = view.findViewById<RecyclerView>(R.id.messagesRecyclerView)
        val chatSendButton: ImageView = view.findViewById(R.id.chatSendButton)
        val chatRentalAdTitle: TextView = view.findViewById(R.id.ChatRentalAdTitle)
        val textInput: TextInputLayout = view.findViewById(R.id.chatTextInput)
        val textInputEditText: TextInputEditText = view.findViewById(R.id.chatTextInputEditText)
        val scrollView: ScrollView = view.findViewById(R.id.chatScrollView)
        val receiverTextView: TextView = view.findViewById(R.id.textview_chat_receiver)
        val goToAddButton : ImageButton = view.findViewById(R.id.imagebutton_chat_gotoad)
        chatRentalAdTitle.text = rentalAdTitle



        goToAddButton.setOnClickListener {
            val rentalAdDetailsFragment = RentalAdDetailsFragment()
            val bundle = Bundle()
            bundle.putLong("id", rentalAdId)
            rentalAdDetailsFragment.arguments = bundle

            /**opening rental ad details fragment**/
            (context as FragmentActivity).supportFragmentManager.beginTransaction().setCustomAnimations(
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_left,
                R.anim.slide_in_from_right,
                R.anim.slide_out_to_right
            ).replace(R.id.fragment_container, rentalAdDetailsFragment).commit()
        }

        var sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
            getMessages(messagesRecyclerView, view)

            val userId = sharedPreferences.getString("USER_ID", null)

            if (receiverId.toString() == userId) receiverTextView.text = senderFullName
            else if (senderId.toString() == userId) receiverTextView.text = receiverFullName
            else receiverTextView.text = ""

            //scrollView.postDelayed({ scrollView.fullScroll(ScrollView.FOCUS_DOWN) }, 200)
            //scrollView.postDelayed({scrollView.scrollToBottom()}, 500)

            /*textInputEditText.setOnFocusChangeListener { view, hasFocus ->
                if (hasFocus) scrollView.postDelayed({ scrollView.fullScroll(ScrollView.FOCUS_DOWN) }, 200)
            }*/

            chatSendButton.setOnClickListener {
                if (textInput.editText!!.text.isNotEmpty()) {

                    val retrofitCreate =
                        Retrofit.Builder().baseUrl(messagesUrl).addConverterFactory(GsonConverterFactory.create())
                            .build()

                    val strRequestBody = textInputEditText.text.toString()
                    val requestBody = strRequestBody.toRequestBody("text/plain".toMediaTypeOrNull())

                    val apiCreate = retrofitCreate.create(MessagesAPI::class.java)
                    sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
                    val requestCall = apiCreate.createMessageForExistingThread(
                        "Bearer " + sharedPreferences.getString("TOKEN", null).toString(), requestBody, threadId
                    )

                    val focus = Focus()
                    focus.clearFocus(requireActivity(), requireContext())

                    requestCall.enqueue(object : Callback<Void> {
                        override fun onResponse(call: Call<Void>, response: Response<Void>) {
                            if (response.code() == 200) {
                                getMessages(messagesRecyclerView, view)
                                textInput.clearFocus()
                                textInput.editText!!.text = null
                            }
                        }

                        override fun onFailure(call: Call<Void>, t: Throwable) {
                            Log.e(javaClass.toString(), "Sending message failed", t)
                            Toast.makeText(context, "Błąd wysyłania wiadomości", Toast.LENGTH_SHORT).show()
                        }
                    })
                }
            }
        }
    }

    fun getMessages(messagesRecyclerView: RecyclerView, view: View) {
        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)

        val loadingDialog = LoadingDialog(view, "Ładowanie...")
        loadingDialog.showLoadingDialog()

        val okHttpClient =
            OkHttpClient().newBuilder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()

        val gson = GsonBuilder().create()
        val retrofit = Retrofit.Builder().baseUrl(messagesUrl).addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient).build()

        val api = retrofit.create(MessagesAPI::class.java)

        val token = sharedPreferences.getString("TOKEN", null)

        api.getAllMessagesFromThread("Bearer$token", threadId).enqueue(object : Callback<List<Message>> {
            override fun onResponse(call: Call<List<Message>>, response: Response<List<Message>>) {
                if (!response.body().isNullOrEmpty()) {
                    val adapter = ChatAdapter(
                        response.body()!!, sharedPreferences.getString("USER_ID", null).toString()
                    )
                    val gridLayout = LinearLayoutManager(activity)
                    /*gridLayout.reverseLayout = true
                    gridLayout.stackFromEnd = true*/
                    messagesRecyclerView.layoutManager = gridLayout
                    messagesRecyclerView.adapter = adapter
                }
                loadingDialog.dismissLoadingDialog()
            }

            override fun onFailure(call: Call<List<Message>>, t: Throwable) {
                Log.e(javaClass.toString(), "Downloading messages failed", t)
                loadingDialog.dismissLoadingDialog()
                Toast.makeText(context, getString(R.string.chat_getallmessagesfailed), Toast.LENGTH_SHORT).show()
            }
        })
    }
}
