package szerownia.mobile.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.SearchAPI
import szerownia.mobile.Adapters.SearchAdapter
import szerownia.mobile.Adapters.SearchBooleanAdapter
import szerownia.mobile.Adapters.SearchNumericAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.Models.Feature
import szerownia.mobile.Models.Filter
import szerownia.mobile.Models.SearchRequest
import szerownia.mobile.R

class CategorySearchFragment : Fragment() {

    private var idCategory: Long = 0


    private var serverUrl =""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        val bundle = this.arguments
        if (bundle != null) {
            idCategory = bundle.getLong("idCategory")
        }
        return inflater.inflate(R.layout.fragment_category_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val searchRecyclerView = view.findViewById<RecyclerView>(R.id.searchRecyclerView)
        val searchBooleanRecyclerView = view.findViewById<RecyclerView>(R.id.searchBooleanRecyclerView)
        val searchNumericRecyclerView = view.findViewById<RecyclerView>(R.id.searchNumericRecyclerView)
        val backIcon: ImageView = view.findViewById(R.id.searchBackIcon)
        val button: Button = view.findViewById(R.id.searchButton)
        val adapter = SearchAdapter()
        val adapterBoolean = SearchBooleanAdapter()
        val adapterNumeric = SearchNumericAdapter()
        val features: MutableList<Feature> = mutableListOf()
        val featuresBoolean :MutableList<Feature> = mutableListOf()
        val featuresNumeric :MutableList<Feature> = mutableListOf()
        val title :EditText = view.findViewById(R.id.featureName)
        val image_search_button : ImageButton =view.findViewById(R.id.image_search)

        val gson = GsonBuilder().create()
        val retrofit = Retrofit.Builder().baseUrl(serverUrl)
            .addConverterFactory(GsonConverterFactory.create(gson)).build()

        val api = retrofit.create(SearchAPI::class.java)

        api.getAllFeatures(idCategory).enqueue(object : Callback<MutableList<Feature>> {
            override fun onResponse(call: Call<MutableList<Feature>>, response: Response<MutableList<Feature>>) {
                for(element in response.body()!!){
                    if(element.type=="BOOLEAN"){
                        featuresBoolean.add(element)
                    }else{
                        if(element.type=="DOUBLE"||element.type=="INTEGER"){
                        featuresNumeric.add(element)}else{
                            features.add(element)
                        }
                    }
                }


                adapter.features = features
                adapterBoolean.features = featuresBoolean
                adapterNumeric.features = featuresNumeric

                val gridLayout = GridLayoutManager(activity, 1)
                val gridLayout2 = GridLayoutManager(activity,1)
                val gridLayout3 = GridLayoutManager(activity,1)

                searchRecyclerView.layoutManager = gridLayout
                searchRecyclerView.adapter = adapter
                searchBooleanRecyclerView.layoutManager=gridLayout2
                searchBooleanRecyclerView.adapter=adapterBoolean
                searchNumericRecyclerView.layoutManager=gridLayout3
                searchNumericRecyclerView.adapter=adapterNumeric
            }

            override fun onFailure(call: Call<MutableList<Feature>>, t: Throwable) {
                return
            }
        })

        backIcon.setOnClickListener {
            val fragment = BrowseFragment()

            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()

        }
        button.setOnClickListener {

            val searchRequest = SearchRequest(idCategory, mutableListOf(), "")
            for (filter: Filter in adapter.filters.values) {
                searchRequest.filters.add(filter)
            }
            for(filter:Filter in adapterBoolean.filters.values){
                searchRequest.filters.add(filter)
            }
            for(filter:Filter in adapterNumeric.filters.values){
                searchRequest.filters.add(filter)
            }

            searchRequest.search=title.text.toString()
            val browseFragment = BrowseFragment()
            val bundle = Bundle()

            bundle.putString("searchRequest", gson.toJson(searchRequest))
            bundle.putBoolean("filtered", true)
            bundle.putLong("idCategory", idCategory)
            browseFragment.arguments = bundle
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, browseFragment).commit()

        }
        image_search_button.setOnClickListener {

            val searchRequest = SearchRequest(idCategory, mutableListOf(), "")
            for (filter: Filter in adapter.filters.values) {
                searchRequest.filters.add(filter)
            }
            for(filter:Filter in adapterBoolean.filters.values){
                searchRequest.filters.add(filter)
            }
            for(filter:Filter in adapterNumeric.filters.values){
                searchRequest.filters.add(filter)
            }

            searchRequest.search=title.text.toString()
            val browseFragment = BrowseFragment()
            val bundle = Bundle()

            bundle.putString("searchRequest", gson.toJson(searchRequest))
            bundle.putBoolean("filtered", true)
            bundle.putLong("idCategory", idCategory)
            browseFragment.arguments = bundle
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, browseFragment).commit()

        }

    }

}


