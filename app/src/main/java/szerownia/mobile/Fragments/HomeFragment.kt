package szerownia.mobile.Fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.RentalAdAPI
import szerownia.mobile.Adapters.RentalAdsAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.Models.RentalAd
import szerownia.mobile.Models.RentalAdResult
import szerownia.mobile.R
import java.util.concurrent.TimeUnit

class HomeFragment : Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    @SuppressLint("SetTextI18n") override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*val categoriesButton = view.findViewById<Button>(R.id.categoriesButton)
        val addProductButton = view.findViewById<Button>(R.id.addProductButton)*/
        val toLocalhostSwitch = view.findViewById<SwitchCompat>(R.id.toLocalhost)
        val rc : RecyclerView = view.findViewById(R.id.PromotedAds_home)
        val promotedTitle:TextView = view.findViewById(R.id.textView20)
        val ads:MutableList<RentalAd> = mutableListOf()
        val helloTextView = view.findViewById<TextView>(R.id.textview_home_hello)
        val helloMessage = resources.getString(R.string.home_hello)
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable

        var serverUrl:String =""
        toLocalhostSwitch.isChecked = !globalVariable.distantServer
        
        toLocalhostSwitch.setOnCheckedChangeListener { _, _ ->

            globalVariable.distantServer = !globalVariable.distantServer


            serverUrl = if (globalVariable.distantServer) {
                Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
            } else {
                Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
            }
        }

        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        val okHttpClient =
            OkHttpClient().newBuilder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()

        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
        val retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient).build()
        val api = retrofit.create(RentalAdAPI::class.java)
        Log.e("AfterAd",ads.toString())
        val adapter= RentalAdsAdapter(ads, mutableListOf(), mutableListOf())
        val gridLayout = GridLayoutManager(activity, 2)
        rc.adapter=adapter
        rc.layoutManager=gridLayout

        api.getAds(25,0).enqueue(object: Callback<RentalAdResult>{
            override fun onResponse(call: Call<RentalAdResult>, response: Response<RentalAdResult>) {
                if (response.isSuccessful){
                    for (rentalAd in response.body()!!.results){
                        if(rentalAd.promoted){
                            ads.add(rentalAd)
                        }
                    }
                    adapter.notifyDataSetChanged()
                    Log.e("isSucessful",ads.toString())
                }
                if (ads.size<1){
                    promotedTitle.visibility=View.GONE
                }else{
                    promotedTitle.visibility=View.VISIBLE
                }
                return
            }

            override fun onFailure(call: Call<RentalAdResult>, t: Throwable) {
                Log.e("Promoted Ads",t.toString())
                return
            }
        })




        /*categoriesButton.setOnClickListener(this)

        addProductButton.setOnClickListener{
            val addProductFragment = AddProductFragment()
            if(it!=null){
                (it.context as FragmentActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container,addProductFragment).commit()
            }
        }*/

        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        if (sharedPreferences.getBoolean("LOGGED_KEY", false) && sharedPreferences.getString(
                "USER_NAME",
                null
            ) != null
        ) {
            val userName = sharedPreferences.getString("USER_NAME", null)
            helloTextView.text = "$helloMessage, $userName!"
        } else {
            helloTextView.text = "$helloMessage!"
        }

    }

}
