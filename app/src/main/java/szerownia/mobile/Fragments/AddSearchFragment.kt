package szerownia.mobile.Fragments

import android.app.AlertDialog
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.util.Log.*
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.GsonBuilder
import com.squareup.timessquare.CalendarPickerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.ProductsAPI
import szerownia.mobile.API.RentalAdAPI
import szerownia.mobile.API.SearchAdAPI
import szerownia.mobile.Adapters.MyProductsListAdapter
import szerownia.mobile.Adapters.TimeSlotsAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.*
import szerownia.mobile.R
import java.io.IOException
import java.math.RoundingMode
import java.util.*
import kotlin.collections.ArrayList

class AddSearchFragment : Fragment(), OnMapReadyCallback {
    private var serverUrl = ""

    val gson = GsonBuilder().create()
    lateinit var retrofit: Retrofit
    private lateinit var map: GoogleMap

    private var rentalAdId: Long = 0L
    private lateinit var rentalAdById: SearchAdById

    private var dateRanges: MutableList<TimeSlot> = mutableListOf()
    private var timeSlotAdapter = TimeSlotsAdapter(dateRanges)
    var validationCounter = 0
    private val saved: HashMap<String, String> = hashMapOf()
    private val savedD: HashMap<Date, Date?> = hashMapOf()
    /*private val bottomNavigationView: BottomNavigationView =
        requireActivity().findViewById(R.id.bottom_navigation)*/

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val bundle = this.arguments
        if (bundle != null) {
            rentalAdId = bundle.getLong("rentalAdId")
            if (bundle.containsKey("savedValues")) {
                saved.putAll(bundle.getSerializable("savedValues") as HashMap<String, String>)
                savedD.putAll(bundle.getSerializable("savedDates") as HashMap<Date, Date?>)

            }
        }

        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }

        retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()

        return inflater.inflate(R.layout.fragment_add_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)

        val dateFrom = Calendar.getInstance()
        val dateTo = Calendar.getInstance()
        dateTo.add(Calendar.MONTH, 3)

        val addTitle: TextView = view.findViewById(R.id.textview_add_search_title)

        val title: TextInputEditText = view.findViewById(R.id.textinputedittext_add_search_title)
        val description: TextInputEditText = view.findViewById(R.id.descriptionSearchInput)

        val location: TextInputEditText = view.findViewById(R.id.locationInput_search)

        setMaxLength(title, R.integer.add_maxtitlelength)
        setMaxLength(description, R.integer.add_maxdescriptionlength)

        setMaxLength(location, R.integer.add_maxlocationlength)

        val searchLocation: ImageView = view.findViewById(R.id.imageview_add_search_searchlocation)
        val addDateRangeButton: ImageView = view.findViewById(R.id.AddDateRange_search)
        val dateRangesRecyclerView: RecyclerView = view.findViewById(R.id.dateRangesRecyclerView_search)
        val noProducts: TextView = view.findViewById(R.id.textviewe_add_search_noproducts)
        val addProduct: ImageView = view.findViewById(R.id.imageviewe_add_search_addproduct)
        val dateRangeTitle: TextView = view.findViewById(R.id.datePickerDescription_search)
        val productsTitle: TextView = view.findViewById(R.id.textview_add_search_productstitle)
        val pickupTitle: TextView = view.findViewById(R.id.textview_add_Search_pickupmethodtitle)
        val locationTitle: TextView = view.findViewById(R.id.textview_add_search_locationtitle)
        val lastTitle:TextView=view.findViewById(R.id.textView21_search)
        val locationCL :ConstraintLayout= view.findViewById(R.id.constraintLayout17_search)
        val dateCL:ConstraintLayout=view.findViewById(R.id.constraintLayout16_search)
        val pickupCL:ConstraintLayout=view.findViewById(R.id.constraintLayout3_search)
        val productsTitleConstraintLayout: ConstraintLayout =
            view.findViewById(R.id.constraintlayout_add_search_productstitle)
        val productsListConstraintLayout: ConstraintLayout =
            view.findViewById(R.id.constraintlayout_add_search_productslist)

        val myProducts: MutableList<MyProduct> = mutableListOf()

        val productRecyclerView = view.findViewById<RecyclerView>(R.id.recyclerview_add_search_products)
        val pickupMethod1: CheckBox = view.findViewById(R.id.pickupMethod1_search)
        val pickupMethod2: CheckBox = view.findViewById(R.id.pickupMethod2_search)
        val pickupMethod3: CheckBox = view.findViewById(R.id.pickupMethod3_search)

        val mapView: MapView = view.findViewById(R.id.mapview_add_search_chosenlocation)
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)
        val zoomValue = 10f

        title.setText(saved["title"])
        description.setText(saved["description"])

        location.setText(saved["location"])

        pickupMethod1.isChecked = saved["pickup1"].toBoolean()
        pickupMethod2.isChecked = saved["pickup2"].toBoolean()
        pickupMethod3.isChecked = saved["pickup3"].toBoolean()

        for (key in savedD.keys) {
            dateRanges.add(TimeSlot(savedD[key], key))
        }

        val layout2 = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        dateRangesRecyclerView.adapter = timeSlotAdapter
        dateRangesRecyclerView.layoutManager = layout2

        val token = "Bearer " + sharedPreferences.getString("TOKEN", null)
        val addButton: Button = view.findViewById(R.id.button_add_search_addadvert)

        if (rentalAdId != 0L) {
            println(INFO, javaClass.toString(), "Downloading searchAd details")
            val rentalAdApi = retrofit.create(SearchAdAPI::class.java)
            rentalAdApi.getSearchAd(rentalAdId).enqueue(object : Callback<SearchAdById> {
                override fun onResponse(call: Call<SearchAdById>, response: Response<SearchAdById>) {
                    println(INFO, javaClass.toString(), "Received rentalAd details")

                    if (response.body() != null) {
                        rentalAdById = response.body()!!

                        addTitle.text = resources.getString(R.string.add_edittitle)
                        productsTitleConstraintLayout.visibility = GONE
                        productsListConstraintLayout.visibility = GONE
                        location.visibility=GONE
                        productsTitle.visibility= GONE
                        locationTitle.visibility= GONE
                        locationCL.visibility= GONE
                        pickupTitle.visibility= GONE
                        dateRangeTitle.visibility= GONE
                        pickupCL.visibility= GONE
                        dateCL.visibility=GONE
                        lastTitle.visibility=GONE




                        title.setText(rentalAdById.title)
                        description.setText(rentalAdById.description)
                        location.setText(rentalAdById.location.name)
                        try {
                            val latLng = LatLng(rentalAdById.location.latitude, rentalAdById.location.longitude)
                            map.apply {
                                addMarker(MarkerOptions().position(latLng))
                                moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomValue))
                            }
                        } catch (ex: IOException) {
                            println(WARN, javaClass.toString(), "Address not found /n$ex")
                            Toast.makeText(context, "Nie znaleziono adresu", Toast.LENGTH_SHORT).show()
                        }

                        for (pickupMethod in rentalAdById.pickupMethod) {
                            if (pickupMethod.name == PickupMethod.PERSONAL_PICKUP.toString()) pickupMethod1.isChecked =
                                true
                            if (pickupMethod.name == PickupMethod.PARCEL_LOCKER.toString()) pickupMethod2.isChecked =
                                true
                            if (pickupMethod.name == PickupMethod.COURIER.toString()) pickupMethod3.isChecked = true
                        }


                        for (i in rentalAdById.timeSlotList) {
                            dateRanges.add(i)
                        }
                        println(dateRanges)
                        timeSlotAdapter = TimeSlotsAdapter(dateRanges)
                        dateRangesRecyclerView.adapter = timeSlotAdapter
                        dateRangesRecyclerView.layoutManager = layout2

                        addButton.text = getString(R.string.add_editbutton)
                        return
                    }
                }

                override fun onFailure(call: Call<SearchAdById>, t: Throwable) {
                    println(WARN, javaClass.toString(), "Receiving rentalAd details failed")
                    Toast.makeText(context, "Nie można pobrać szczegółów ogłoszenia", Toast.LENGTH_SHORT).show()
                    return
                }
            })
        }

        addDateRangeButton.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(context)
            val addDateView = LayoutInflater.from(context).inflate(R.layout.popup_date_range_picker, null)
            val closeButton: ImageView = addDateView.findViewById(R.id.dateRangeCloseButton)
            val calendarPicker: CalendarPickerView = addDateView.findViewById(R.id.addDateRangeCalendarPicker)
            val confirmButton: Button = addDateView.findViewById(R.id.confirmDateRange)
            calendarPicker.init(dateFrom.time, dateTo.time).inMode(CalendarPickerView.SelectionMode.RANGE)

            dialogBuilder.setView(addDateView)
            val dialog = dialogBuilder.create()
            dialog.show()

            closeButton.setOnClickListener {
                dialog.dismiss()
            }

            confirmButton.setOnClickListener {
                dateRanges.add(
                    TimeSlot(
                        calendarPicker.selectedDates[calendarPicker.selectedDates.size - 1],
                        calendarPicker.selectedDates[0]
                    )
                )
                dateRangeTitle.error = null
                dialog.dismiss()
                timeSlotAdapter.notifyDataSetChanged()
            }

            calendarPicker.setOnDateSelectedListener(object : CalendarPickerView.OnDateSelectedListener {
                override fun onDateSelected(date: Date?) {
                    if (calendarPicker.selectedDates.size > 1) {
                        confirmButton.visibility = VISIBLE
                    }
                }

                override fun onDateUnselected(date: Date?) {
                    confirmButton.visibility = GONE
                }
            })

        }
        addProduct.setOnClickListener {
            val addProductFragment = AddProductFragment()
            val bundle = Bundle()
            bundle.putBoolean("searchProduct", true)
            val savedValues: HashMap<String, String?> = hashMapOf()
            savedValues["title"] = title.text.toString()
            savedValues["location"] = location.text.toString()
            savedValues["description"] = description.text.toString()
            savedValues["pickup1"] = pickupMethod1.isChecked.toString()
            savedValues["pickup2"] = pickupMethod2.isChecked.toString()
            savedValues["pickup3"] = pickupMethod3.isChecked.toString()

            val savedDates: HashMap<Date, Date?> = hashMapOf()
            for (timeslot in dateRanges) {
                savedDates[timeslot.startDate] = timeslot.endDate
            }


            bundle.putSerializable("savedDates", savedDates)
            bundle.putSerializable("savedValues", savedValues)
            addProductFragment.arguments = bundle
            if (it != null) {
                (it.context as FragmentActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, addProductFragment).commit()
            }
        }

        val api = retrofit.create(ProductsAPI::class.java)
        lateinit var productsListAdapter: MyProductsListAdapter
        var myProductsGridLayoutManager: GridLayoutManager

        api.getMyProducts(token, true).enqueue(object : Callback<List<MyProduct>> {
            override fun onResponse(call: Call<List<MyProduct>>, response: Response<List<MyProduct>>) {
                if (response.isSuccessful) {
                    myProducts.addAll(response.body()!!)
//                     myProducts response.body()!!
                    productsListAdapter = MyProductsListAdapter(myProducts)
                    myProductsGridLayoutManager = GridLayoutManager(activity, 1)
                    productRecyclerView.layoutManager = myProductsGridLayoutManager
                    productRecyclerView.adapter = productsListAdapter
                }
                if (myProducts.isNotEmpty()) {
                    noProducts.visibility = GONE
                }
                return
            }

            override fun onFailure(call: Call<List<MyProduct>>, t: Throwable) {
                e("Add Rental Ad Error", t.message.toString())
            }
        })

        val geocoder = Geocoder(context)
        var address: List<Address>
        var p1: LatLng? = null

        searchLocation.setOnClickListener {
            if (location.text.toString().isEmpty()) {
                Toast.makeText(context, "Wpisz adres", Toast.LENGTH_SHORT).show()
            } else {
                try {
                    address = geocoder.getFromLocationName(location.text.toString(), 5)
                    if (address != null) {
                        val locationResult = address[0]
                        p1 = LatLng(locationResult.latitude, locationResult.longitude)
                        println(
                            INFO, javaClass.toString(), "Address found. Lat: ${p1!!.latitude}, Lon: ${p1!!.longitude}"
                        )

                        map.apply {
                            addMarker(MarkerOptions().position(p1!!))
                            moveCamera(CameraUpdateFactory.newLatLngZoom(p1!!, zoomValue))
                        }
                    } else {
                        Toast.makeText(context, "Nie znaleziono adresu", Toast.LENGTH_SHORT).show()
                    }
                } catch (ex: IOException) {
                    println(WARN, javaClass.toString(), "Address not found /n$ex")
                    Toast.makeText(context, "Nie znaleziono adresu", Toast.LENGTH_SHORT).show()
                }
            }
        }

        addButton.setOnClickListener {
            Log.e("Button Clicked", rentalAdId.toString())
            val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
            val retrofit =
                Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()
            val searchAdAPI = retrofit.create(SearchAdAPI::class.java)


            val loadingDialog: LoadingDialog
            if (rentalAdId != 0L) {
                loadingDialog = LoadingDialog(view, "Zapisywanie ogłoszenia...")
                loadingDialog.showLoadingDialog()

                validationCounter=0

                validateRangedField(
                    title,
                    R.string.all_emptymale,
                    R.string.add_title,
                    R.integer.add_maxtitlelength,
                    R.integer.add_mintitlelength
                )

                validateOnlyLength(description, R.string.add_description, R.integer.add_maxdescriptionlength)
                if(validationCounter>0){
                    loadingDialog.dismissLoadingDialog()
                }else{e("updating",rentalAdId.toString())
                    val updatedRentalAd = SearchAdUpdate(
                        description.text.toString(),
//                        LocationForRentalAdRequest(p1!!.latitude, p1!!.longitude, location.text.toString()),
//                        pickupMethodList,
//                        dateRanges,
                        title.text.toString()
                    )

                    searchAdAPI.updateSearchAd(rentalAdId, token, updatedRentalAd).enqueue(object : Callback<Void> {
                        override fun onResponse(call: Call<Void>, response: Response<Void>) {
                            loadingDialog.dismissLoadingDialog()

                            if (response.code() == 200) {
                                println(INFO, javaClass.toString(), "Updated ad")
                                Toast.makeText(context, "Zapisano zmiany ogłoszenia", Toast.LENGTH_SHORT).show()
                                val fragment: Fragment = MySearchAdsFragment()
                                val fr = activity!!.supportFragmentManager
//                                    bottomNavigationView.selectedItemId = R.id.menu_home
                                fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                            } else {
                                println(
                                    WARN,
                                    javaClass.toString(),
                                    "Error while updating ad with response code: ${response.code()}"
                                )
                                Toast.makeText(
                                    context,
                                    "Nie można zapisać zmian ogłoszenia, błąd ${response.code()}",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                        override fun onFailure(call: Call<Void>, t: Throwable) {
                            loadingDialog.dismissLoadingDialog()

                            println(ERROR, javaClass.toString(), "Updating ad failed /n$t")
                            Toast.makeText(context, "Nie można zapisać zmian ogłoszenia", Toast.LENGTH_SHORT).show()
                        }

                    })
                }


            } else {
                loadingDialog = LoadingDialog(view, "Tworzenie ogłoszenia...")
                loadingDialog.showLoadingDialog()
            }



            val pickupMethodList = ArrayList<PickupMethod>()
            if (pickupMethod1.isChecked) pickupMethodList.add(PickupMethod.PERSONAL_PICKUP)
            if (pickupMethod2.isChecked) pickupMethodList.add(PickupMethod.PARCEL_LOCKER)
            if (pickupMethod3.isChecked) pickupMethodList.add(PickupMethod.COURIER)


            validationCounter = 0

            validateRangedField(
                title,
                R.string.all_emptymale,
                R.string.add_title,
                R.integer.add_maxtitlelength,
                R.integer.add_mintitlelength
            )

            validateOnlyLength(description, R.string.add_description, R.integer.add_maxdescriptionlength)

            validateRangedField(
                location,
                R.string.all_emptyfemale,
                R.string.add_location,
                R.integer.add_maxlocationlength,
                R.integer.add_minlocationlength
            )

            setTextViewErrorStatus(
                productsTitle, R.string.add_noproducts, productsListAdapter.checkedProducts.toMutableList()
            )

            setTextViewErrorStatus(pickupTitle, R.string.add_nopickup, pickupMethodList.toMutableList())

            setTextViewErrorStatus(dateRangeTitle, R.string.add_emptydateranges, dateRanges.toMutableList())

            if (!location.text.isNullOrEmpty()) {
                try {
                    address = geocoder.getFromLocationName(location.text.toString(), 5)
                    if (!address.isNullOrEmpty()) {
                        val locationResult = address[0]
                        p1 = LatLng(locationResult.latitude, locationResult.longitude)
                        println(
                            INFO, javaClass.toString(), "Address found. Lat: ${p1!!.latitude}, Lon: ${p1!!.longitude}"
                        )
                    } else {
                        validationCounter++
                        setTextViewErrorStatus(locationTitle, R.string.add_addressnotfound, address.toMutableList())

                    }
                } catch (ex: IOException) {
                    println(ERROR, javaClass.toString(), "Address not found, geocoder not working /n$ex")
                    validationCounter++
                    setTextViewErrorStatus(locationTitle, R.string.add_geocoderfailed, emptyList<Any>().toMutableList())
                }
            }

            if (validationCounter > 0) {
                loadingDialog.dismissLoadingDialog()
            } else {
                if (rentalAdId != 0L) {


                } else {
                    val checkedProducts: ArrayList<Int> = productsListAdapter.checkedProducts
                    println(INFO, javaClass.toString(), "Number of checked products in ad: $checkedProducts")

                    val newRentalAd = SearchAdCreate(
                        description.text.toString(),
                        LocationForRentalAdRequest(p1!!.latitude, p1!!.longitude, location.text.toString()),
                        pickupMethodList,
                        dateRanges,
                        title.text.toString()
                    )


                    searchAdAPI.createNewSearchAd(checkedProducts[0].toString(), token, newRentalAd)
                        .enqueue(object : Callback<Void> {
                            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                loadingDialog.dismissLoadingDialog()

                                if (response.code() == 200 || response.code() == 201) {
                                    println(INFO, javaClass.toString(), "Created new ad")
                                    Toast.makeText(context, "Dodano nowe ogłoszenie", Toast.LENGTH_SHORT).show()
                                    val fragment: Fragment = HomeFragment()
                                    val fr = activity!!.supportFragmentManager
                                    //bottomNavigationView.selectedItemId = R.id.menu_home
                                    fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                                } else {
                                    println(
                                        WARN,
                                        javaClass.toString(),
                                        "Error while creating new ad with response code: ${response.code()}"
                                    )

                                }
                            }

                            override fun onFailure(call: Call<Void>, t: Throwable) {
                                loadingDialog.dismissLoadingDialog()

                                println(ERROR, javaClass.toString(), "Adding ad failed /n$t")
                                Toast.makeText(context, "Nie można dodać ogłoszenia", Toast.LENGTH_SHORT).show()
                            }
                        })
                }
            }
        }
    }

    private fun setTextViewErrorStatus(inputField: TextView, errorStringId: Int, listToValidate: MutableList<Any>) {
        if (listToValidate.isEmpty()) {
            validationCounter++
            inputField.error = getString(errorStringId)

            inputField.setOnClickListener {
                inputField.requestFocus()
            }

        } else {
            inputField.error = null
        }
    }

    private fun setMaxLength(inputField: TextInputEditText, fieldMaxLength: Int) {
        inputField.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(resources.getInteger(fieldMaxLength)))
    }

    private fun validateOnlyLength(inputField: TextInputEditText, fieldNameId: Int, fieldMaxLength: Int) {
        if (inputField.text!!.length > resources.getInteger(fieldMaxLength)) {
            validationCounter++
            inputField.error = resources.getQuantityString(
                R.plurals.all_toolong,
                resources.getInteger(fieldMaxLength),
                resources.getInteger(fieldMaxLength),
                getString(fieldNameId)
            )
        }
    }

    private fun validateRangedField(
        inputField: TextInputEditText, emptyId: Int, fieldNameId: Int, fieldMaxLength: Int, fieldMinLength: Int
    ) {
        when {
            inputField.text.isNullOrEmpty() -> {
                validationCounter++
                inputField.error = getString(emptyId, getString(fieldNameId))
            }
            inputField.text!!.length > resources.getInteger(fieldMaxLength) -> {
                validationCounter++
                inputField.error = resources.getQuantityString(
                    R.plurals.all_toolong,
                    resources.getInteger(fieldMaxLength),
                    resources.getInteger(fieldMaxLength),
                    getString(fieldNameId)
                )
            }
            inputField.text!!.length < resources.getInteger(fieldMinLength) -> {
                validationCounter++
                inputField.error = resources.getQuantityString(
                    R.plurals.all_tooshort,
                    resources.getInteger(fieldMinLength),
                    resources.getInteger(fieldMinLength),
                    getString(fieldNameId)
                )
            }
        }
    }

    private fun validateNumbers(
        inputField: TextInputEditText, emptyId: Int, fieldNameId: Int, fieldMaxLength: Int, fieldMinValue: Int
    ) {
        when {
            inputField.text!!.isEmpty() -> {
                validationCounter++
                inputField.error = resources.getString(emptyId, getString(fieldNameId))
            }
            inputField.text!!.length > resources.getInteger(fieldMaxLength) -> {
                validationCounter++
                inputField.error = resources.getQuantityString(
                    R.plurals.all_toolong,
                    resources.getInteger(fieldMaxLength),
                    resources.getInteger(fieldMaxLength),
                    getString(fieldNameId)
                )
            }
            inputField.text!!.toString().toDouble().toBigDecimal().setScale(2, RoundingMode.HALF_UP)
                .toDouble() < resources.getInteger(fieldMinValue) -> {
                validationCounter++
                inputField.error = resources.getString(
                    R.string.add_pricetoolow, resources.getInteger(fieldMinValue), getString(fieldNameId)
                )
            }
        }
    }

    override fun onMapReady(map: GoogleMap) {
        this.map = map!!

        map.apply {
            val polska = LatLng(52.14, 19.7)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(polska, 5f))
        }
    }
}
