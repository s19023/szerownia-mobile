package szerownia.mobile.Fragments.Appraiser

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.GsonBuilder
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.ClaimsAPI
import szerownia.mobile.API.ImageAPI
import szerownia.mobile.Adapters.AppraisalPhotosAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.CreateClaimDTO
import szerownia.mobile.Models.ImageData
import szerownia.mobile.R
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*

class AssignedAppraisalFragment : Fragment() {

    private var serverUrl = ""
    var claimId = -1L
    val gson = GsonBuilder().create()
    lateinit var rc :RecyclerView
    lateinit var retrofit: Retrofit
    lateinit var imageBitmap: Bitmap
    val photos: MutableList<Bitmap> = mutableListOf()
    lateinit var currentPhotoPath: String
    val imageData: MutableList<Long> = mutableListOf()
    val adapter = AppraisalPhotosAdapter(photos)
    var value = 0L

    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            imageBitmap = it.data!!.extras!!.get("data") as Bitmap
            photos.add(imageBitmap)
            Log.i("PhotoAdded","Photo Added")
        }
        rc.visibility = View.VISIBLE
        adapter.notifyDataSetChanged()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        val bundle = this.arguments
        if (bundle != null) {
            claimId = bundle.getLong("id")
            value= bundle.getLong("value")

        }

        retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()
        return inflater.inflate(R.layout.fragment_assigned_appraisal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        val token = "Bearer " + sharedPreferences.getString("TOKEN", null)
        rc = requireView().findViewById(R.id.addPhotoRecyclerView2)
        val addPhotoButton:ImageButton = view.findViewById(R.id.addImageButton2)
        val appraisalValue: TextInputEditText = view.findViewById(R.id.appraisalInput)
        val opinion: TextView = view.findViewById(R.id.appraisalPhotoInput)
        val submitButton:Button = view.findViewById(R.id.button2)
        val appraisalHint: TextInputLayout = view.findViewById(R.id.appraisalInputLayout)


        appraisalHint.hint = appraisalHint.hint.toString() + " max: "+value.toString()
        addPhotoButton.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    requireContext(), Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_DENIED
            ) {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA), 0)

            }else{
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultLauncher.launch(takePictureIntent)
            }
        }

        submitButton.setOnClickListener {

            if(appraisalValue.text.toString().toLong()>value){
                appraisalValue.error = "Kwota wyceny nie może być większa niż kwota polisy"
            }else{

                val loadingDialog = LoadingDialog(view, "Tworzenie sprawozdania...")
                loadingDialog.showLoadingDialog()
                val imageIds = mutableListOf<Long>()
                for (photo in photos) {
                    val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.GERMANY).toString()
                    val storageDir: File = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
                    val file = File.createTempFile(
                        "JPEG_${timeStamp}_", ".jpg", storageDir
                    ).apply {
                        currentPhotoPath = absolutePath
                    }

                    try {
                        // Get the file output stream
                        val stream: OutputStream = FileOutputStream(file)

                        // Compress bitmap
                        photo.compress(Bitmap.CompressFormat.JPEG, 100, stream)

                        // Flush the stream
                        stream.flush()

                        // Close stream
                        stream.close()
                    } catch (e: IOException) { // Catch the exception
                        e.printStackTrace()
                    }

                    val imageList = MultipartBody.Part.createFormData(
                        "imageList", file.name, file.asRequestBody("image/*".toMediaType())
                    )

                    val api = retrofit.create(ImageAPI::class.java)
                    api.uploadImage(token, imageList).enqueue(object : Callback<ImageData> {
                        override fun onResponse(call: Call<ImageData>, response: Response<ImageData>) {
                            Log.i("imageUpload",response.toString())
                            if (response.isSuccessful) {
                                imageData.add(response.body()!![0].imageId)


                                val imageNames:MutableList<String> = mutableListOf()
//            for(i in 0 ..photos.size){
//                imageIds.add(imageData.get(i).get(0).imageId)
//                imageNames.add("")
//            }

                                val newReport =CreateClaimDTO(claimId,appraisalValue.text.toString().toInt(),opinion.text.toString(),imageIds,imageNames,Calendar.getInstance().time
                                )

                                val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
                                val retrofit =
                                    Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()
                                val claimsAPI = retrofit.create(ClaimsAPI::class.java)

                                claimsAPI.createInspection(newReport,token).enqueue(object: Callback<Void>{
                                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                        Toast.makeText(context, "Upload Successful", Toast.LENGTH_SHORT).show()
                                        loadingDialog.dismissLoadingDialog()
                                        requireActivity().supportFragmentManager.beginTransaction().replace(R.id.appraiserFragmentContainer, AppraiserHomeFragment()).commit()
                                    }

                                    override fun onFailure(call: Call<Void>, t: Throwable) {
                                        Toast.makeText(context, "Upload Unsuccessful", Toast.LENGTH_SHORT).show()
                                        Log.e("Appraisal",t.toString())
                                        loadingDialog.dismissLoadingDialog()
                                    }
                                })
                                Log.i("imageDataAdded","imageDataAdded")
                                return
                            }
                        }

                        override fun onFailure(call: Call<ImageData>, t: Throwable) {
                            Log.e("Error", t.message.toString())
                            return
                        }
                    })
                }


            }



        }

        rc.adapter = adapter
        rc.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

    }


}