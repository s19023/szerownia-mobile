package szerownia.mobile.Fragments.Appraiser

import android.content.Context
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.ClaimsAPI
import szerownia.mobile.API.UsersAPI
import szerownia.mobile.Adapters.AssignmentsAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.Appraisal
import szerownia.mobile.Models.Assignment
import szerownia.mobile.Models.SearchRequest
import szerownia.mobile.R
import java.util.*

class AppraiserHomeFragment: Fragment() {

    private var serverUrl = ""

    val gson = GsonBuilder().create()
    lateinit var retrofit: Retrofit

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }

        retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()
        return inflater.inflate(R.layout.fragment_appraiser_home, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val rc = view.findViewById<RecyclerView>(R.id.AppraiserHomeRecyclerView)
        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        val assignments:MutableList<Appraisal> = mutableListOf()
        val adapter  = AssignmentsAdapter(assignments)
        val noClaims = view.findViewById<TextView>(R.id.noClaimsTextView)
        val layout = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        val loadingDialog = LoadingDialog(view, "Wczytywanie ogłoszeń...")
        loadingDialog.showLoadingDialog()
        rc.adapter=adapter
        rc.layoutManager=layout
        val api = retrofit.create(ClaimsAPI::class.java)

        val token = "Bearer " + sharedPreferences.getString("TOKEN", null)

        Log.e("Token",token)
        api.getClaimsForInspection(token).enqueue(object: Callback<List<Appraisal>>{
            override fun onResponse(call: Call<List<Appraisal>>, response: Response<List<Appraisal>>) {
                    Log.e("response",response.body().toString())
                    assignments.clear()
                if(response.isSuccessful) {
                    for (element in response.body()!!) {
                        assignments.add(element)
                    }
                    if (assignments.size == 0) {
                        noClaims.visibility = VISIBLE
                    } else {
                        noClaims.visibility = GONE
                    }

                    loadingDialog.dismissLoadingDialog()
                }
                adapter.notifyDataSetChanged()
                return
            }

            override fun onFailure(call: Call<List<Appraisal>>, t: Throwable) {
                loadingDialog.dismissLoadingDialog()
                Log.e("Getting Appraisals",t.toString())
            }
        })


        adapter.notifyDataSetChanged()

    }
}