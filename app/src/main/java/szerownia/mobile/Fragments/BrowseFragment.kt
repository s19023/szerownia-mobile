package szerownia.mobile.Fragments

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.*
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.CategoriesAPI
import szerownia.mobile.API.SearchAPI
import szerownia.mobile.Adapters.CustomArrayAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.Category
import szerownia.mobile.Models.Filter
import szerownia.mobile.Models.RentalAd
import szerownia.mobile.Models.SearchRequest
import szerownia.mobile.R
import java.util.concurrent.TimeUnit

class BrowseFragment : Fragment() {

    private var serverUrl = ""
    lateinit var searchRequest: SearchRequest

    var filtered: Boolean = false
    var categoryId: Long = 0L
    var isRentalAd: Boolean = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }

        val bundle = this.arguments
        if (bundle != null) {
            val gson = GsonBuilder().create()
            searchRequest =
                gson.fromJson(bundle.get("searchRequest").toString(), object : TypeToken<SearchRequest>() {}.type)
            filtered = bundle.getBoolean("filtered")
            categoryId = bundle.getLong("idCategory")
        }
        return inflater.inflate(R.layout.fragment_browse, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
            .replace(R.id.frame_browse, RentalAdsFragment()).commit()

        val searchBox = view.findViewById<SearchView>(R.id.searchBar)
        val categoryChoice = view.findViewById<AutoCompleteTextView>(R.id.categoryChoice)
        val tabLayout: TabLayout = view.findViewById(R.id.tablayout_browse_ads)
        val categoryChoiceLayout = view.findViewById<TextInputLayout>(R.id.categoryChoiceLayout)
        val constraintLayout10 = view.findViewById<ConstraintLayout>(R.id.constraintLayout10)
        tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.text) {
                    "Ogłoszenia użyczeń" -> {
                        (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
                            .setCustomAnimations(
                                R.anim.slide_in_from_right,
                                R.anim.slide_out_to_right,
                                R.anim.slide_in_from_left,
                                R.anim.slide_out_to_left
                            ).replace(R.id.frame_browse, RentalAdsFragment()).commit()
                        isRentalAd = true
                        constraintLayout10.visibility = VISIBLE
                        searchBox.visibility = VISIBLE
                        categoryChoice.visibility = VISIBLE
                        categoryChoiceLayout.visibility = VISIBLE
                    }
                    "Ogłoszenia poszukiwania" -> {
                        (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
                            .setCustomAnimations(
                                R.anim.slide_in_from_left,
                                R.anim.slide_out_to_left,
                                R.anim.slide_in_from_right,
                                R.anim.slide_out_to_right
                            ).replace(R.id.frame_browse, SearchAdsFragment()).commit()
                        isRentalAd = false
                        constraintLayout10.visibility = GONE
                        searchBox.visibility = GONE
                        categoryChoice.visibility = GONE
                        categoryChoiceLayout.visibility = GONE
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
        val okHttpClient =
            OkHttpClient().newBuilder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()

        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
        val retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient).build()
        val api = retrofit.create(CategoriesAPI::class.java)

        val categoryNames = mutableListOf<String>()
        categoryNames.add("Wszstkie")
        val categories = mutableListOf<Category>()
        var chosenCategory = Category(0, "", null, mutableListOf(), true)
        var searchName= ""
        val detailedSearch = view.findViewById<TextView>(R.id.detailedSearch)
        categoryChoice.setHint(R.string.hintCategory)
        val unicodeMap:HashMap<String,String> = hashMapOf()

        val m:HashMap<String,String> = HashMap()
        unicodeMap.put("Motoryzacja", "\uD83D\uDE97")
        unicodeMap.put("Dom i Ogród", "\uD83C\uDFE1")
        unicodeMap.put("Elektronika", "\uD83D\uDDA5")
        unicodeMap.put("Moda", "\uD83D\uDC57")
        unicodeMap.put("Rolnictwo", "\uD83D\uDE9C")
        unicodeMap.put("Dla Dzieci", "\uD83D\uDC76")
        unicodeMap.put("Sport i Hobby", "\uD83E\uDD3A")
        unicodeMap.put("Muzyka i Edukacja", "\uD83C\uDFBC")
        unicodeMap.put("Ślub i Wesele", "\uD83D\uDC70\uD83C\uDFFD")


        val arrayAdapter = CustomArrayAdapter(requireContext(), R.layout.dropdown_item, categories,categoryNames,unicodeMap)

        //Wczytywanie nazw kategorii
        api.getAllCategories().enqueue(object : Callback<List<Category>> {
            override fun onResponse(call: Call<List<Category>>, response: Response<List<Category>>) {
                for (element in response.body()!!) {
//                    if (!element.abstract) {
                        categoryNames.add(element.name)

                        if (categoryId == element.idCategory) {
                            categoryChoice.setHint(element.name)
//                        }
                    }
                    categories.add(element)

                }
                arrayAdapter.notifyDataSetChanged()
                categoryChoice.setAdapter(arrayAdapter)
                return
            }

            override fun onFailure(call: Call<List<Category>>, t: Throwable) {
                return
            }
        })


        val rentalAds = arrayListOf<RentalAd>()




        categoryChoice.setOnItemClickListener { parent, view, position, id ->

            if(id==0L){
                val fragment = BrowseFragment()
                val fr = requireActivity().supportFragmentManager
                fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
            }



            //Wysłanie zapytania o produkty z kategorii bez tekstu

            for (category in categories) {
                if (category.name == categoryNames[position]) {
                    chosenCategory = category
                }
            }


                searchRequest= SearchRequest(
                    chosenCategory.idCategory, mutableListOf<Filter>(), "")




            val fragment = RentalAdsFragment()
            val bundle = Bundle()
            bundle.putBoolean("filtered", true)
            bundle.putSerializable("searchRequest", gson.toJson(searchRequest))
            fragment.arguments = bundle
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.frame_browse, fragment).commit()

            if(!chosenCategory.abstract) {
                detailedSearch.setText(R.string.detailedSearch)
                detailedSearch.setOnClickListener {

                    val fragment = CategorySearchFragment()
                    val bundle = Bundle()
                    bundle.putLong("idCategory", chosenCategory.idCategory)
                    fragment.arguments = bundle
                    val fr = requireActivity().supportFragmentManager
                    fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                }
            }

            for(category in categories){
                if(category.abstract){
                    if (category.idCategory==id){
                        Log.e("idCategory",category.idCategory.toString())
                        Log.e("idListener",id.toString())
                        val fragment1 = SubcategoriesFragment()
                        val bundle1 = Bundle()
                        bundle1.putLong("idCategory",category.idCategory)
                        fragment1.arguments = bundle1
                        val fr1 = requireActivity().supportFragmentManager
                        fr1.beginTransaction().replace(R.id.frame_browse, fragment1).commit()
                    }
                }
            }
        }

        searchBox.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                val imm: InputMethodManager = context!!.getSystemService(
                    Activity.INPUT_METHOD_SERVICE
                ) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)

                if (query.isNullOrBlank()) {
                    (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
                        .replace(R.id.frame_browse, RentalAdsFragment()).commit()

                    val imm: InputMethodManager = context!!.getSystemService(
                        Activity.INPUT_METHOD_SERVICE
                    ) as InputMethodManager
                    imm.hideSoftInputFromWindow(view.windowToken, 0)

                } else {
                    val api = retrofit.create(SearchAPI::class.java)


                    if (categoryChoice.text.isNullOrBlank()) {
                        searchRequest= SearchRequest(null, mutableListOf(),query)
                    }else{
                        searchRequest= SearchRequest(chosenCategory.idCategory, mutableListOf(),query)
                    }

                    if (query.length >= 3) {
                        if (isRentalAd) {
                            val fragment = RentalAdsFragment()
                            val bundle = Bundle()
                            bundle.putBoolean("filtered", true)
                            bundle.putSerializable("searchRequest", gson.toJson(searchRequest))
                            fragment.arguments = bundle
                            val fr = requireActivity().supportFragmentManager
                            fr.beginTransaction().replace(R.id.frame_browse, fragment).commit()
                        } else {
                            val fragment = SearchAdsFragment()
                            val fr = requireActivity().supportFragmentManager
                            fr.beginTransaction().replace(R.id.frame_browse, fragment).commit()
                        }
                    }
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                return true;
            }
        })

        if (filtered) {



            if (isRentalAd) {
                val fragment = RentalAdsFragment()
                val bundle = Bundle()
                bundle.putBoolean("filtered", true)
                bundle.putSerializable("searchRequest", gson.toJson(searchRequest))
                fragment.arguments = bundle
                val fr = requireActivity().supportFragmentManager
                fr.beginTransaction().replace(R.id.frame_browse, fragment).commit()
            } else {
                val fragment = SearchAdsFragment()
                val fr = requireActivity().supportFragmentManager
                fr.beginTransaction().replace(R.id.frame_browse, fragment).commit()
            }
        }
    }
}
