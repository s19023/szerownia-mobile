package szerownia.mobile.Fragments

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.text.method.DigitsKeyListener
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.RatingBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cottacush.android.currencyedittext.CurrencyEditText
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.CategoriesAPI
import szerownia.mobile.API.ProductsAPI
import szerownia.mobile.API.SearchAPI
import szerownia.mobile.Adapters.AddProductBooleanAdapter
import szerownia.mobile.Adapters.AddProductNonBooleanAdapter
import szerownia.mobile.Adapters.CustomArrayAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.Models.Category
import szerownia.mobile.Models.Feature
import szerownia.mobile.Models.PostFeatureValue
import szerownia.mobile.Models.ProductCreateRequestDTO
import szerownia.mobile.R
import java.util.*
import kotlin.collections.HashMap

class AddProductFragment : Fragment() {

    var categoryId: Long = 0L
    private var serverUrl = ""
    var searchProduct: Boolean = true
    private val savedValues: HashMap<String, String> = hashMapOf()
    private val savedDates: HashMap<Date, Date?> = hashMapOf()
    private val savedPictures: HashMap<Int, Bitmap> = hashMapOf()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        val bundle = this.arguments
        if (bundle != null) {
            searchProduct = bundle.getBoolean("searchProduct", true)
            savedValues.putAll(bundle.getSerializable("savedValues") as HashMap<String, String>)
            savedDates.putAll(bundle.getSerializable("savedDates") as HashMap<Date, Date?>)
            if (bundle.containsKey("savedPictures")) {
                savedPictures.putAll(bundle.getSerializable("savedPictures") as HashMap<Int, Bitmap>)
            }
        }
        return inflater.inflate(R.layout.fragment_add_product, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
        val retrofit =
            Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()

        val categoryChoice = view.findViewById<AutoCompleteTextView>(R.id.addProductCategoryChoice)
        val allCategories = mutableListOf<Category>()
        val api = retrofit.create(CategoriesAPI::class.java)
        val categoryNames = mutableListOf<String>()
        val categories = mutableListOf<Category>()

        val productName: TextInputEditText = view.findViewById(R.id.addProductName)
        val ratingBar: RatingBar = view.findViewById(R.id.ratingbar_addProduct)
        val productMake: TextInputEditText = view.findViewById(R.id.addProductMake)
        val productModel: TextInputEditText = view.findViewById(R.id.addProductModel)
        val productPrice: CurrencyEditText = view.findViewById(R.id.addProductPrice)
        val nonBooleanProductsRecyclerView = view.findViewById<RecyclerView>(R.id.productRecyclerViewFeaturesNonBoolean)
        val booleanProductsRecyclerView = view.findViewById<RecyclerView>(R.id.productRecyclerViewFeaturesBoolean)
        val nonBooleanFeatures = mutableListOf<Feature>()
        val booleanFeatures = mutableListOf<Feature>()
        val nonBooleanAdapter = AddProductNonBooleanAdapter()
        val booleanAdapter = AddProductBooleanAdapter()
        val addProductButton = view.findViewById<Button>(R.id.addProductButton)
        productPrice.setLocale(Locale("pl", "PL"))

        val keyListener = DigitsKeyListener.getInstance("1234567890,")
        productPrice.keyListener = keyListener

        var chosenCategory = 0L
        val unicodeMap: HashMap<String, String> = hashMapOf()

        val m: HashMap<String, String> = HashMap()
        unicodeMap.put("Motoryzacja", "\uD83D\uDE97")
        unicodeMap.put("Dom i Ogród", "\uD83C\uDFE1")
        unicodeMap.put("Elektronika", "\uD83D\uDDA5")
        unicodeMap.put("Moda", "\uD83D\uDC57")
        unicodeMap.put("Rolnictwo", "\uD83D\uDE9C")
        unicodeMap.put("Dla Dzieci", "\uD83D\uDC76")
        unicodeMap.put("Sport i Hobby", "\uD83E\uDD3A")
        unicodeMap.put("Muzyka i Edukacja", "\uD83C\uDFBC")
        unicodeMap.put("Ślub i Wesele", "\uD83D\uDC70\uD83C\uDFFD")

        val arrayAdapter =
            CustomArrayAdapter(requireContext(), R.layout.dropdown_item, categories, categoryNames, unicodeMap)
        categoryChoice.setHint(R.string.hintCategory)



        api.getAllCategories().enqueue(object : Callback<List<Category>> {
            override fun onResponse(call: Call<List<Category>>, response: Response<List<Category>>) {
                for (element in response.body()!!) {
                    if (!element.abstract) {
                        categoryNames.add(element.name)

                        if (categoryId == element.idCategory) {
                            categoryChoice.setHint(element.name)
                        }
                    }
                    categories.add(element)

                }
                arrayAdapter.notifyDataSetChanged()
                categoryChoice.setAdapter(arrayAdapter)
                return

            }

            override fun onFailure(call: Call<List<Category>>, t: Throwable) {
                return
            }
        })


        addProductButton.setOnClickListener {
            val features = mutableListOf<PostFeatureValue>()
            for (feature in booleanFeatures) {
                features.add(booleanAdapter.postFeatures[feature]!!)
            }
            for (feature in nonBooleanFeatures) {
                if (nonBooleanAdapter.postFeatures[feature] != null) {
                    features.add(nonBooleanAdapter.postFeatures[feature]!!)
                }
            }

            Log.e("Features", features.toString())
            val api = retrofit.create(ProductsAPI::class.java)
            val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
            val token = "Bearer " + sharedPreferences.getString("TOKEN", null)
            val productCreateRequestDTO = ProductCreateRequestDTO(
                ratingBar.rating.toDouble(),
                productPrice.getNumericValueBigDecimal(),
                chosenCategory,
                productMake.text.toString(),
                productModel.text.toString(),
                productName.text.toString(),
                features,
                searchProduct
            )
            api.createProduct(token, productCreateRequestDTO).enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    if (response.code() == 200) {
                        val fragment: Fragment = if (!searchProduct) {
                            AddFragment()
                        } else {
                            AddSearchFragment()
                        }
                        val bundle = Bundle()
                        bundle.putSerializable("savedValues", savedValues)
                        bundle.putSerializable("savedDates", savedDates)
                        bundle.putSerializable("savedPictures", savedPictures)
                        fragment.arguments = bundle
                        val fr = requireActivity().supportFragmentManager
                        fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                        return
                    } else {
                        Log.w(javaClass.toString(), "Error adding new product, response code: " + response.code())
                        Toast.makeText(context, "Błąd dodawania produktu", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    Toast.makeText(context, "Error while adding product", Toast.LENGTH_SHORT).show()
                    Log.e("Adding Product error", t.toString())
                }

            })
        }


        categoryChoice.setOnItemClickListener { parent, view, position, id ->
            for (category in categories) {
                if (category.name == categoryNames[position]) {
                    chosenCategory = category.idCategory
                }
            }


            Log.e("Position", position.toString())
            Log.e("ChosenCategory", chosenCategory.toString())
            val api = retrofit.create(SearchAPI::class.java)


            api.getAllFeatures(chosenCategory).enqueue(object : Callback<MutableList<Feature>> {
                override fun onResponse(call: Call<MutableList<Feature>>, response: Response<MutableList<Feature>>) {

                    nonBooleanFeatures.clear()
                    booleanFeatures.clear()
                    Log.e("ResponseBody", response.body().toString())
                    for (element in response.body()!!) {
                        if (element.type == "BOOLEAN") {
                            booleanFeatures.add(element)
                        } else {
                            nonBooleanFeatures.add(element)
                        }
                    }



                    nonBooleanAdapter.features = nonBooleanFeatures
                    booleanAdapter.features = booleanFeatures

                    val mandatoryGridLayout = GridLayoutManager(activity, 1)
                    val bonusGrisLayout = GridLayoutManager(activity, 1)
                    nonBooleanProductsRecyclerView.layoutManager = mandatoryGridLayout
                    nonBooleanProductsRecyclerView.adapter = nonBooleanAdapter
                    booleanProductsRecyclerView.layoutManager = bonusGrisLayout
                    booleanProductsRecyclerView.adapter = booleanAdapter
                }

                override fun onFailure(call: Call<MutableList<Feature>>, t: Throwable) {
                    Log.e("Products Failure", t.toString())
                }
            })

        }

    }

}