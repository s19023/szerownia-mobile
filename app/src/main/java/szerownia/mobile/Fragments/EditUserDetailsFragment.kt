package szerownia.mobile.Fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.*
import szerownia.mobile.API.UsersAPI
import szerownia.mobile.Models.MyDetails
import szerownia.mobile.Models.UserUpdateRequest

class EditUserDetailsFragment : Fragment() {
    private var serverUrl = ""
    private lateinit var email: String
    private val validation = Validation()
    private val focus = Focus()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_account_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }

        val firstName: TextInputEditText = view.findViewById(R.id.textinputedittext_accountedit_firstname)
        val lastName: TextInputEditText = view.findViewById(R.id.textinputedittext_accountedit_lastname)
        val telephoneNumber: TextInputEditText = view.findViewById(R.id.textinputedittext_accountedit_telephonenumber)
        val updateUserDetailsButton: Button = view.findViewById(R.id.button_accountedit_updateuserdetails)

        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        val token = "Bearer " + sharedPreferences.getString("TOKEN", null)

        val gson = GsonBuilder().create()
        val retrofit =
            Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()
        val api = retrofit.create(UsersAPI::class.java)

        api.getMyDetails(token).enqueue(object : Callback<MyDetails> {
            @SuppressLint("SetTextI18n") override fun onResponse(call: Call<MyDetails>, response: Response<MyDetails>) {
                val myDetails = response.body()
                if (myDetails != null) {
                    firstName.setText(myDetails.firstName)
                    lastName.setText(myDetails.lastName)
                    telephoneNumber.setText(myDetails.telephoneNumber)
                    email = myDetails.email
                }
            }

            override fun onFailure(call: Call<MyDetails>, t: Throwable) {
                Log.println(Log.ERROR, javaClass.toString(), "Downloading user details failed /n$t")
                Toast.makeText(context, "Nie można pobrać danych użytkownika", Toast.LENGTH_SHORT).show()
            }
        })

        updateUserDetailsButton.setOnClickListener {
            focus.clearFocus(requireActivity(), requireContext())
            validation.resetValidationCounter()

            validation.validateRangedField(
                firstName,
                R.string.all_emptyit,
                R.string.edituserdetails_firstname,
                R.integer.user_firstnamemaxlength,
                R.integer.user_firstnameminlength,
                requireActivity()
            )
            validation.validateRangedField(
                lastName,
                R.string.all_emptyit,
                R.string.edituserdetails_lastname,
                R.integer.user_lastnamemaxlength,
                R.integer.user_lastnameminlength,
                requireActivity()
            )
            validation.validateRangedField(
                telephoneNumber,
                R.string.all_emptymale,
                R.string.edituserdetails_telephonenumber,
                R.integer.user_telephonenumbermaxlength,
                R.integer.user_telephonenumberminlength,
                requireActivity()
            )

            val userUpdateRequest = UserUpdateRequest(
                firstName.text.toString(), lastName.text.toString(), email, telephoneNumber.text.toString()
            )

            if (validation.getValidationCounter() == 0) {
                api.updateUser(token, userUpdateRequest).enqueue(object : Callback<Void> {
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        if (response.code() == 200) {
                            Log.println(Log.ERROR, javaClass.toString(), "Updated user details")
                            Toast.makeText(context, "Zapisano dane użytkownika", Toast.LENGTH_SHORT).show()

                            val fragment: Fragment = AccountFragment()
                            val fr = requireActivity().supportFragmentManager
                            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                        } else {
                            Log.println(
                                Log.ERROR,
                                javaClass.toString(),
                                "Updating user details failed with code " + response.code()
                            )
                            Toast.makeText(context, "Błąd zapisu zmian ogłoszenia", Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        Log.println(Log.ERROR, javaClass.toString(), "Updating user details failed /n$t")
                        Toast.makeText(context, "Nie można zmienić danych użytkownika", Toast.LENGTH_SHORT).show()
                    }
                })
            }
        }

    }

}