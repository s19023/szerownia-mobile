package szerownia.mobile.Fragments

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.*
import szerownia.mobile.API.UsersAPI
import szerownia.mobile.Models.User

class RegisterFragment : Fragment() {

    private var serverUrl = ""
    private val validation = Validation()
    private val focus = Focus()
    private val saved: HashMap<String, String> = hashMapOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        val bundle = this.arguments
        if (bundle != null) {
            if (bundle.containsKey("savedValues")) {
                saved.putAll(bundle.getSerializable("savedValues") as HashMap<String, String>)
            }
        }
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val loginPrompt: TextView = view.findViewById(R.id.textview_register_login)
        val registerButton: TextView = view.findViewById(R.id.button_register)
        val registerEmail: TextInputEditText = view.findViewById(R.id.edittext_register_email)
        val registerPassword: TextInputEditText = view.findViewById(R.id.edittext_register_password)
        val registerRepeatPassword: TextInputEditText = view.findViewById(R.id.edittext_register_repeatpassword)
        val registerFirstName: TextInputEditText = view.findViewById(R.id.edittext_register_firstname)
        val registerLastName: TextInputEditText = view.findViewById(R.id.edittext_register_lastname)
        val registerPhone: TextInputEditText = view.findViewById(R.id.edittext_register_phone)
        val registerCheckBoxUserAgreement: CheckBox = view.findViewById(R.id.checkbox_register_useragreement)

        registerEmail.setText(saved["email"])
        registerFirstName.setText(saved["firstName"])
        registerLastName.setText(saved["lastName"])
        registerPhone.setText(saved["phone"])

        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                focus.clearFocus(requireActivity(), requireContext())
                widget.cancelPendingInputEvents()

                val fragment: Fragment = TermsFragment()
                val bundle = Bundle()
                val savedValues: HashMap<String, String?> = hashMapOf()
                savedValues["email"] = registerEmail.text.toString()
                savedValues["firstName"] = registerFirstName.text.toString()
                savedValues["lastName"] = registerLastName.text.toString()
                savedValues["phone"] = registerPhone.text.toString()
                bundle.putSerializable("savedValues", savedValues)
                fragment.arguments = bundle
                val fr = activity!!.supportFragmentManager
                fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
            }
        }

        registerRepeatPassword.setOnFocusChangeListener { v, b ->
            validation.validateMatchingPasswords(
                registerPassword, registerRepeatPassword, R.string.passwords_dont_match, requireActivity()
            )
        }

        registerPassword.setOnFocusChangeListener { v, b ->
            validation.validateMatchingPasswords(
                registerPassword, registerRepeatPassword, R.string.passwords_dont_match, requireActivity()
            )
        }

        val linkText = SpannableString("Regulamin Szerownia")
        linkText.setSpan(clickableSpan, 0, linkText.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        val cs = TextUtils.expandTemplate(
            "Oświadczam, że znam i akceptuję postanowienia ^1.", linkText
        )

        registerCheckBoxUserAgreement.text = cs
        registerCheckBoxUserAgreement.movementMethod = LinkMovementMethod.getInstance()


        loginPrompt.setOnClickListener {
            focus.clearFocus(requireActivity(), requireContext())

            val fragment: Fragment = LoginFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
        }

        registerButton.setOnClickListener {
            focus.clearFocus(requireActivity(), requireContext())
            validation.resetValidationCounter()

            validation.validateEmail(
                registerEmail,
                R.string.all_emptymale,
                R.string.register_wrong_fromat,
                R.string.register_taken,
                R.string.register_email,
                requireActivity(),
                serverUrl
            )

            validation.validateRangedField(
                registerPassword,
                R.string.all_emptyit,
                R.string.login_password,
                R.integer.user_passwordmaxlength,
                R.integer.user_passwordminlength,
                requireActivity()
            )

            validation.validateRangedField(
                registerFirstName,
                R.string.all_emptyit,
                R.string.edituserdetails_firstname,
                R.integer.user_firstnamemaxlength,
                R.integer.user_firstnameminlength,
                requireActivity()
            )
            validation.validateRangedField(
                registerLastName,
                R.string.all_emptyit,
                R.string.edituserdetails_lastname,
                R.integer.user_lastnamemaxlength,
                R.integer.user_lastnameminlength,
                requireActivity()
            )
            validation.validateRangedField(
                registerPhone,
                R.string.all_emptymale,
                R.string.edituserdetails_telephonenumber,
                R.integer.user_telephonenumbermaxlength,
                R.integer.user_telephonenumberminlength,
                requireActivity()
            )
            validation.validateIfEmpty(
                registerEmail, R.string.all_emptymale, R.string.register_email, requireActivity()
            )
            validation.validateIfEmpty(
                registerPassword, R.string.all_emptyit, R.string.login_password, requireActivity()
            )
            validation.validateMatchingPasswords(
                registerPassword, registerRepeatPassword, R.string.passwords_dont_match, requireActivity()
            )
            validation.validateChecked(
                registerCheckBoxUserAgreement, R.string.user_agrement_not_checked, requireActivity()
            )
            if (validation.getValidationCounter() > 0) {
                Toast.makeText(context, "Formuilarz zawiera błędy", Toast.LENGTH_SHORT).show()
            } else {

                val newUser = User(
                    registerEmail.text.toString(),
                    registerFirstName.text.toString(),
                    registerLastName.text.toString(),
                    registerPassword.text.toString(),
                    registerPhone.text.toString()
                )

                val retrofit =
                    Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create()).build()

                val api = retrofit.create(UsersAPI::class.java)
                val requestCall = api.addUser(newUser)

                val loadingDialog = LoadingDialog(view, "Rejestracja...")
                loadingDialog.showLoadingDialog()

                requestCall.enqueue(object : Callback<Void> {
                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        loadingDialog.dismissLoadingDialog()
                        Toast.makeText(context, "Błąd rejestracji", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        loadingDialog.dismissLoadingDialog()

                        if (response.isSuccessful) {
                            /*val sharedPreferences =
                                activity!!.getSharedPreferences("logged", Context.MODE_PRIVATE)
                            val editor = sharedPreferences.edit()
                            editor.apply {
                                putBoolean("LOGGED_KEY", true)
                                putString("LOGIN_EMAIL", registerEmail.text.toString())
                                putString("LOGIN_PASSWORD", registerPassword.text.toString())
                            }.apply()*/
                            Toast.makeText(context, "Rejestracja udana", Toast.LENGTH_SHORT).show()
                            val fragment: Fragment = LoginFragment()
                            val fr = activity!!.supportFragmentManager
                            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                        } else {
                            Toast.makeText(context, "Błąd rejestracji " + response.code(), Toast.LENGTH_SHORT).show()
                        }
                    }
                })
            }
        }

    }

    fun showDialog(view: View) {
        val dialogBuilder = AlertDialog.Builder(context)
        val popupView = LayoutInflater.from(context).inflate(R.layout.popup_register_user_agreement, null)
        val closeUserAgreementPopupIcon: ImageView = popupView.findViewById(R.id.imageview_popup_user_agreement_close)

        dialogBuilder.setView(popupView)
        val dialog = dialogBuilder.create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()

        closeUserAgreementPopupIcon.setOnClickListener {
            dialog.dismiss()
        }
    }
}


