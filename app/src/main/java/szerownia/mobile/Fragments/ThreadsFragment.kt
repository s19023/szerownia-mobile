package szerownia.mobile.Fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.MessagesAPI
import szerownia.mobile.Adapters.ThreadsAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.Thread
import szerownia.mobile.R
import java.util.concurrent.TimeUnit

class ThreadsFragment : Fragment() {
    private var serverUrl = ""
    private var messagesUrl = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        messagesUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_messages_url")!!
        }else{
            Helper().getConfigValue(requireContext().applicationContext, "localhost_messages_url")!!
        }

        return inflater.inflate(R.layout.fragment_threads, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val threadsRecyclerView = view.findViewById<RecyclerView>(R.id.ThreadsRecyclerView)
        val emptyMessagesTextView = view.findViewById<TextView>(R.id.textview_messages_nomessages)
        val swipeRefreshLayout = view.findViewById<SwipeRefreshLayout>(R.id.swiperefreshlayout_threads)

        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {

            val loadingDialog = LoadingDialog(view, "Ładowanie...")
            loadingDialog.showLoadingDialog()

            val okHttpClient =
                OkHttpClient().newBuilder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
            val retrofit = Retrofit.Builder().baseUrl(messagesUrl).addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient).build()
            val api = retrofit.create(MessagesAPI::class.java)

            var token = sharedPreferences.getString("TOKEN", null)

            api.getAllThreads("Bearer$token").enqueue(object : Callback<List<Thread>> {
                override fun onResponse(call: Call<List<Thread>>, response: Response<List<Thread>>) {
                    if (response.isSuccessful) {
                        if (response.body()!!.isNotEmpty()) {
                            val adapter = ThreadsAdapter(response.body()!!.reversed(),serverUrl,token, requireActivity())
                            val gridLayout = GridLayoutManager(activity, 1)
                            threadsRecyclerView.layoutManager = gridLayout
                            threadsRecyclerView.adapter = adapter
                        } else if (response.body()!!.isEmpty()) {
                            emptyMessagesTextView.visibility = View.VISIBLE
                        }
                        loadingDialog.dismissLoadingDialog()
                    }
                }

                override fun onFailure(call: Call<List<Thread>>, t: Throwable) {
                    Log.e(javaClass.toString(), "Downloading user threads failed", t)
                    loadingDialog.dismissLoadingDialog()
                    Toast.makeText(context, getString(R.string.threads_getallthreadsfailed), Toast.LENGTH_SHORT).show()
                }
            })

            swipeRefreshLayout.setOnRefreshListener {
                token = sharedPreferences.getString("TOKEN", null)
                api.getAllThreads("Bearer$token").enqueue(object : Callback<List<Thread>> {
                    override fun onResponse(call: Call<List<Thread>>, response: Response<List<Thread>>) {
                        if (response.isSuccessful) {
                            if (response.body()!!.isNotEmpty()) {
                                val adapter = ThreadsAdapter(response.body()!!.reversed(),serverUrl,token, requireActivity())
                                val gridLayout = GridLayoutManager(activity, 1)
                                threadsRecyclerView.layoutManager = gridLayout
                                threadsRecyclerView.adapter = adapter
                                swipeRefreshLayout.isRefreshing = false
                            } else if (response.body()!!.isEmpty()) {
                                emptyMessagesTextView.visibility = View.VISIBLE
                            }
                        }
                    }

                    override fun onFailure(call: Call<List<Thread>>, t: Throwable) {
                        Log.e(javaClass.toString(), "Downloading user threads failed", t)
                        swipeRefreshLayout.isRefreshing = false
                        Toast.makeText(context, getString(R.string.threads_getallthreadsfailed), Toast.LENGTH_SHORT).show()
                    }
                })
            }

        }
    }
}
