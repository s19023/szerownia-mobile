package szerownia.mobile.Fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.auth0.android.jwt.JWT
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.RentalAdAPI
import szerownia.mobile.API.SearchAdAPI
import szerownia.mobile.Adapters.MyAdsAdapter
import szerownia.mobile.Adapters.MySearchAdsAdapter
import szerownia.mobile.Adapters.MySearchAdsAdapter2
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.RentalAd
import szerownia.mobile.Models.RentalAdResult
import szerownia.mobile.Models.SearchAd
import szerownia.mobile.Models.SearchAdResult
import szerownia.mobile.R
import java.util.concurrent.TimeUnit

class MySearchAdsFragment : Fragment() {

    private var serverUrl = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        return inflater.inflate(R.layout.fragment_my_ads, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val backIcon: ImageView = view.findViewById(R.id.imageview_myads_backicon)
        val searchAds = arrayListOf<SearchAd>()
        val noAds: TextView = view.findViewById(R.id.noActiveAdsTextView)
        val rc = view.findViewById<RecyclerView>(R.id.recyclerview_myads)
        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        val token = "Bearer " + sharedPreferences.getString("TOKEN", null)
        val title :TextView = view.findViewById(R.id.textview_myads_title)
        title.text=resources.getString(R.string.MySearchAds)
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(resources.getInteger(R.integer.all_connect).toLong(), TimeUnit.SECONDS)
            .readTimeout(resources.getInteger(R.integer.all_read).toLong(), TimeUnit.SECONDS).build()

        val gson = GsonBuilder().create()
        val retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient).build()

        val api = retrofit.create(SearchAdAPI::class.java)

        backIcon.setOnClickListener {
            val nextFragment = AccountFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, nextFragment).commit()
        }
        val jwt = JWT(sharedPreferences.getString("TOKEN", "")!!)

        val userId = jwt.getClaim("id").asLong()
        val loadingDialog = LoadingDialog(view, getString(R.string.myads_downloading))
        loadingDialog.showLoadingDialog()

        api.getAllSearchAds( 25, 0).enqueue(object : Callback<SearchAdResult> {
            override fun onResponse(call: Call<SearchAdResult>, response: Response<SearchAdResult>) {
                if (response.body() != null) {
                    for (element in response.body()!!.results) {
                        if(element.userId==userId){
                            searchAds.add(element)
                        }
                    }
                    if (searchAds.isEmpty()) {
                        noAds.visibility = View.VISIBLE
                    }

                }
                val productImages = mutableListOf<Int>()
                val locImages = mutableListOf<Int>()
                for (item in searchAds) {
                    productImages.add(R.drawable.szerownia_logo_512)
                    locImages.add(R.drawable.all_location_icon)
                }

                loadingDialog.dismissLoadingDialog()

                val adapter = MySearchAdsAdapter2(searchAds,productImages,locImages)
                val gridLayout = GridLayoutManager(activity, 2)
                rc.layoutManager = gridLayout
                rc.adapter = adapter
                return
            }

            override fun onFailure(call: Call<SearchAdResult>, t: Throwable) {
                loadingDialog.dismissLoadingDialog()

                Log.println(Log.ERROR, javaClass.toString(), "Downloading user ads failed /n$t")
                Toast.makeText(context, getString(R.string.myads_downloading_error), Toast.LENGTH_SHORT).show()
            }
        })
    }
}
