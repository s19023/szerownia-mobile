package szerownia.mobile.Fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.method.DigitsKeyListener
import android.util.Log.*
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cottacush.android.currencyedittext.CurrencyEditText
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.GsonBuilder
import com.squareup.timessquare.CalendarPickerView
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.*
import szerownia.mobile.API.ImageAPI
import szerownia.mobile.API.ProductsAPI
import szerownia.mobile.API.RentalAdAPI
import szerownia.mobile.API.UsersAPI
import szerownia.mobile.Adapters.MyProductsListAdapter
import szerownia.mobile.Adapters.PhotosAdapter
import szerownia.mobile.Adapters.TimeSlotsAdapter
import szerownia.mobile.Models.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.time.LocalDate
import java.time.ZoneId
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class AddFragment : Fragment(), OnMapReadyCallback {
    private var serverUrl = ""

    val gson = GsonBuilder().create()
    lateinit var retrofit: Retrofit
    private lateinit var map: GoogleMap

    private var rentalAdId: Long = 0L
    private lateinit var rentalAdById: RentalAdById
    private val photos: MutableList<Bitmap> = mutableListOf()
    lateinit var adPhoto: RecyclerView
    val adapter = PhotosAdapter(photos)
    lateinit var imageBitmap: Bitmap
    lateinit var currentPhotoPath: String
    val imageIds: MutableList<Long> = mutableListOf()
    private var dateRanges: MutableList<TimeSlot> = mutableListOf()
    private var timeSlotAdapter = TimeSlotsAdapter(dateRanges)
    private val validation = Validation()
    private val focus = Focus()
    private val saved: HashMap<String, String> = hashMapOf()
    private val savedD: HashMap<Date, Date?> = hashMapOf()
    private val savedP: HashMap<Int, Bitmap> = hashMapOf()

    private var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            imageBitmap = it.data!!.extras!!.get("data") as Bitmap
            photos.add(imageBitmap)
        }
        adPhoto.visibility = VISIBLE
        adapter.notifyDataSetChanged()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val bundle = this.arguments
        if (bundle != null) {
            rentalAdId = bundle.getLong("rentalAdId")
            if (bundle.containsKey("savedValues")) {
                saved.putAll(bundle.getSerializable("savedValues") as HashMap<String, String>)
                savedD.putAll(bundle.getSerializable("savedDates") as HashMap<Date, Date?>)
                savedP.putAll(bundle.getSerializable("savedPictures") as HashMap<Int, Bitmap>)
            }
        }

        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }

        retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()

        return inflater.inflate(R.layout.fragment_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        adPhoto = requireView().findViewById(R.id.recyclerview_add_photos)

        val addTitle: TextView = view.findViewById(R.id.textview_add_title)
        val photosTitle: TextView = view.findViewById(R.id.textview_add_photostitle)

        val title: TextInputEditText = view.findViewById(R.id.textinputedittext_add_title)
        val description: TextInputEditText = view.findViewById(R.id.descriptionInput)
        val pricePerDay: CurrencyEditText = view.findViewById(R.id.pricePerDayInput)
        val deposit: CurrencyEditText = view.findViewById(R.id.depositInput)
        val costOfDelay: CurrencyEditText = view.findViewById(R.id.costOfDelayInput)
        val location: TextInputEditText = view.findViewById(R.id.locationInput)
        val averageRentalPriceRequirement: CurrencyEditText = view.findViewById(R.id.addAdvertAdPriceRequirementInput)
        val minRentalHistory: TextInputEditText = view.findViewById(R.id.addAdvertHistoryRequirementInput)
        val customRequirement: TextInputEditText = view.findViewById(R.id.addAdvertCustomRequirementInput)

        pricePerDay.setLocale(Locale("pl", "PL"))
        deposit.setLocale(Locale("pl", "PL"))
        costOfDelay.setLocale(Locale("pl", "PL"))
        averageRentalPriceRequirement.setLocale(Locale("pl", "PL"))

        val keyListener = DigitsKeyListener.getInstance("1234567890,")
        pricePerDay.keyListener = keyListener
        deposit.keyListener = keyListener
        costOfDelay.keyListener = keyListener
        averageRentalPriceRequirement.keyListener = keyListener

        validation.setMaxLength(title, R.integer.add_maxtitlelength, requireActivity())
        validation.setMaxLength(description, R.integer.add_maxdescriptionlength, requireActivity())
        validation.setMaxLength(pricePerDay, R.integer.add_maxpriceperdaylength, requireActivity())
        validation.setMaxLength(deposit, R.integer.add_maxdepositlength, requireActivity())
        validation.setMaxLength(costOfDelay, R.integer.add_maxcostofdelaylength, requireActivity())
        validation.setMaxLength(location, R.integer.add_maxlocationlength, requireActivity())
        validation.setMaxLength(
            averageRentalPriceRequirement, R.integer.add_maxaveragerentalpricelength, requireActivity()
        )
        validation.setMaxLength(minRentalHistory, R.integer.add_maxminrentalhistorylength, requireActivity())
        validation.setMaxLength(customRequirement, R.integer.add_maxcustomrequirementlength, requireActivity())

        val searchLocation: ImageView = view.findViewById(R.id.imageview_add_searchlocation)
        val addDateRangeButton: ImageView = view.findViewById(R.id.AddDateRange)
        val dateRangesRecyclerView: RecyclerView = view.findViewById(R.id.dateRangesRecyclerView)
        val noProducts: TextView = view.findViewById(R.id.textviewe_add_noproducts)
        val addProduct: ImageView = view.findViewById(R.id.imageviewe_add_addproduct)
        val promotionAsk: CheckBox = view.findViewById(R.id.promotedAsk)
        val dateRangeTitle: TextView = view.findViewById(R.id.datePickerDescription)
        val productsTitle: TextView = view.findViewById(R.id.textview_add_productstitle)
        val pickupTitle: TextView = view.findViewById(R.id.textview_add_pickupmethodtitle)
        val locationTitle: TextView = view.findViewById(R.id.textview_add_locationtitle)

        val productsTitleConstraintLayout: ConstraintLayout = view.findViewById(R.id.constraintlayout_add_productstitle)
        val productsListConstraintLayout: ConstraintLayout = view.findViewById(R.id.constraintlayout_add_productslist)

        val averageOpinionRequirementRatingBar: RatingBar =
            view.findViewById(R.id.ratingbar_addfragment_averageopinionrequirement)

        val myProducts: MutableList<MyProduct> = mutableListOf()
        val addImageButton: ImageButton = view.findViewById(R.id.imagebutton_add_addphoto)

        val productRecyclerView = view.findViewById<RecyclerView>(R.id.recyclerview_add_products)
        val pickupMethod1: CheckBox = view.findViewById(R.id.pickupMethod1)
        val pickupMethod2: CheckBox = view.findViewById(R.id.pickupMethod2)
        val pickupMethod3: CheckBox = view.findViewById(R.id.pickupMethod3)

        val bottomNavigationView = requireActivity().findViewById<BottomNavigationView>(R.id.bottom_navigation)
        val mapView: MapView = view.findViewById(R.id.mapview_add_chosenlocation)
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)
        val zoomValue = 10f

        val layout = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        val layout2 = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        adPhoto.adapter = adapter
        adPhoto.layoutManager = layout

        dateRangesRecyclerView.adapter = timeSlotAdapter
        dateRangesRecyclerView.layoutManager = layout2

        val token = "Bearer " + sharedPreferences.getString("TOKEN", null)
        val addButton: Button = view.findViewById(R.id.button_add_addadvert)

        val api2 = retrofit.create(UsersAPI::class.java)
        api2.getMyDetails(token).enqueue(object : Callback<MyDetails> {
            @SuppressLint("SetTextI18n") override fun onResponse(call: Call<MyDetails>, response: Response<MyDetails>) {
                val myDetails = response.body()
                if (myDetails != null) {
                    if (myDetails.premium) {
                        if (rentalAdId == 0L) promotionAsk.visibility = VISIBLE
                    }
                }
                return
            }

            override fun onFailure(call: Call<MyDetails>, t: Throwable) {
                e("Loading premium Status", t.toString())
                return
            }

        })
        val geocoder = Geocoder(context)
        var address: List<Address>
        var p1: LatLng? = null

        title.setText(saved["title"])
        description.setText(saved["description"])
        pricePerDay.setText(saved["pricePerDay"])
        deposit.setText(saved["deposit"])
        costOfDelay.setText(saved["costOfDelay"])
        location.setText(saved["location"])
        averageRentalPriceRequirement.setText(saved["averageRentalPriceRequirement"])
        minRentalHistory.setText(saved["minRentalHistory"])
        customRequirement.setText(saved["customRequirement"])
        pickupMethod1.isChecked = saved["pickup1"].toBoolean()
        pickupMethod2.isChecked = saved["pickup2"].toBoolean()
        pickupMethod3.isChecked = saved["pickup3"].toBoolean()
        if (saved.containsKey("averageOpinionRequirementRatingBar")) {
            averageOpinionRequirementRatingBar.rating = saved["averageOpinionRequirementRatingBar"]!!.toFloat()
        }
        for (key in savedD.keys) {
            dateRanges.add(TimeSlot(savedD[key], key))
        }
        photos.addAll(savedP.values)
        adapter.notifyDataSetChanged()
        if (photos.size > 0) {
            adPhoto.visibility = VISIBLE
        }


        if (rentalAdId != 0L) {
            println(INFO, javaClass.toString(), "Downloading rentalAd details")
            val rentalAdApi = retrofit.create(RentalAdAPI::class.java)
            rentalAdApi.getAd(rentalAdId).enqueue(object : Callback<RentalAdById> {
                override fun onResponse(call: Call<RentalAdById>, response: Response<RentalAdById>) {
                    println(INFO, javaClass.toString(), "Received rentalAd details")

                    if (response.body() != null) {
                        rentalAdById = response.body()!!

                        addTitle.text = resources.getString(R.string.add_edittitle)
                        photosTitle.visibility = GONE
                        addImageButton.visibility = GONE
                        adPhoto.visibility = GONE
                        productsTitleConstraintLayout.visibility = GONE
                        productsListConstraintLayout.visibility = GONE

                        title.setText(rentalAdById.title)
                        description.setText(rentalAdById.description)
                        pricePerDay.setText(rentalAdById.pricePerDay.toString())
                        deposit.setText(rentalAdById.depositAmount.toString())
                        costOfDelay.setText(rentalAdById.penaltyForEachDayOfDelayInReturns.toString())
                        location.setText(rentalAdById.location.name)
                        try {
                            val latLng = LatLng(rentalAdById.location.latitude, rentalAdById.location.longitude)
                            map.apply {
                                addMarker(MarkerOptions().position(latLng))
                                moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomValue))
                            }
                        } catch (ex: IOException) {
                            println(WARN, javaClass.toString(), "Address not found /n$ex")
                            Toast.makeText(context, "Nie znaleziono adresu", Toast.LENGTH_SHORT).show()
                        }

                        for (pickupMethod in rentalAdById.pickupMethodList) {
                            if (pickupMethod.name == PickupMethod.PERSONAL_PICKUP.toString()) pickupMethod1.isChecked =
                                true
                            if (pickupMethod.name == PickupMethod.PARCEL_LOCKER.toString()) pickupMethod2.isChecked =
                                true
                            if (pickupMethod.name == PickupMethod.COURIER.toString()) pickupMethod3.isChecked = true
                        }

                        averageOpinionRequirementRatingBar.rating = rentalAdById.averageOpinion.toFloat()
                        averageRentalPriceRequirement.setText(rentalAdById.averageRentalPrice.toString())
                        minRentalHistory.setText(rentalAdById.minRentalHistory.toString())
                        if (rentalAdById.customRequirement != null) customRequirement.setText(rentalAdById.customRequirement)

                        for (i in rentalAdById.timeSlotList) {
                            dateRanges.add(i)
                        }
                        timeSlotAdapter = TimeSlotsAdapter(dateRanges)
                        dateRangesRecyclerView.adapter = timeSlotAdapter
                        dateRangesRecyclerView.layoutManager = layout2

                        addButton.text = getString(R.string.add_editbutton)
                        return
                    }
                }

                override fun onFailure(call: Call<RentalAdById>, t: Throwable) {
                    println(WARN, javaClass.toString(), "Receiving rentalAd details failed")
                    Toast.makeText(context, "Nie można pobrać szczegółów ogłoszenia", Toast.LENGTH_SHORT).show()
                    return
                }
            })
        }

        addDateRangeButton.setOnClickListener {
            focus.clearFocus(requireActivity(), requireContext())

            val dateFrom = Calendar.getInstance()
            val dateTo = Calendar.getInstance()
            dateTo.add(Calendar.MONTH, 3)
            if (dateRanges.size > 0) {
                dateFrom.time = dateRanges[dateRanges.size - 1].endDate
                dateFrom.add(Calendar.DAY_OF_YEAR, 1)
                dateTo.time = dateFrom.time
                dateTo.add(Calendar.MONTH, 3)

            }

            val dialogBuilder = AlertDialog.Builder(context)
            val addDateView = LayoutInflater.from(context).inflate(R.layout.popup_date_range_picker, null)
            val closeButton: ImageView = addDateView.findViewById(R.id.dateRangeCloseButton)
            val calendarPicker: CalendarPickerView = addDateView.findViewById(R.id.addDateRangeCalendarPicker)
            val confirmButton: Button = addDateView.findViewById(R.id.confirmDateRange)
            calendarPicker.init(dateFrom.time, dateTo.time).inMode(CalendarPickerView.SelectionMode.RANGE)

            dialogBuilder.setView(addDateView)
            val dialog = dialogBuilder.create()
            dialog.show()

            closeButton.setOnClickListener {
                dialog.dismiss()
            }

            confirmButton.setOnClickListener {
                dateRanges.add(
                    TimeSlot(
                        calendarPicker.selectedDates[calendarPicker.selectedDates.size - 1],
                        calendarPicker.selectedDates[0]
                    )
                )
                dateRangeTitle.error = null
                dialog.dismiss()
                timeSlotAdapter.notifyDataSetChanged()
            }

            calendarPicker.setOnDateSelectedListener(object : CalendarPickerView.OnDateSelectedListener {
                override fun onDateSelected(date: Date?) {
                    if (calendarPicker.selectedDates.size > 1) {
                        confirmButton.visibility = VISIBLE
                    }
                }

                override fun onDateUnselected(date: Date?) {
                    confirmButton.visibility = GONE
                }
            })

        }
        addProduct.setOnClickListener {
            focus.clearFocus(requireActivity(), requireContext())

            val addProductFragment = AddProductFragment()
            val bundle = Bundle()
            bundle.putBoolean("searchProduct", false)
            val savedValues: HashMap<String, String?> = hashMapOf()
            savedValues["title"] = title.text.toString()
            savedValues["description"] = description.text.toString()
            savedValues["pricePerDay"] = pricePerDay.text.toString()
            savedValues["deposit"] = deposit.text.toString()
            savedValues["costOfDelay"] = costOfDelay.text.toString()
            savedValues["location"] = location.text.toString()
            savedValues["averageRentalPriceRequirement"] = averageRentalPriceRequirement.text.toString()
            savedValues["minRentalHistory"] = minRentalHistory.text.toString()
            savedValues["customRequirement"] = customRequirement.text.toString()
            savedValues["averageOpinionRequirementRatingBar"] = averageOpinionRequirementRatingBar.rating.toString()
            savedValues["pickup1"] = pickupMethod1.isChecked.toString()
            savedValues["pickup2"] = pickupMethod2.isChecked.toString()
            savedValues["pickup3"] = pickupMethod3.isChecked.toString()

            val savedDates: HashMap<Date, Date?> = hashMapOf()
            for (timeslot in dateRanges) {
                savedDates[timeslot.startDate] = timeslot.endDate
            }
            val savedPictures: HashMap<Int, Bitmap> = hashMapOf()
            for (i in 0 until photos.size) {
                savedPictures[i] = photos[i]
            }

            bundle.putSerializable("savedPictures", savedPictures)
            bundle.putSerializable("savedDates", savedDates)
            bundle.putSerializable("savedValues", savedValues)
            addProductFragment.arguments = bundle
            if (it != null) {
                (it.context as FragmentActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, addProductFragment).commit()
            }
        }

        addImageButton.setOnClickListener {
            focus.clearFocus(requireActivity(), requireContext())

            if (ContextCompat.checkSelfPermission(
                    requireContext(), Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_DENIED
            ) {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA), 0)

            } else {
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultLauncher.launch(takePictureIntent)
            }
        }

        val api = retrofit.create(ProductsAPI::class.java)
        var productsListAdapter = MyProductsListAdapter(emptyList())
        var myProductsGridLayoutManager: GridLayoutManager

        api.getMyProducts(token, false).enqueue(object : Callback<List<MyProduct>> {
            override fun onResponse(call: Call<List<MyProduct>>, response: Response<List<MyProduct>>) {
                if (response.isSuccessful) {
                    myProducts.addAll(response.body()!!)
//                     myProducts response.body()!!
                    productsListAdapter = MyProductsListAdapter(myProducts)
                    myProductsGridLayoutManager = GridLayoutManager(activity, 1)
                    productRecyclerView.layoutManager = myProductsGridLayoutManager
                    productRecyclerView.adapter = productsListAdapter
                }
                if (myProducts.isNotEmpty()) {
                    noProducts.visibility = GONE
                }
                return
            }

            override fun onFailure(call: Call<List<MyProduct>>, t: Throwable) {
                e("Add Rental Ad Error", t.message.toString())
            }
        })



        searchLocation.setOnClickListener {
            focus.clearFocus(requireActivity(), requireContext())

            if (location.text.toString().isEmpty()) {
                Toast.makeText(context, "Wpisz adres", Toast.LENGTH_SHORT).show()
            } else {
                try {
                    address = geocoder.getFromLocationName(location.text.toString(), 5)
                    if (address != null) {
                        val locationResult = address[0]
                        p1 = LatLng(locationResult.latitude, locationResult.longitude)
                        println(
                            INFO, javaClass.toString(), "Address found. Lat: ${p1!!.latitude}, Lon: ${p1!!.longitude}"
                        )

                        map.apply {
                            addMarker(MarkerOptions().position(p1!!))
                            moveCamera(CameraUpdateFactory.newLatLngZoom(p1!!, zoomValue))
                        }
                    } else {
                        Toast.makeText(context, "Nie znaleziono adresu", Toast.LENGTH_SHORT).show()
                    }
                } catch (ex: IOException) {
                    println(WARN, javaClass.toString(), "Address not found /n$ex")
                    Toast.makeText(context, "Nie znaleziono adresu", Toast.LENGTH_SHORT).show()
                }
            }
        }

        addButton.setOnClickListener {
            validation.resetValidationCounter()
            focus.clearFocus(requireActivity(), requireContext())

            val loadingDialog: LoadingDialog
            if (rentalAdId != 0L) {
                loadingDialog = LoadingDialog(view, "Zapisywanie ogłoszenia...")
                loadingDialog.showLoadingDialog()
            } else {
                loadingDialog = LoadingDialog(view, "Tworzenie ogłoszenia...")
                loadingDialog.showLoadingDialog()
            }

            val pickupMethodList = ArrayList<PickupMethod>()
            if (pickupMethod1.isChecked) pickupMethodList.add(PickupMethod.PERSONAL_PICKUP)
            if (pickupMethod2.isChecked) pickupMethodList.add(PickupMethod.PARCEL_LOCKER)
            if (pickupMethod3.isChecked) pickupMethodList.add(PickupMethod.COURIER)

            val okHttpClient =
                OkHttpClient().newBuilder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS)
                    .build()

            val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
            val retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient).build()
            val rentalAdAPI = retrofit.create(RentalAdAPI::class.java)
            val storageDir: File = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
            val file = File.createTempFile(
                "JPEG_${Calendar.getInstance().time.toInstant().epochSecond}_", ".jpg", storageDir
            ).apply {
                currentPhotoPath = absolutePath
            }
            var imageList = MultipartBody.Part.createFormData(
                "imageList", file.name, file.asRequestBody("image/*".toMediaType())
            )
            for (photo in photos) {
                val storageDir: File = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
                val file = File.createTempFile(
                    "JPEG_${Calendar.getInstance().time.toInstant().epochSecond}_", ".jpg", storageDir
                ).apply {
                    currentPhotoPath = absolutePath
                }


                try {
                    // Get the file output stream
                    val stream: OutputStream = FileOutputStream(file)

                    // Compress bitmap
                    photo.compress(Bitmap.CompressFormat.JPEG, 100, stream)

                    // Flush the stream
                    stream.flush()

                    // Close stream
                    stream.close()
                } catch (e: IOException) { // Catch the exception
                    e.printStackTrace()
                }

                imageList = MultipartBody.Part.createFormData(
                    "imageList", file.name, file.asRequestBody("image/*".toMediaType())
                )
            }
            validation.validateRangedField(
                title,
                R.string.all_emptymale,
                R.string.add_title,
                R.integer.add_maxtitlelength,
                R.integer.add_mintitlelength,
                requireActivity()
            )

            validation.validateOnlyLength(
                description, R.string.add_description, R.integer.add_maxdescriptionlength, requireActivity()
            )

            validation.validateNumbers(
                pricePerDay,
                R.string.all_emptyfemale,
                R.string.add_priceperday,
                R.integer.add_maxpriceperdaylength,
                R.integer.add_minpriceperdayvalue,
                requireActivity()
            )
            validation.validateNumbers(
                deposit,
                R.string.all_emptyfemale,
                R.string.add_deposit,
                R.integer.add_maxdepositlength,
                R.integer.add_mindepositvalue,
                requireActivity()
            )

            validation.validateNumbers(
                costOfDelay,
                R.string.all_emptyfemale,
                R.string.add_costofdelay,
                R.integer.add_maxcostofdelaylength,
                R.integer.add_mincostofdelayvalue,
                requireActivity()
            )

            validation.validateRangedField(
                location,
                R.string.all_emptyfemale,
                R.string.add_location,
                R.integer.add_maxlocationlength,
                R.integer.add_minlocationlength,
                requireActivity()
            )

//                validation.validateNumbers(
//                    averageRentalPriceRequirement,
//                    R.string.all_emptyfemale,
//                    R.string.add_averagerentalprice,
//                    R.integer.add_maxaveragerentalpricelength,
//                    R.integer.add_minaveragerentalpricevalue,
//                    requireActivity()
//                )

//                validation.validateNumbers(
//                    minRentalHistory,
//                    R.string.all_emptyfemale,
//                    R.string.add_minrentalhistory,
//                    R.integer.add_maxminrentalhistorylength,
//                    R.integer.add_minminrentalhistoryvalue,
//                    requireActivity()
//                )

            validation.validateOnlyLength(
                customRequirement,
                R.string.add_customrequirement,
                R.integer.add_maxcustomrequirementlength,
                requireActivity()
            )

            if (rentalAdId == 0L) {
                validation.setTextViewErrorStatus(
                    photosTitle, R.string.add_nophotos, adapter.getPhotosList().toMutableList(), requireActivity()
                )

                validation.setTextViewErrorStatus(
                    productsTitle,
                    R.string.add_noproducts,
                    productsListAdapter.checkedProducts.toMutableList(),
                    requireActivity()
                )
            }

            validation.setTextViewErrorStatus(
                pickupTitle, R.string.add_nopickup, pickupMethodList.toMutableList(), requireActivity()
            )

            validation.setTextViewErrorStatus(
                dateRangeTitle, R.string.add_emptydateranges, dateRanges.toMutableList(), requireActivity()
            )

            if (!location.text.isNullOrEmpty()) {
                try {
                    address = geocoder.getFromLocationName(location.text.toString(), 5)
                    if (!address.isNullOrEmpty()) {
                        val locationResult = address[0]
                        p1 = LatLng(locationResult.latitude, locationResult.longitude)
                        println(
                            INFO, javaClass.toString(), "Address found. Lat: ${p1!!.latitude}, Lon: ${p1!!.longitude}"
                        )
                    } else {
                        validation.addErrorToCounter()
                        validation.setTextViewErrorStatus(
                            locationTitle, R.string.add_addressnotfound, address.toMutableList(), requireActivity()
                        )

                    }
                } catch (ex: IOException) {
                    println(
                        ERROR, javaClass.toString(), "Address not found, geocoder not working /n$ex"
                    )
                    validation.addErrorToCounter()
                    validation.setTextViewErrorStatus(
                        locationTitle, R.string.add_geocoderfailed, emptyList<Any>().toMutableList(), requireActivity()
                    )
                }
            }

            if (validation.getValidationCounter() > 0) {
                loadingDialog.dismissLoadingDialog()
                Toast.makeText(
                    context, "Formularz zawiera błędy", Toast.LENGTH_SHORT
                ).show()
            } else {
                validation.resetValidationCounter()

                if (rentalAdId != 0L) {

                    var averageRentalPriceValue = 0.0
                    if (averageRentalPriceRequirement.text.isNullOrEmpty())
                        averageRentalPriceValue = averageRentalPriceRequirement.text.toString().toDouble()

                    var minRentalHistoryValue = 0
                    if (minRentalHistory.text.isNullOrEmpty())
                        minRentalHistoryValue = minRentalHistory.text.toString().toInt()

                    val updatedRentalAd = RentalAdUpdate(
                        title.text.toString(),
                        dateRanges,
                        LocationForRentalAdRequest(
                            p1!!.latitude, p1!!.longitude, location.text.toString()
                        ),
                        pickupMethodList,
                        description.text.toString(),
                        pricePerDay.getNumericValueBigDecimal(),
                        deposit.getNumericValueBigDecimal(),
                        costOfDelay.getNumericValueBigDecimal(),
                        averageOpinionRequirementRatingBar.rating.toDouble(),
                        averageRentalPriceValue,
                        minRentalHistoryValue,
                        customRequirement.text.toString()
                    )

                    rentalAdAPI.updateRentalAd(rentalAdId, token, updatedRentalAd).enqueue(object : Callback<Void> {
                            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                loadingDialog.dismissLoadingDialog()

                                if (response.code() == 200) {
                                    println(INFO, javaClass.toString(), "Updated ad")
                                    Toast.makeText(
                                        context, "Zapisano zmiany ogłoszenia", Toast.LENGTH_SHORT
                                    ).show()

                                    val fragment: Fragment = MyAdsFragment()
                                    val fr = activity!!.supportFragmentManager
//                                    bottomNavigationView.selectedItemId = R.id.menu_home
                                    fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                                } else {
                                    println(
                                        WARN,
                                        javaClass.toString(),
                                        "Error while updating ad with response code: ${response.code()}"
                                    )
                                    Toast.makeText(
                                        context,
                                        "Nie można zapisać zmian ogłoszenia, błąd ${response.code()}",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }

                            override fun onFailure(call: Call<Void>, t: Throwable) {
                                loadingDialog.dismissLoadingDialog()

                                e(javaClass.toString(), "Updating ad failed", t)
                                Toast.makeText(
                                    context, "Nie można zapisać zmian ogłoszenia", Toast.LENGTH_SHORT
                                ).show()
                            }

                        })
                } else {

                    val api = retrofit.create(ImageAPI::class.java)
                    api.uploadImage(token, imageList).enqueue(object : Callback<ImageData> {
                        override fun onResponse(call: Call<ImageData>, response: Response<ImageData>) {
                            if (response.isSuccessful) {
                                imageIds.add(response.body()!!.get(0).imageId)

                                val checkedProducts: ArrayList<Int> = productsListAdapter.checkedProducts
                                println(
                                    INFO, javaClass.toString(), "Number of checked products in ad: $checkedProducts"
                                )

                                var averageRentalPriceValue = 0.0
                                if (!averageRentalPriceRequirement.text.isNullOrEmpty())
                                    averageRentalPriceValue = averageRentalPriceRequirement.text.toString().toDouble()

                                var minRentalHistoryValue = 0
                                if (!minRentalHistory.text.isNullOrEmpty())
                                    minRentalHistoryValue = minRentalHistory.text.toString().toInt()

                                var promoted = false
                                if (promotionAsk.isChecked) {
                                    promoted = true
                                }
                                val newRentalAd = RentalAdCreate(
                                    title.text.toString(),
                                    dateRanges,
                                    LocationForRentalAdRequest(
                                        p1!!.latitude, p1!!.longitude, location.text.toString()
                                    ),
                                    pickupMethodList,
                                    description.text.toString(),
                                    pricePerDay.getNumericValueBigDecimal(),
                                    deposit.getNumericValueBigDecimal(),
                                    costOfDelay.getNumericValueBigDecimal(),
                                    averageOpinionRequirementRatingBar.rating.toDouble(),
                                    averageRentalPriceValue,
                                    minRentalHistoryValue,
                                    customRequirement.text.toString(),
                                    imageIds,
                                    promoted
                                )
                                e("newRentalAd", newRentalAd.toString())
                                rentalAdAPI.createNewRentalAd(checkedProducts[0].toString(), token, newRentalAd)
                                    .enqueue(object : Callback<Long> {
                                        override fun onResponse(call: Call<Long>, response: Response<Long>) {
                                            loadingDialog.dismissLoadingDialog()

                                            if (response.code() == 200) {
                                                println(
                                                    INFO, javaClass.toString(), "Created new ad with id: ${response.body()}"
                                                )
                                                Toast.makeText(
                                                    context, "Dodano nowe ogłoszenie", Toast.LENGTH_SHORT
                                                ).show()

                                                val fragment: Fragment = HomeFragment()
                                                val fr = activity!!.supportFragmentManager
                                                bottomNavigationView.selectedItemId = R.id.menu_home
                                                fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                                            } else {
                                                println(
                                                    WARN,
                                                    javaClass.toString(),
                                                    "Error while creating new ad with response code: ${response.code()}"
                                                )
                                                Toast.makeText(
                                                    context,
                                                    "Nie można dodać ogłoszenia, błąd ${response.code()}",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }
                                        }

                                        override fun onFailure(call: Call<Long>, t: Throwable) {
                                            loadingDialog.dismissLoadingDialog()

                                            println(ERROR, javaClass.toString(), "Adding ad failed /n$t")
                                            Toast.makeText(
                                                context, "Nie można dodać ogłoszenia", Toast.LENGTH_SHORT
                                            ).show()
                                        }
                                    })
                            }
                        }

                        override fun onFailure(call: Call<ImageData>, t: Throwable) {
                            e("Error Image", t.message.toString())
                        }
                    })
                }

            }
        }
    }

    override fun onMapReady(map: GoogleMap) {
        this.map = map!!

        map.apply {
            val polska = LatLng(52.14, 19.7)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(polska, 5f))
        }
    }
}
