package szerownia.mobile.Fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Log.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.GsonBuilder
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.*
import szerownia.mobile.API.MessagesAPI
import szerownia.mobile.API.RentalAdAPI
import szerownia.mobile.API.ReportAPI
import szerownia.mobile.API.UsersAPI
import szerownia.mobile.Adapters.DiscountAdapter
import szerownia.mobile.Adapters.PickupMethodListAdapter
import szerownia.mobile.Adapters.ProductListAdapter
import szerownia.mobile.Models.*
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

class RentalAdDetailsFragment : Fragment(), OnMapReadyCallback {

    private var serverUrl = ""
    private var messagesUrl = ""
    private var userId = ""
    private var rentalAdId: Long = 0
    private lateinit var rentalAdById: RentalAdById
    private lateinit var images: List<Int>
    private lateinit var locImages: List<Int>   //czy to potrzebne?
    private var latitude = 52.14
    private var longitude = 19.7
    private var zoomValue = 5f
    private lateinit var map: GoogleMap
    private val focus = Focus()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        messagesUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_messages_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_messages_url")!!
        }

        val bundle = this.arguments
        if (bundle != null) {
            rentalAdId = bundle.getLong("id")
        }
        return inflater.inflate(R.layout.fragment_rentalad_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val loadingDialog = LoadingDialog(view, "Ładowanie...")
        loadingDialog.showLoadingDialog()

        val mapView: MapView = view.findViewById(R.id.mapview_rentalad)
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)

        val title: TextView = view.findViewById(R.id.rentalAdTitle)
        val location: TextView = view.findViewById(R.id.location)
        val pricePerDay: TextView = view.findViewById(R.id.pricePerDay)
        val endDate: TextView = view.findViewById(R.id.adEndDate)
        val publicationDate: TextView = view.findViewById(R.id.adStartDate)
        val description: TextView = view.findViewById(R.id.adDescription)
        val adImage: ImageView = view.findViewById(R.id.adImage)
        val userName: TextView = view.findViewById(R.id.userName)
        val sendButton: Button = view.findViewById(R.id.sendMessageButton)
        val pricePerDayDetails: TextView = view.findViewById(R.id.pricePerDayDetails)
        val depositAmount: TextView = view.findViewById(R.id.depositAmount)
        val penaltyForEachDayOfDelayInReturns: TextView = view.findViewById(R.id.penaltyForEachDayOfDelayInReturns)
        val callButton: Button = view.findViewById(R.id.callButton)
        val ratingBar: RatingBar = view.findViewById(R.id.ratingbar_rentalad_details)
        val bottomNavigationView = requireActivity().findViewById<BottomNavigationView>(R.id.bottom_navigation)
        val rentButton: Button = view.findViewById(R.id.rentButton)
        val floatingActionButtonPromote: FloatingActionButton = view.findViewById(R.id.promoteButton)
        val backButton: ImageView = view.findViewById(R.id.imageview_addetails_backbutton)
        val discountRecyclerView: RecyclerView = view.findViewById(R.id.DiscountRecyclerView)
        val constraintLayoutDiscount: ConstraintLayout = view.findViewById(R.id.constraintLayoutRabat)

        val floatingActionButtonDelete: FloatingActionButton =
            view.findViewById(R.id.floatingactionbutton_addetails_delete)
        val floatingActionButtonEdit: FloatingActionButton = view.findViewById(R.id.floatingactionbutton_addetails_edit)
        val manageTitleConstraintLayout: ConstraintLayout =
            view.findViewById(R.id.constraintlayout_addetails_managertitle)

        val contactConstraintLayout: ConstraintLayout = view.findViewById(R.id.constraintlayout_addetails_contact)
        val reportConstraintLayout: ConstraintLayout = view.findViewById(R.id.constraintlayout_addetails_report)

        /*val nextImage: ImageButton = view.findViewById(R.id.nextImage)
        val previousImage: ImageButton = view.findViewById(R.id.previousImage)*/
        val okHttpClient =
            OkHttpClient().newBuilder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()
        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
        val retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient).build()

        val api = retrofit.create(RentalAdAPI::class.java)
        val api2 = retrofit.create(UsersAPI::class.java)

        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        val token = "Bearer " + sharedPreferences.getString("TOKEN", null)

        val productRecyclerView = view.findViewById<RecyclerView>(R.id.productsListView)
        val pickupRecyclerView = view.findViewById<RecyclerView>(R.id.pickupMethodList)

        api.getAd(rentalAdId).enqueue(object : Callback<RentalAdById> {
            override fun onResponse(call: Call<RentalAdById>, response: Response<RentalAdById>) {
                val locImages = mutableListOf<Int>()
                locImages.add(R.drawable.all_location_icon)
                rentalAdById = response.body()!!

                if (!sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                    callButton.visibility = View.GONE
                    reportConstraintLayout.visibility = View.GONE
                }
                if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                    userId = sharedPreferences.getString("USER_ID", "0").toString()
                    if (userId != "0" && userId == rentalAdById.user.id.toString()) {
                        api2.getMyDetails(token).enqueue(object : Callback<MyDetails> {
                            @SuppressLint("SetTextI18n") override fun onResponse(
                                call: Call<MyDetails>, response: Response<MyDetails>
                            ) {
                                val myDetails = response.body()
//                                if (myDetails != null) {
//                                    if (myDetails.premium && !rentalAdById.promoted) {
//                                        floatingActionButtonPromote.visibility = View.VISIBLE
//                                    }
//                                }
                                return
                            }

                            override fun onFailure(call: Call<MyDetails>, t: Throwable) {
                                Log.e("Loading premium Status", t.toString())
                                return
                            }

                        })


                        floatingActionButtonDelete.visibility = View.VISIBLE
                        floatingActionButtonEdit.visibility = View.VISIBLE
                        manageTitleConstraintLayout.visibility = View.VISIBLE

                        contactConstraintLayout.visibility = View.GONE
                        reportConstraintLayout.visibility = View.GONE
                        rentButton.visibility = View.GONE
                        floatingActionButtonPromote.setOnClickListener {
                            val dialogBuilder = AlertDialog.Builder(context)
                            val popupView = LayoutInflater.from(context).inflate(R.layout.popup_addetails_delete, null)

                            val text: TextView = popupView.findViewById(R.id.textView18)
                            text.text = getString(R.string.wantToPromote)
                            val promoteButton: Button = popupView.findViewById(R.id.button_popupdeletead_yes)
                            promoteButton.setBackgroundColor(Color.BLUE)
                            val cancelButton: Button = popupView.findViewById(R.id.button_popupdeletead_no)
                            cancelButton.setBackgroundColor(Color.RED)

                            dialogBuilder.setView(popupView)
                            val dialog = dialogBuilder.create()
                            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                            dialog.show()

                            promoteButton.setOnClickListener {
                                dialog.dismiss()
                            }

                            cancelButton.setOnClickListener {
                                dialog.dismiss()
                            }
                        }

                        floatingActionButtonDelete.setOnClickListener {
                            val dialogBuilder = AlertDialog.Builder(context)
                            val popupView = LayoutInflater.from(context).inflate(R.layout.popup_addetails_delete, null)

                            val deleteButton: Button = popupView.findViewById(R.id.button_popupdeletead_yes)
                            val cancelButton: Button = popupView.findViewById(R.id.button_popupdeletead_no)

                            dialogBuilder.setView(popupView)
                            val dialog = dialogBuilder.create()
                            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                            dialog.show()

                            deleteButton.setOnClickListener {
                                println(INFO, javaClass.toString(), "Deleting ad " + rentalAdById.id)

                                api.deleteAd(rentalAdId).enqueue(object : Callback<Void> {
                                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                        Toast.makeText(context, "Usunięto ogłoszenie", Toast.LENGTH_SHORT).show()

                                        dialog.dismiss()
                                        activity!!.supportFragmentManager.beginTransaction()
                                            .replace(R.id.fragment_container, HomeFragment()).commit()
                                    }

                                    override fun onFailure(call: Call<Void>, t: Throwable) {
                                        println(
                                            WARN, javaClass.toString(), "Deleting ad ${rentalAdById.id} failed /n$t"
                                        )
                                        Toast.makeText(context, "Nie można usunąć ogłoszenia", Toast.LENGTH_SHORT)
                                            .show()

                                        dialog.dismiss()
                                    }
                                })
                            }

                            cancelButton.setOnClickListener {
                                dialog.dismiss()
                            }
                        }

                        floatingActionButtonEdit.setOnClickListener {
                            val fragment = AddFragment()
                            val bundle = Bundle()
                            bundle.putLong("rentalAdId", rentalAdById.id)
                            fragment.arguments = bundle
                            val fr = activity!!.supportFragmentManager
                            val sharedPreferences = activity!!.getSharedPreferences("logged", Context.MODE_PRIVATE)
                            if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                                fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                            } else {
                                fr.beginTransaction().replace(R.id.fragment_container, UserNotLoggedInFragment())
                                    .commit()
                            }
                        }
                    }
                }

                val productImages = rentalAdById.imagesIdList
                title.text = rentalAdById.title
                location.text = rentalAdById.location.name
                pricePerDay.text = getPrice(rentalAdById.pricePerDay)
                pricePerDayDetails.text = getPrice(rentalAdById.pricePerDay)
                depositAmount.text = getPrice(rentalAdById.depositAmount)
                description.text = rentalAdById.description
                userName.text = rentalAdById.user.firstName
                penaltyForEachDayOfDelayInReturns.text = getPrice(rentalAdById.penaltyForEachDayOfDelayInReturns)
                ratingBar.rating = rentalAdById.user.sharerRate.toFloat()

                latitude = rentalAdById.location.latitude
                longitude = rentalAdById.location.longitude
                zoomValue = 10f

                map.apply {
                    val targetLocation = LatLng(latitude, longitude)
                    addMarker(MarkerOptions().position(targetLocation))
                    moveCamera(CameraUpdateFactory.newLatLngZoom(targetLocation, 10f))
                }

                val pattern = "dd-MM-yyyy"
                val simpleDateFormat = SimpleDateFormat(pattern)
                if (rentalAdById.timeSlotList.last().endDate != null) endDate.text =
                    simpleDateFormat.format(rentalAdById.timeSlotList.last().endDate).toString()
                else endDate.text = "-----"

                if (rentalAdById.timeSlotList.first().startDate != null) publicationDate.text =
                    simpleDateFormat.format(rentalAdById.timeSlotList.first().startDate).toString()
                else publicationDate.text = "-----"

                adImage.setImageResource(R.drawable.szerownia_logo_512)

                var position = 0
                if (productImages.isNotEmpty()) {

                    Glide.with(view).load("$serverUrl" + "api/image/" + productImages[position]).into(adImage)
                }

//                nextImage.setOnContextClickListener {
//                    if(position<productImages.size){
//                        position++
//                    }else{
//                        position=0
//                    }
//                    Glide.with(view).load("$serverUrl+api/image/"+productImages[position]).into(adImage)
//                    true
//                }
//                previousImage.setOnContextClickListener {
//                    if(position>0){
//                        position--
//                    }else{
//                        position=productImages.indices.last
//                    }
//                    Glide.with(view).load("$serverUrl+api/image/"+productImages[position]).into(adImage)
//                    true
//                }

                /**products list**/
                var productsList = emptyList<Product>()
                if (rentalAdById.products.isNotEmpty()) productsList = rentalAdById.products
                val productListAdapter = ProductListAdapter(productsList)
                val productGridLayoutManager = GridLayoutManager(activity, 1)
                productRecyclerView.layoutManager = productGridLayoutManager
                productRecyclerView.adapter = productListAdapter
                val discountText: TextView = view.findViewById(R.id.textView26)
                val discountAdapter = DiscountAdapter(rentalAdById.discounts)
                val discountGridLayoutManager = GridLayoutManager(activity, 1)
                if (rentalAdById.discounts.isEmpty()) {
                    discountText.visibility = View.GONE
                } else {
                    discountText.visibility = View.VISIBLE
                }
                discountRecyclerView.adapter = discountAdapter
                discountRecyclerView.layoutManager = discountGridLayoutManager

                val pickupImages = ArrayList<Int>()
                pickupImages.add(R.drawable.all_pickup_personal_icon)
                pickupImages.add(R.drawable.all_pickup_shipping_icon)
                pickupImages.add(R.drawable.parcel_locker)

                /**pickup method list**/
                var pickupMethodList = rentalAdById.pickupMethodList
                var pickupMethodListString = ArrayList<String>()

                for (i in pickupMethodList) {
                    pickupMethodListString.add(i.name)
                }

                val pickupAdapter = PickupMethodListAdapter(pickupMethodList, pickupImages)
                val pickupGridLayoutManager = GridLayoutManager(activity, 1)
                pickupRecyclerView.layoutManager = pickupGridLayoutManager
                pickupRecyclerView.adapter = pickupAdapter

                sendButton.setOnClickListener {
                    val fr = activity!!.supportFragmentManager

                    if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                        val dialogBuilder = AlertDialog.Builder(context)
                        val popupView = LayoutInflater.from(context).inflate(R.layout.popup_addetails_newmessage, null)
                        dialogBuilder.setView(popupView)
                        val dialog = dialogBuilder.create()
                        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        dialog.show()

                        val message: TextInputEditText = popupView.findViewById(R.id.chatTextInputEditText)
                        val sendButton: ImageView = popupView.findViewById(R.id.chatSendButton)

                        sendButton.setOnClickListener {
                            focus.clearFocus(requireActivity(), requireContext())

                            if (message.text.toString().isEmpty()) {
                                Toast.makeText(context, "Wiadomość jest pusta", Toast.LENGTH_SHORT).show()
                            } else {
                                val strRequestBody = message.text.toString()
                                val requestBody = strRequestBody.toRequestBody("text/plain".toMediaTypeOrNull())

                                val retrofitMessages = Retrofit.Builder().baseUrl(messagesUrl)
                                    .addConverterFactory(GsonConverterFactory.create(gson)).client(okHttpClient).build()

                                val apiMessages = retrofitMessages.create(MessagesAPI::class.java)
                                apiMessages.createMessageForNewThread(
                                    "Bearer " + sharedPreferences.getString(
                                        "TOKEN", null
                                    ).toString(),
                                    rentalAdById.user.id.toString() + "A" + rentalAdById.id.toString(),
                                    requestBody
                                ).enqueue(object : Callback<Void> {
                                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                        if (response.code() == 200) {
                                            dialog.dismiss()
                                            Toast.makeText(context, "Wiadomość wysłana", Toast.LENGTH_SHORT).show()
                                            println(
                                                INFO, javaClass.toString(), "New message sent"
                                            )
                                        } else {
                                            Toast.makeText(context, "Nie można wysłać wiadomości", Toast.LENGTH_SHORT)
                                                .show()
                                            println(
                                                ERROR,
                                                javaClass.toString(),
                                                "Sending message failed with error code ${response.code()}"
                                            )
                                        }
                                    }

                                    override fun onFailure(call: Call<Void>, t: Throwable) {
                                        Toast.makeText(context, "Nie można wysłać wiadomości", Toast.LENGTH_SHORT)
                                            .show()
                                        println(ERROR, javaClass.toString(), "Sending message failed: $t")
                                    }
                                })
                            }
                        }
                    } else {
                        fr.beginTransaction().replace(R.id.fragment_container, UserNotLoggedInFragment()).commit()
                    }

                }

                callButton.setOnClickListener {
                    if (response.body()!!.user.telephoneNumber != null) {
                        val telephoneNumber = response.body()!!.user.telephoneNumber

                        val intent = Intent(Intent.ACTION_DIAL)
                        intent.data = Uri.parse("tel:$telephoneNumber")
                        startActivity(intent)
                    }
                }

                rentButton.setOnClickListener {
                    api.getAllAvailableDatesOfHire(rentalAdById.id).enqueue(object : Callback<List<TimeSlotDTO>> {

                        override fun onResponse(call: Call<List<TimeSlotDTO>>, response: Response<List<TimeSlotDTO>>) {
                            val timeSlotList: MutableList<TimeSlot> = mutableListOf()
                            Log.e("Response", response.body().toString())
                            if (response.isSuccessful) {
                                for (element: TimeSlotDTO in response.body()!!) {
                                    timeSlotList.add(TimeSlot(element.to, element.from))

                                }
                                val fragment = RentFragment()
                                val bundle = Bundle()

                                bundle.putLong("rentalAdId", rentalAdById.id)
                                bundle.putString("timeSlots", gson.toJson(timeSlotList))
//                                bundle.putLong("DateFrom", rentalAdById.timeSlotList.first().startDate.time)
//                                if (rentalAdById.timeSlotList.last().endDate != null) bundle.putLong(
//                                    "DateTo", rentalAdById.timeSlotList.last().endDate!!.time
//                                )
                                bundle.putStringArrayList("pickupMethods", pickupMethodListString)
                                fragment.arguments = bundle
                                val fr = activity!!.supportFragmentManager
                                val sharedPreferences = activity!!.getSharedPreferences("logged", Context.MODE_PRIVATE)
                                if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                                    fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                                } else {
                                    fr.beginTransaction().replace(R.id.fragment_container, UserNotLoggedInFragment())
                                        .commit()
                                }
                            }
                        }

                        override fun onFailure(call: Call<List<TimeSlotDTO>>, t: Throwable) {
                            val dialogBuilder = AlertDialog.Builder(context)
                            val view2 = LayoutInflater.from(context).inflate(R.layout.popup_use_web, null)
                            val closeButton: ImageView = view2.findViewById(R.id.useWebButton)
                            val textView: TextView = view2.findViewById(R.id.useWebMessage)
                            textView.text = getString(R.string.ErrorWhileLoading)
                            dialogBuilder.setView(view2)
                            val dialog = dialogBuilder.create()
                            dialog.show()
                            closeButton.setOnClickListener {
                                dialog.dismiss()
                                dialog.dismiss()
                            }
                        }
                    })

                }

                val reportLayout: ConstraintLayout = view.findViewById(R.id.constraintlayout_addetails_report)

                reportLayout.setOnClickListener {
                    val dialogBuilder = AlertDialog.Builder(context)
                    val popupView = LayoutInflater.from(context).inflate(R.layout.popup_report_ad, null)
                    val closeButton: ImageView = popupView.findViewById(R.id.imageview_popup_report_close)

                    val fraud: ConstraintLayout = popupView.findViewById(R.id.constraintLayout_popup_report_fraud)
                    val offended: ConstraintLayout = popupView.findViewById(R.id.constraintLayout_popup_report_offended)
                    val indecent: ConstraintLayout = popupView.findViewById(R.id.constraintLayout_popup_report_indecent)
                    val illegal: ConstraintLayout = popupView.findViewById(R.id.constraintLayout_popup_report_illegal)

                    val reportDescription: TextInputLayout =
                        popupView.findViewById(R.id.textinputlayout_report_description)

                    dialogBuilder.setView(popupView)
                    val dialog = dialogBuilder.create()
                    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.show()

                    fraud.setOnClickListener {
                        report("FRAUD", dialog, reportDescription)
                    }

                    offended.setOnClickListener {
                        report("OFFENDED", dialog, reportDescription)
                    }

                    indecent.setOnClickListener {
                        report("INDECENT", dialog, reportDescription)
                    }

                    illegal.setOnClickListener {
                        report("ILLEGAL", dialog, reportDescription)
                    }

                    closeButton.setOnClickListener {
                        dialog.dismiss()
                    }
                }

                loadingDialog.dismissLoadingDialog()
            }

            override fun onFailure(call: Call<RentalAdById>, t: Throwable) {
                println(ERROR, javaClass.toString(), "Downloading rental ad details failed with error:/n$t")
                loadingDialog.dismissLoadingDialog()
                Toast.makeText(context, getString(R.string.rentaladdetails_getadbyidfailed), Toast.LENGTH_SHORT).show()

                (view.context as FragmentActivity).supportFragmentManager.beginTransaction().setCustomAnimations(
                    R.anim.slide_in_from_right,
                    R.anim.slide_out_to_right,
                    R.anim.slide_in_from_left,
                    R.anim.slide_out_to_left
                ).replace(R.id.fragment_container, BrowseFragment()).commit()
            }
        })

        backButton.setOnClickListener {
            (view.context as FragmentActivity).supportFragmentManager.beginTransaction().setCustomAnimations(
                R.anim.slide_in_from_right,
                R.anim.slide_out_to_right,
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_left
            ).replace(R.id.fragment_container, BrowseFragment()).commit()
        }
    }

    fun report(subject: String, dialog: AlertDialog, reportDescription: TextInputLayout) {

        if (reportDescription.editText!!.text.toString().isNotEmpty()) {

            val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
            val retrofit =
                Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()
            val api = retrofit.create(ReportAPI::class.java)

            val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
            val notifierId = sharedPreferences.getString("USER_ID", "0")

            val ticket = ReportAdTicket(
                rentalAdId, reportDescription.editText!!.text.toString(), notifierId?.toLong()!!, subject, false
            )

            api.createModTicket(ticket).enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    println(response.code())
                    if (response.code() == 200) {
                        dialog.dismiss()
                        Toast.makeText(context, "Dziękujemy za zgłoszenie!", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    e(javaClass.toString(), "Report error", t)
                    Toast.makeText(context, "Błąd dodawania zgłoszenia", Toast.LENGTH_SHORT).show()
                }
            })
        } else {
            reportDescription.error = "Uzasadnij swoje zgłoszenie"
        }
    }

    fun getPrice(price: Double): String {
        return if (price.rem(1).equals(0.0)) price.toInt().toString() + " zł"
        else price.toString() + "0 zł"
    }

    override fun onMapReady(p0: GoogleMap) {
        this.map = p0!!

        map.apply {
            val polska = LatLng(52.14, 19.7)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(polska, 5f))
        }
    }
}
