package szerownia.mobile.Fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.ReportAPI
import szerownia.mobile.API.SearchAdAPI
import szerownia.mobile.API.UsersAPI
import szerownia.mobile.Adapters.PickupMethodListAdapter
import szerownia.mobile.Adapters.ProductListAdapter
import szerownia.mobile.GlobalVariable
import szerownia.mobile.Helper
import szerownia.mobile.LoadingDialog
import szerownia.mobile.Models.MyDetails
import szerownia.mobile.Models.Product
import szerownia.mobile.Models.ReportAdTicket
import szerownia.mobile.Models.SearchAdById
import szerownia.mobile.R
import java.text.SimpleDateFormat

class SearchAdDetailsFragment : Fragment(), OnMapReadyCallback {

    private var serverUrl = ""

    private var userId = ""
    private var searchAdId: Long = 0
    private var latitude = 52.14
    private var longitude = 19.7
    private var zoomValue = 5f
    private lateinit var map: GoogleMap

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        val bundle = this.arguments
        if (bundle != null) {
            searchAdId = bundle.getLong("id")
        }
        return inflater.inflate(R.layout.fragment_searchad_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapView: MapView = view.findViewById(R.id.mapview_searchad_details)

        val loadingDialog = LoadingDialog(view, "Pobieranie szczegółów ogłoszenia...")
        loadingDialog.showLoadingDialog()

        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)

        val title: TextView = view.findViewById(R.id.textview_searchad_details_title)
        val location: TextView = view.findViewById(R.id.textview_searchad_details_location)
        val endDate: TextView = view.findViewById(R.id.textview_searchad_details_end_date)
        val publicationDate: TextView = view.findViewById(R.id.textview_searchad_details_start_date)
        val description: TextView = view.findViewById(R.id.textview_searchad_details_description)
        val adImage: ImageView = view.findViewById(R.id.imageview_searchad_details)
        val userName: TextView = view.findViewById(R.id.textview_searchad_details_username)
        val sendButton: Button = view.findViewById(R.id.button_searchad_details_message)
        val callButton: Button = view.findViewById(R.id.button_searchad_details_call)
        val ratingBar: RatingBar = view.findViewById(R.id.ratingBar_searchad_details)
        val replyButton: Button = view.findViewById(R.id.button)
        val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
        val token = "Bearer " + sharedPreferences.getString("TOKEN", null)


        val floatingActionButtonDelete: FloatingActionButton =
            view.findViewById(R.id.floatingactionbutton_addetails_delete)
        val floatingActionButtonEdit: FloatingActionButton = view.findViewById(R.id.floatingactionbutton_addetails_edit)
        val manageTitleConstraintLayout: ConstraintLayout =
            view.findViewById(R.id.constraintlayout_addetails_managertitle)


        val contactLayout: ConstraintLayout = view.findViewById(R.id.constraintlayout_searchad_contact)

        if (!sharedPreferences.getBoolean("LOGGED_KEY", false)) {
            replyButton.visibility = View.GONE
        }

        replyButton.setOnClickListener {
            val nextFragment = AddFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, nextFragment).commit()
        }

        val productRecyclerView: RecyclerView = view.findViewById(R.id.recyclerView_searchad_details_products)
        val pickupRecyclerView: RecyclerView = view.findViewById(R.id.recyclerView_searchad_details_pickup_methods)

        val bottomNavigationView = requireActivity().findViewById<BottomNavigationView>(R.id.bottom_navigation)
        val reportLayout: ConstraintLayout = view.findViewById(R.id.constraintLayout_searchad_details_report)

        if (!requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
                .getBoolean("LOGGED_KEY", false)
        ) reportLayout.visibility = View.GONE

        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
        val retrofit =
            Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()
        val api = retrofit.create(SearchAdAPI::class.java)
        val api2 = retrofit.create(UsersAPI::class.java)

        api.getSearchAd(searchAdId).enqueue(object : Callback<SearchAdById> {
            override fun onResponse(call: Call<SearchAdById>, response: Response<SearchAdById>) {
                val searchAdById = response.body()!!

                if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                    userId = sharedPreferences.getString("USER_ID", "0").toString()
                    if (userId != "0" && userId == response.body()!!.user.id.toString()) {
                        contactLayout.visibility = View.GONE
                        reportLayout.visibility = View.GONE
                        replyButton.visibility = View.GONE
                    }
                }
                if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                    userId = sharedPreferences.getString("USER_ID", "0").toString()
                    if (userId != "0" && userId == searchAdById.user.id.toString()) {
                        api2.getMyDetails(token).enqueue(object : Callback<MyDetails> {
                            @SuppressLint("SetTextI18n") override fun onResponse(
                                call: Call<MyDetails>, response: Response<MyDetails>
                            ) {
                                val myDetails = response.body()
//                                if (myDetails != null) {
//                                    if (myDetails.premium && !rentalAdById.promoted) {
//                                        floatingActionButtonPromote.visibility = View.VISIBLE
//                                    }
//                                }
                                return
                            }

                            override fun onFailure(call: Call<MyDetails>, t: Throwable) {
                                Log.e("Loading premium Status", t.toString())
                                return
                            }

                        })


                        floatingActionButtonDelete.visibility = View.VISIBLE
                        floatingActionButtonEdit.visibility = View.VISIBLE
                        manageTitleConstraintLayout.visibility = View.VISIBLE

                        reportLayout.visibility = View.GONE
                        replyButton.visibility = View.GONE


                        floatingActionButtonDelete.setOnClickListener {
                            val dialogBuilder = AlertDialog.Builder(context)
                            val popupView = LayoutInflater.from(context).inflate(R.layout.popup_addetails_delete, null)
                            Log.e("Ad to delete",searchAdById.id.toString())

                            val delete2Button: Button = popupView.findViewById(R.id.button_popupdeletead_yes)
                            val cancelButton: Button = popupView.findViewById(R.id.button_popupdeletead_no)

                            dialogBuilder.setView(popupView)
                            val dialog = dialogBuilder.create()
                            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                            dialog.show()

                            delete2Button.setOnClickListener {
                                Log.e("ToDelete", "aaaaaaaaaaa")
                                Log.println(Log.INFO, javaClass.toString(), "Deleting ad " + searchAdById.id)

                                api.deleteAd(searchAdId, token).enqueue(object : Callback<Void> {
                                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                                        Toast.makeText(context, "Usunięto ogłoszenie", Toast.LENGTH_SHORT).show()

                                        dialog.dismiss()
                                        activity!!.supportFragmentManager.beginTransaction()
                                            .replace(R.id.fragment_container, HomeFragment()).commit()
                                    }

                                    override fun onFailure(call: Call<Void>, t: Throwable) {
                                        Log.println(
                                            Log.WARN, javaClass.toString(), "Deleting ad ${searchAdById.id} failed /n$t"
                                        )
                                        Toast.makeText(context, "Nie można usunąć ogłoszenia", Toast.LENGTH_SHORT)
                                            .show()

                                        dialog.dismiss()
                                    }
                                })
                            }

                            cancelButton.setOnClickListener {
                                Log.e("Dissmissed","aaaaa")
                                dialog.dismiss()
                            }
                        }

                        floatingActionButtonEdit.setOnClickListener {
                            val fragment = AddSearchFragment()
                            val bundle = Bundle()
                            bundle.putLong("rentalAdId", searchAdById.id)
                            fragment.arguments = bundle
                            val fr = activity!!.supportFragmentManager
                            val sharedPreferences = activity!!.getSharedPreferences("logged", Context.MODE_PRIVATE)
                            if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                                fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                            } else {
                                fr.beginTransaction().replace(R.id.fragment_container, UserNotLoggedInFragment())
                                    .commit()
                            }
                        }
                    }
                }

                title.text = searchAdById.title
                location.text = searchAdById.location.name
                description.text = searchAdById.description
                userName.text = searchAdById.user.firstName
                ratingBar.rating = searchAdById.user.borrowerRate.toFloat()

                latitude = searchAdById.location.latitude
                longitude = searchAdById.location.longitude
                zoomValue = 10f

                map.apply {
                    val targetLocation = LatLng(latitude, longitude)
                    addMarker(MarkerOptions().position(targetLocation))
                    moveCamera(CameraUpdateFactory.newLatLngZoom(targetLocation, 10f))
                }

                val pattern = "dd-MM-yyyy"
                val simpleDateFormat = SimpleDateFormat(pattern)
                if (searchAdById.timeSlotList.isNotEmpty()) endDate.text =
                    simpleDateFormat.format(searchAdById.timeSlotList[searchAdById.timeSlotList.size-1].endDate).toString()
                else endDate.text = "-----"

                if (searchAdById.timeSlotList.isNotEmpty()) publicationDate.text =
                    simpleDateFormat.format(searchAdById.timeSlotList[0].startDate).toString()
                else publicationDate.text = "-----"

                Log.e("SearchAdById", searchAdById.toString())
                Log.e("Products", searchAdById.products.toString())
                /**products list**/
                var productsList = emptyList<Product>()
                if (searchAdById.products.isNotEmpty()) productsList = searchAdById.products
                val productListAdapter = ProductListAdapter(productsList)
                val productGridLayoutManager = GridLayoutManager(activity, 1)
                productRecyclerView.layoutManager = productGridLayoutManager
                productRecyclerView.adapter = productListAdapter

                /**pickup method list**/
                val pickupImages = ArrayList<Int>()
                pickupImages.add(R.drawable.all_pickup_personal_icon)
                pickupImages.add(R.drawable.all_pickup_shipping_icon)
                pickupImages.add(R.drawable.parcel_locker)

                var pickupMethodList = searchAdById.pickupMethod
                val pickupAdapter = PickupMethodListAdapter(pickupMethodList, pickupImages)
                val pickupGridLayoutManager = GridLayoutManager(activity, 1)
                pickupRecyclerView.layoutManager = pickupGridLayoutManager
                pickupRecyclerView.adapter = pickupAdapter

                sendButton.setOnClickListener {
                    val fragment = ChatFragment()
                    val bundle = Bundle()
                    bundle.putLong("rentalAdId", response.body()!!.id)
                    bundle.putString("rentalAdTitle", response.body()!!.title)
                    fragment.arguments = bundle
                    val fr = activity!!.supportFragmentManager
                    bottomNavigationView.selectedItemId = R.id.menu_messages
                    val sharedPreferences = activity!!.getSharedPreferences("logged", Context.MODE_PRIVATE)
                    if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                        fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
                    } else {
                        fr.beginTransaction().replace(R.id.fragment_container, UserNotLoggedInFragment()).commit()
                    }
                }
                if (!sharedPreferences.getBoolean("LOGGED_KEY", false)) {
                    callButton.visibility = View.GONE
                }

                callButton.setOnClickListener {
                    if (response.body() != null) {
                        val telephoneNumber = response.body()!!.user.telephoneNumber

                        val intent = Intent(Intent.ACTION_DIAL)
                        intent.data = Uri.parse("tel:$telephoneNumber")
                        startActivity(intent)
                    }
                }

                loadingDialog.dismissLoadingDialog()

                reportLayout.setOnClickListener {
                    val dialogBuilder = AlertDialog.Builder(context)
                    val popupView = LayoutInflater.from(context).inflate(R.layout.popup_report_ad, null)
                    val closeButton: ImageView = popupView.findViewById(R.id.imageview_popup_report_close)

                    val fraud: ConstraintLayout = popupView.findViewById(R.id.constraintLayout_popup_report_fraud)
                    val offended: ConstraintLayout = popupView.findViewById(R.id.constraintLayout_popup_report_offended)
                    val indecent: ConstraintLayout = popupView.findViewById(R.id.constraintLayout_popup_report_indecent)
                    val illegal: ConstraintLayout = popupView.findViewById(R.id.constraintLayout_popup_report_illegal)
                    val reportDescription: TextInputLayout =
                        popupView.findViewById(R.id.textinputlayout_report_description)

                    dialogBuilder.setView(popupView)
                    val dialog = dialogBuilder.create()
                    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.show()

                    fraud.setOnClickListener {
                        report("FRAUD", dialog, reportDescription)
                    }

                    offended.setOnClickListener {
                        report("OFFENDED", dialog, reportDescription)
                    }

                    indecent.setOnClickListener {
                        report("INDECENT", dialog, reportDescription)
                    }

                    illegal.setOnClickListener {
                        report("ILLEGAL", dialog, reportDescription)
                    }

                    closeButton.setOnClickListener {
                        dialog.dismiss()
                    }
                }

            }

            override fun onFailure(call: Call<SearchAdById>, t: Throwable) {
                loadingDialog.dismissLoadingDialog()

                Log.e(javaClass.toString(), "Downloading search ad details failed", t)
                Toast.makeText(context, "Błąd pobierania szczegółów ogłoszenia", Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun getPrice(price: Double): String {
        return if (price.rem(1).equals(0.0)) price.toInt().toString() + " zł"
        else price.toString() + "0 zł"
    }

    fun report(subject: String, dialog: AlertDialog, reportDescription: TextInputLayout) {

        if (reportDescription.editText!!.text.toString().isNotEmpty()) {

            val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
            val retrofit =
                Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create(gson)).build()
            val api = retrofit.create(ReportAPI::class.java)

            val sharedPreferences = requireActivity().getSharedPreferences("logged", Context.MODE_PRIVATE)
            val notifierId = sharedPreferences.getString("USER_ID", "0")

            val ticket = ReportAdTicket(
                searchAdId, reportDescription.editText!!.text.toString(), notifierId?.toLong()!!, subject, true
            )

            api.createModTicket(ticket).enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    println(response.code())
                    if (response.code() == 200) {
                        dialog.dismiss()
                        Toast.makeText(context, "Dziękujemy za zgłoszenie!", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    Log.e(javaClass.toString(), "Sending mod ticket failed", t)
                    Toast.makeText(context, "Błąd wysyłania raportu", Toast.LENGTH_SHORT).show()
                }
            })
        } else {
            reportDescription.error = "Uzasadnij swoje zgłoszenie"
        }
    }

    override fun onMapReady(p0: GoogleMap) {
        this.map = p0!!

        map.apply {
            val polska = LatLng(52.14, 19.7)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(polska, 5f))
        }
    }
}
