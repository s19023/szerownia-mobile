package szerownia.mobile.Fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.auth0.android.jwt.JWT
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputEditText
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.*
import szerownia.mobile.API.UsersAPI
import szerownia.mobile.Models.Login
import szerownia.mobile.Models.MyDetails
import szerownia.mobile.Models.Token
import java.util.concurrent.TimeUnit

class LoginFragment : Fragment() {

    private var serverUrl = ""
    private var userName = ""
    private val validation = Validation()
    private val focus = Focus()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val registerPrompt: TextView = view.findViewById(R.id.registerPrompt)
        val loginButton: TextView = view.findViewById(R.id.loginButton)
        val loginEmail: TextInputEditText = view.findViewById(R.id.loginEmail)
        val loginPassword: TextInputEditText = view.findViewById(R.id.loginPassword)
        val bottomNavigationView = requireActivity().findViewById<BottomNavigationView>(R.id.bottom_navigation)
        val remindPrompt:TextView = view.findViewById(R.id.remindPassword)

        remindPrompt.setOnClickListener {
            focus.clearFocus(requireActivity(), requireContext())

            val fragment: Fragment = ResetPasswordFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
        }
        registerPrompt.setOnClickListener {
            focus.clearFocus(requireActivity(), requireContext())

            val fragment: Fragment = RegisterFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
        }

        loginButton.setOnClickListener {
            validation.resetValidationCounter()

            validation.validateIfEmpty(loginEmail, R.string.all_emptymale, R.string.login_email, requireActivity())
            validation.validateIfEmpty(loginPassword, R.string.all_emptyit, R.string.login_password, requireActivity())

            if (validation.getValidationCounter() == 0) {
                val login = Login(loginEmail.text.toString(), loginPassword.text.toString())

                val okHttpClient =
                    OkHttpClient().newBuilder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS)
                        .build()

                val retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient).build()
                val api = retrofit.create(UsersAPI::class.java)

                val loadingDialog = LoadingDialog(view, "Logowanie...")
                loadingDialog.showLoadingDialog()

                api.login(login).enqueue(object : Callback<Token> {
                    override fun onFailure(call: Call<Token>, t: Throwable) {
                        loadingDialog.dismissLoadingDialog()
                        loginEmail.clearFocus()
                        loginPassword.clearFocus()

                        Log.println(Log.ERROR, javaClass.toString(), "Logging in failed /n$t")
                        Toast.makeText(context, "Logowanie nie powiodło się", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<Token>, response: Response<Token>) {
                        loadingDialog.dismissLoadingDialog()
                        focus.clearFocus(requireActivity(), requireContext())

                        if (response.isSuccessful) {
                            val jwt = JWT(response.body()!!.token)
                            val token = response.body()!!.token
                            val refreshToken = response.body()!!.refreshToken

                            val userId = jwt.getClaim("id").asString().toString()
                            val role = jwt.getClaim("role").asString().toString()
                            if (role !in resources.getStringArray(R.array.Superusers)) {
                                val sharedPreferences = activity!!.getSharedPreferences("logged", Context.MODE_PRIVATE)
                                val editor = sharedPreferences.edit()
                                editor.apply {
                                    putString("TOKEN", token)
                                    putString("REFRESH_TOKEN", refreshToken)
                                    putString("USER_ID", userId)
                                    putBoolean("LOGGED_KEY", true)
                                    putString("LOGIN_EMAIL", loginEmail.text.toString())
                                    putString("LOGIN_PASSWORD", loginPassword.text.toString())
                                }.apply()

                                try {
                                    val activity: MainActivity = requireActivity() as MainActivity
                                    activity.getMessagesWebSocketService().openConnection(activity.applicationContext)
                                } catch (e: Exception) {
                                    Log.println(Log.ERROR, javaClass.toString(), "Opening socket failed with error: $e")
                                }

                                val apiUserDetails = retrofit.create(UsersAPI::class.java)
                                apiUserDetails.getMyDetails("Bearer $token").enqueue(object : Callback<MyDetails> {
                                    @SuppressLint("SetTextI18n") override fun onResponse(
                                        call: Call<MyDetails>, response: Response<MyDetails>
                                    ) {
                                        val myDetails = response.body()
                                        if (myDetails != null) {
                                            userName = myDetails.firstName
                                            println(userName + " received")

                                            editor.apply {
                                                putString("USER_NAME", userName)
                                            }.apply()
                                        }
                                    }

                                    override fun onFailure(call: Call<MyDetails>, t: Throwable) {
                                        Log.e(javaClass.toString(), "Downloading user data failed", t)
                                        Toast.makeText(context, "Błąd pobierania szczegółów użytkownika", Toast.LENGTH_SHORT).show()
                                    }
                                })

//                                if (loginEmail.text.toString() in resources.getStringArray(R.array.Appraisers)) {
//                                    val myIntent = Intent(context, AppraiserActivity::class.java)
//                                    startActivity(myIntent)
//                                } else {

                                val fragment: Fragment = HomeFragment()
                                val fr = activity!!.supportFragmentManager
                                bottomNavigationView.selectedItemId = R.id.menu_home
                                fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
//                                }

                            } else {
                                if (role == "APPRAISER") {
                                    val sharedPreferences =
                                        activity!!.getSharedPreferences("logged", Context.MODE_PRIVATE)
                                    val editor = sharedPreferences.edit()
                                    editor.apply {
                                        putString("TOKEN", token)
                                        putString("REFRESH_TOKEN", refreshToken)
                                        putString("USER_ID", userId)
                                        putBoolean("LOGGED_KEY", true)
                                        putString("LOGIN_EMAIL", loginEmail.text.toString())
                                        putString("LOGIN_PASSWORD", loginPassword.text.toString())
                                    }.apply()
                                    val myIntent = Intent(context, AppraiserActivity::class.java)
                                    startActivity(myIntent)
                                } else {
                                    val dialogBuilder = AlertDialog.Builder(context)
                                    val view2 = LayoutInflater.from(context).inflate(R.layout.popup_use_web, null)
                                    val closeButton: ImageView = view2.findViewById(R.id.useWebButton)
                                    dialogBuilder.setView(view2)
                                    val dialog = dialogBuilder.create()
                                    dialog.show()
                                    closeButton.setOnClickListener { dialog.dismiss() }
                                }
                            }
                        } else {
                            Log.println(Log.WARN, javaClass.toString(), "Logging in failed, wrong email or password")
                            Toast.makeText(
                                context,
                                "Logowanie nie powiodło się, błędny email lub hasło",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                })
            }
        }
    }
}
