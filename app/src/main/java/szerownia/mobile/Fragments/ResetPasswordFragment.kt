package szerownia.mobile.Fragments

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.*
import szerownia.mobile.API.UsersAPI
import java.util.concurrent.TimeUnit

class ResetPasswordFragment : Fragment() {

    private var serverUrl = ""
    private var userName = ""
    private val validation = Validation()
    private val focus = Focus()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val globalVariable: GlobalVariable = requireContext().applicationContext as GlobalVariable
        serverUrl = if (globalVariable.distantServer) {
            Helper().getConfigValue(requireContext().applicationContext, "server_url")!!
        } else {
            Helper().getConfigValue(requireContext().applicationContext, "localhost_url")!!
        }
        return inflater.inflate(R.layout.fragment_reset_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rememberPrompt: TextView = view.findViewById(R.id.goBackPrompt)
        val resetPasswordButton: TextView = view.findViewById(R.id.resetPasswordButton)
        val resetPasswordEmail: TextInputEditText = view.findViewById(R.id.resetPassword)

        rememberPrompt.setOnClickListener {
            focus.clearFocus(requireActivity(), requireContext())

            val fragment: Fragment = LoginFragment()
            val fr = requireActivity().supportFragmentManager
            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()
        }

        resetPasswordButton.setOnClickListener {
            validation.resetValidationCounter()

            validation.validateIfEmpty(
                resetPasswordEmail,
                R.string.all_emptymale,
                R.string.register_email,
                requireActivity()

            )

            if (validation.getValidationCounter() == 0) {
                val okHttpClient =
                    OkHttpClient().newBuilder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS)
                        .build()







                val retrofit = Retrofit.Builder().baseUrl(serverUrl).addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient).build()
                val api = retrofit.create(UsersAPI::class.java)

                val loadingDialog = LoadingDialog(view, "Resetowanie Hasła...")
                loadingDialog.showLoadingDialog()

                val body: RequestBody =
                    resetPasswordEmail.text.toString().toRequestBody("text/plain".toMediaTypeOrNull())

                api.getResetToken(body).enqueue(object : Callback<Void> {
                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        loadingDialog.dismissLoadingDialog()
                        Log.println(Log.ERROR, javaClass.toString(), "Reset failed /n$t")
                        Toast.makeText(context, "Reset hasła nie powiódł się", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<Void>, response: Response<Void>) {

                        loadingDialog.dismissLoadingDialog()
                        focus.clearFocus(requireActivity(), requireContext())
                        val dialogBuilder = AlertDialog.Builder(context)
                        val view2 = LayoutInflater.from(context).inflate(R.layout.popup_use_web, null)
                        val closeButton: ImageView = view2.findViewById(R.id.useWebButton)
                        val textView:TextView = view2.findViewById(R.id.useWebMessage)
                        textView.text = "Link resetujący został Ci wysłany na maila."
                        dialogBuilder.setView(view2)
                        val dialog = dialogBuilder.create()
                        dialog.show()
                        closeButton.setOnClickListener { dialog.dismiss()
                            val fragment: Fragment = LoginFragment()
                            val fr = requireActivity().supportFragmentManager
                            fr.beginTransaction().replace(R.id.fragment_container, fragment).commit()}

                    }
                })
            }
        }
    }
}
