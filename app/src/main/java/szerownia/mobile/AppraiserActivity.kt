package szerownia.mobile

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Button
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import szerownia.mobile.API.TokenAPI
import szerownia.mobile.Fragments.Appraiser.AppraiserHomeFragment
import szerownia.mobile.Models.RefreshToken
import szerownia.mobile.Models.Token

class AppraiserActivity : AppCompatActivity() {
    var handler: Handler = Handler()
    var serverUrl =""
    var runnable: Runnable? = null
    var delay = 300000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val globalVariable:GlobalVariable = applicationContext as GlobalVariable
        serverUrl = if(globalVariable.distantServer) {
            Helper().getConfigValue(applicationContext, "server_url")!!
        }else{
            Helper().getConfigValue(applicationContext, "localhost_url")!!
        }
        setContentView(R.layout.activity_appraiser)
        val logout:Button = findViewById(R.id.AppraiserLogout)
        val sharedPreferences = getSharedPreferences("logged", Context.MODE_PRIVATE)
        logout.setOnClickListener {
            val editor = sharedPreferences.edit()
            editor.clear()  //temporary solution? not to safe
            editor.apply {
                putBoolean("LOGGED_KEY", false)
            }.apply()

            val myIntent = Intent(this, MainActivity::class.java)
            startActivity(myIntent)
        }
        supportFragmentManager.beginTransaction().replace(R.id.appraiserFragmentContainer, AppraiserHomeFragment()).commit()
    }
    override fun onResume() {
        super.onResume()
        println("Refreshing token on startup")
        refreshToken()
        handler.postDelayed(Runnable {
            refreshToken()
            handler.postDelayed(runnable!!, delay.toLong())
        }.also { runnable = it }, delay.toLong())
    }
    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable!!)
    }




    private fun refreshToken() {

        val sharedPreferences = getSharedPreferences("logged", Context.MODE_PRIVATE)
        if (sharedPreferences.getBoolean("LOGGED_KEY", false)) {
            println("Refreshing token")

            val gson = GsonBuilder().create()
            val retrofit = Retrofit.Builder().baseUrl(serverUrl)
                .addConverterFactory(GsonConverterFactory.create(gson)).build()

            val token = sharedPreferences.getString("TOKEN", null)
            val rfToken = sharedPreferences.getString("REFRESH_TOKEN", null)
            var refreshToken = RefreshToken(token!!, rfToken!!)

            val api = retrofit.create(TokenAPI::class.java)

            api.refreshToken(refreshToken).enqueue(object : Callback<Token> {
                override fun onResponse(call: Call<Token>, response: Response<Token>) {
                    println(response.code())

                    if (response.body() != null) {
                        val editor = sharedPreferences.edit()
                        editor.apply {
                            putString("TOKEN", response.body()!!.token)
                            putString("REFRESH_TOKEN", response.body()!!.refreshToken)
                        }.apply()
                    } else {
                        val editor = sharedPreferences.edit()
                        editor.apply {
                            putBoolean("LOGGED_KEY", false)
                        }.apply()
                    }

                }

                override fun onFailure(call: Call<Token>, t: Throwable) {
                    Log.e("Appraiser token refresh",t.toString())
                }
            })
        }
    }
}