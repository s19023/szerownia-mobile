package szerownia.mobile

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentActivity

class Focus {
    fun clearFocus(activity: FragmentActivity, context: Context){
        val viewFocus: View? = activity.currentFocus
        if (viewFocus != null) {
            val imm: InputMethodManager =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(viewFocus.windowToken, 0)
            viewFocus.clearFocus()
        }
    }
}
