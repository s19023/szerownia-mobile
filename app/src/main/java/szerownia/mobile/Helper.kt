package szerownia.mobile

import android.content.Context
import android.content.res.Resources
import android.util.Log

import java.io.IOException
import java.io.InputStream
import java.util.*

class Helper() {
    private val _tag = "Helper"

    public fun getConfigValue(context: Context, name: String?): String? {
        val resources: Resources =context.resources
        try {
            val rawResource: InputStream = resources.openRawResource(R.raw.config)
            val properties = Properties()
            properties.load(rawResource)
            return properties.getProperty(name)
        } catch (e: Resources.NotFoundException) {
            Log.e(_tag, "Unable to find the config file: " + e.message)
        } catch (e: IOException) {
            Log.e(_tag, "Failed to open config file.")
        }
        return null
    }
}